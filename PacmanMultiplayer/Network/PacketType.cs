﻿namespace Network
{
    public struct PacketType
    {
        public struct ConnectionError
        {
            public const ushort NICKNAME_INVALID = 0xff0;
            public const ushort NICKNAME_TAKEN = 0x0fff;
            public const ushort NICKNAME_TOO_LONG = 0x0fa1;
            public const ushort NICKNAME_TOO_SHORT = 0x0fb3;
            public const ushort SERVER_FULL = 0x0f0c;
        }

        public struct Connection
        {
            public const ushort CONNECTION_FAILED = 0x0bad;
            public const ushort FIRST_HELLO = 0x0001;
            public const ushort HELLO = 0x0011;
            public const ushort WELCOME = 0x0111;
            public const ushort SERVER_DISCONNECT = 0x0069;
            public const ushort CLIENT_DISCONNECT = 0x0096;
        }

        public struct Info
        {
            public const ushort ACK = 0x1145;
            public const ushort CLIENT_LEFT = 0x0099;
            public const ushort GAME_ROOMS_ACK = 0x1a09;
            public const ushort GAME_ROOM_INFO = 0x15bc;
            public const ushort GAME_ROOM_CREATED = 0x13f9;
            public const ushort GAME_ROOM_CLOSED = 0x14f4;
            public const ushort GAME_ROOM_CLIENT_JOINED = 0x15ab;
            public const ushort GAME_ROOM_CLIENT_LEFT = 0x18ab;
        }

        public struct Request
        {
            public const ushort CONNECT = 0x3abc;
            public const ushort PM = 0xAF00;
            public const ushort MOVE_REQUEST = 0x3001;
            public const ushort MESSAGE_ROOM = 0x3fcd;
            public const ushort START_GAME = 0x30fd;
            public const ushort CREATE_ROOM = 0x3034;
            public const ushort JOIN_ROOM = 0x3081;
            //public const ushort CLOSE_ROOM = 0x3199;
            public const ushort EXIT_ROOM = 0x3200;
        }

        public struct Response
        {
            public const ushort JOIN_ROOM_REQUEST_FAILED = 0x4035;
            public const ushort CREATE_ROOM_REQUEST_OK = 0x4f1f;
        }

        public struct Game
        {
            public const ushort GAME_ABORTED = 0x6521;
            public const ushort GAME_START = 0x6415;
            public const ushort GAME_OVER = 0x6919;
            public const ushort GAME_INIT = 0x6001;
        }

        public struct GameInfo
        {
            public const ushort SEND_MAP = 0x7001;
            public const ushort ALL_PLAYERS_POSITION = 0x7021;
            public const ushort ALL_GHOSTS_POSITION = 0x7022;
            public const ushort GHOST_DIRECTION = 0x7045;
            public const ushort PLAYER_DIRECTION = 0x7046;
            public const ushort PLAYER_RESPAWN = 0x7111;
        }
    }
}
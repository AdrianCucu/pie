﻿using static System.Reflection.MethodBase;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Net;
using System;
using System.IO;

namespace Network
{
    /*
        Packet format
        =============

        byte
        0        1        2        3        4
        +--------.--------.--------.--------+
        |   Packet size   |    Packet Type  |   Packet Header
        +-----------------+-----------------+
        |                                   |
        .                                   .
        .       MAX 65531 PAYLOAD DATA      .   Packet Payload Data
        .                                   .
        +-----------------------------------+


        Packet TOTAL size
        =   4(Packet Header Size) + 
            Packet Payload Data Size
        ====================================================
        Packet size <=  65535 bytes
        Pakcet Type =   PacketType.Connection.SERVER_FULL,   
                        PacketType.GameInfo.PLAYER_DIRECTION
                        ...
    */

    public struct Packet
    {
        public static readonly int HEADER_LEN = 4;

        private ushort m_PacketType;
        private PacketWriter m_Writer;
        private PacketReader m_Reader;
        private byte[] m_ReaderBuffer;
        private bool m_Locked;
        private object m_Lock;

        public ushort Type
        {
            get => m_PacketType;
        }

        public Packet(ushort packetType)
        {
            m_Lock = new object();
            m_PacketType = packetType;
            m_Writer = new PacketWriter();
            m_Reader = null;
            m_ReaderBuffer = null;
            m_Locked = false;
        }

        public Packet(ushort packetType, byte[] bytes)
        {
            m_Lock = new object();
            m_PacketType = packetType;
            m_Writer = new PacketWriter();
            m_Writer.Write(bytes);
            m_Reader = null;
            m_ReaderBuffer = null;
            m_Locked = false;
        }

        public Packet(ushort packetType, byte[] bytes, int offset, int length)
        {
            m_Lock = new object();
            m_PacketType = packetType;
            m_Writer = new PacketWriter();
            m_Writer.Write(bytes, offset, length);
            m_Reader = null;
            m_ReaderBuffer = null;
            m_Locked = false;
        }

        public byte[] Pack()
        {
            if (!m_Locked)
            {
                //Construct the packet by adding packet header

                long payload_size = m_Writer.BaseStream.Position;
                long packet_size = payload_size + 4;

                if (payload_size > 65531)
                {
                    throw new Exception("Packet payload is too big.");
                }

                byte[] raw_packet = new byte[payload_size + 4];

                byte[] raw_payload = m_Writer.GetBytes();


                Buffer.BlockCopy(
                    raw_payload, 0,
                    raw_packet, 4, raw_payload.Length);


                raw_packet[0] = (byte)((packet_size >> 0) & 0xff);
                raw_packet[1] = (byte)((packet_size >> 8) & 0xff);

                raw_packet[2] = (byte)((m_PacketType >> 0) & 0xff);
                raw_packet[3] = (byte)((m_PacketType >> 8) & 0xff);

                return raw_packet;
            }
            return null;
        }

        public byte[] GetBytes()
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    return m_ReaderBuffer;
                }
                return m_Writer.GetBytes();
            }
        }

        public void Lock()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    m_ReaderBuffer = m_Writer.GetBytes();
                    m_Reader = new PacketReader(m_ReaderBuffer);
                    m_Writer.Close();
                    m_Writer = null;
                    m_Locked = true;
                }
            }
        }

        public void SkipBytes(uint amount)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Skip from an unlocked Packet.");
                }
                m_Reader.BaseStream.Position += amount;
            }
        }

        public long SeekRead(long offset, SeekOrigin orgin)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot SeekRead on an unlocked Packet.");
                }
                return m_Reader.BaseStream.Seek(offset, orgin);
            }
        }

        public int RemainingRead()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot SeekRead on an unlocked Packet.");
                }
                return (int)(m_Reader.BaseStream.Length - m_Reader.BaseStream.Position);
            }
        }

        public byte ReadUInt8()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadByte();
            }
        }
        public byte ReadByte()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadByte();
            }
        }
        public sbyte ReadInt8()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadSByte();
            }
        }
        public UInt16 ReadUInt16()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadUInt16();
            }
        }
        public Int16 ReadInt16()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadInt16();
            }
        }
        public UInt32 ReadUInt32()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadUInt32();
            }
        }
        public Int32 ReadInt32()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadInt32();
            }
        }
        public UInt64 ReadUInt64()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadUInt64();
            }
        }
        public Int64 ReadInt64()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadInt64();
            }
        }
        public Single ReadSingle()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadSingle();
            }
        }
        public Double ReadDouble()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                return m_Reader.ReadDouble();
            }
        }
        public String ReadAscii()
        {
            return ReadAscii(1252);
        }
        public String ReadAscii(int codepage)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }

                UInt16 length = m_Reader.ReadUInt16();

                if (length > 0)
                {
                    byte[] bytes = m_Reader.ReadBytes(length);
                    return Encoding.GetEncoding(codepage).GetString(bytes);
                }
                else
                {
                    return null;
                }
            }
        }
        public String ReadUnicode()
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }

                UInt16 length = m_Reader.ReadUInt16();
                byte[] bytes = m_Reader.ReadBytes(length * 2);
                return Encoding.Unicode.GetString(bytes);
            }
        }

        public byte[] ReadUInt8Array(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                byte[] values = new byte[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadByte();
                }
                return values;
            }
        }
        public char[] ReadCharArray(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                char[] values = m_Reader.ReadChars(count);
                return values;
            }
        }
        public sbyte[] ReadInt8Array(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                sbyte[] values = new sbyte[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadSByte();
                }
                return values;
            }
        }
        public UInt16[] ReadUInt16Array(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                UInt16[] values = new UInt16[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadUInt16();
                }
                return values;
            }
        }
        public Int16[] ReadInt16Array(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                Int16[] values = new Int16[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadInt16();
                }
                return values;
            }
        }
        public UInt32[] ReadUInt32Array(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                UInt32[] values = new UInt32[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadUInt32();
                }
                return values;
            }
        }
        public Int32[] ReadInt32Array(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                Int32[] values = new Int32[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadInt32();
                }
                return values;
            }
        }
        public UInt64[] ReadUInt64Array(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                UInt64[] values = new UInt64[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadUInt64();
                }
                return values;
            }
        }
        public Int64[] ReadInt64Array(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                Int64[] values = new Int64[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadInt64();
                }
                return values;
            }
        }
        public Single[] ReadSingleArray(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                Single[] values = new Single[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadSingle();
                }
                return values;
            }
        }
        public Double[] ReadDoubleArray(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                Double[] values = new Double[count];
                for (int x = 0; x < count; ++x)
                {
                    values[x] = m_Reader.ReadDouble();
                }
                return values;
            }
        }
        public String[] ReadAsciiArray(int count)
        {
            return ReadAsciiArray(1252);
        }
        public String[] ReadAsciiArray(int codepage, int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                String[] values = new String[count];
                for (int x = 0; x < count; ++x)
                {
                    UInt16 length = m_Reader.ReadUInt16();
                    byte[] bytes = m_Reader.ReadBytes(length);
                    values[x] = Encoding.UTF7.GetString(bytes);
                }
                return values;
            }
        }
        public String[] ReadUnicodeArray(int count)
        {
            lock (m_Lock)
            {
                if (!m_Locked)
                {
                    throw new Exception("Cannot Read from an unlocked Packet.");
                }
                String[] values = new String[count];
                for (int x = 0; x < count; ++x)
                {
                    UInt16 length = m_Reader.ReadUInt16();
                    byte[] bytes = m_Reader.ReadBytes(length * 2);
                    values[x] = Encoding.Unicode.GetString(bytes);
                }
                return values;
            }
        }

        public void CopyBytes(byte[] buffer, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Copy on a locked Packet.");
                }
                m_Writer.Write(buffer, index, count);
            }
        }

        public long SeekWrite(long offset, SeekOrigin orgin)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot SeekWrite on a locked Packet.");
                }
                return m_Writer.BaseStream.Seek(offset, orgin);
            }
        }
        public void GoBackAndWrite(long backlen, byte a)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position - backlen;
                m_Writer.Write((byte)a);
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position + backlen - 1;
            }
        }
        public void GoBackAndWrite(long backlen, Int16 a)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position - backlen;
                m_Writer.Write((Int16)a);
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position + backlen + 2;
            }
        }
        public void GoBackAndWrite(long backlen, UInt16 a)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position - backlen;
                m_Writer.Write((UInt16)a);
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position + backlen + 2;
            }
        }
        public void GoBackAndWrite(long backlen, Int32 a)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position - backlen;
                m_Writer.Write((Int32)a);
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position + backlen + 4;
            }
        }
        public void GoBackAndWrite(long backlen, UInt32 a)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position - backlen;
                m_Writer.Write((UInt32)a);
                m_Writer.BaseStream.Position = m_Writer.BaseStream.Position + backlen + 4;
            }
        }

        public void ReWriteAt(long position, byte value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                long backup_position = m_Writer.BaseStream.Position;
                m_Writer.BaseStream.Position = position;
                m_Writer.Write((byte)value);
                m_Writer.BaseStream.Position = backup_position;
            }
        }

        public void WriteUInt8(byte value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteByte(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(unchecked((byte)Convert.ToInt64(value)));
            }
        }
        public void WriteInt8(sbyte value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteUInt16(UInt16 value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteInt16(Int16 value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteUInt32(UInt32 value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteInt32(Int32 value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteUInt64(UInt64 value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteInt64(Int64 value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteSingle(Single value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteDouble(Double value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(value);
            }
        }
        public void WriteAscii(String value)
        {
            WriteAscii(value, 1252);
        }
        public void WriteAscii(String value, int code_page)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }

                byte[] codepage_bytes = Encoding.GetEncoding(code_page).GetBytes(value);
                string utf7_value = Encoding.UTF7.GetString(codepage_bytes);
                byte[] bytes = Encoding.Default.GetBytes(utf7_value);

                m_Writer.Write((ushort)bytes.Length);
                m_Writer.Write(bytes);
            }
        }
        public void WriteUnicode(String value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }

                byte[] bytes = Encoding.Unicode.GetBytes(value);

                m_Writer.Write((ushort)value.ToString().Length);
                m_Writer.Write(bytes);
            }
        }

        public void WriteUInt8(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(unchecked((byte)Convert.ToInt64(value)));
            }
        }
        public void WriteInt8(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(unchecked((sbyte)Convert.ToInt64(value)));
            }
        }
        public void WriteUInt16(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(unchecked((ushort)Convert.ToInt64(value)));
            }
        }
        public void WriteInt16(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(unchecked((short)Convert.ToInt64(value)));
            }
        }
        public void WriteUInt32(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(unchecked((uint)Convert.ToInt64(value)));
            }
        }
        public void WriteInt32(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(unchecked((int)Convert.ToInt64(value)));
            }
        }
        public void WriteUInt64(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(unchecked((ulong)Convert.ToInt64(value)));
            }
        }
        public void WriteInt64(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(unchecked((long)Convert.ToInt64(value)));
            }
        }
        public void WriteSingle(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(Convert.ToSingle(value));
            }
        }
        public void WriteDouble(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(Convert.ToDouble(value));
            }
        }
        public void WriteAscii(object value)
        {
            WriteAscii(value, 1252);
        }
        public void WriteAscii(object value, int code_page)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }

                byte[] codepage_bytes = Encoding.GetEncoding(code_page).GetBytes(value.ToString());
                string utf7_value = Encoding.UTF7.GetString(codepage_bytes);
                byte[] bytes = Encoding.Default.GetBytes(utf7_value);

                m_Writer.Write((ushort)bytes.Length);
                m_Writer.Write(bytes);
            }
        }
        public void WriteUnicode(object value)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }

                byte[] bytes = Encoding.Unicode.GetBytes(value.ToString());

                m_Writer.Write((ushort)value.ToString().Length);
                m_Writer.Write(bytes);
            }
        }
        public void WriteUInt8Array(byte[] values)
        {
            if (m_Locked)
            {
                throw new Exception("Cannot Write to a locked Packet.");
            }
            m_Writer.Write(values);
        }
        public void WriteUInt8Array(byte[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    m_Writer.Write(values[x]);
                }
            }
        }
        public void WriteUInt16Array(UInt16[] values)
        {
            WriteUInt16Array(values, 0, values.Length);
        }
        public void WriteUInt16Array(UInt16[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    m_Writer.Write(values[x]);
                }
            }
        }
        public void WriteInt16Array(Int16[] values)
        {
            WriteInt16Array(values, 0, values.Length);
        }
        public void WriteInt16Array(Int16[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    m_Writer.Write(values[x]);
                }
            }
        }
        public void WriteUInt32Array(UInt32[] values)
        {
            WriteUInt32Array(values, 0, values.Length);
        }
        public void WriteUInt32Array(UInt32[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    m_Writer.Write(values[x]);
                }
            }
        }
        public void WriteInt32Array(Int32[] values)
        {
            WriteInt32Array(values, 0, values.Length);
        }
        public void WriteInt32Array(Int32[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    m_Writer.Write(values[x]);
                }
            }
        }
        public void WriteUInt64Array(UInt64[] values)
        {
            WriteUInt64Array(values, 0, values.Length);
        }
        public void WriteUInt64Array(UInt64[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    m_Writer.Write(values[x]);
                }
            }
        }
        public void WriteInt64Array(Int64[] values)
        {
            WriteInt64Array(values, 0, values.Length);
        }
        public void WriteInt64Array(Int64[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    m_Writer.Write(values[x]);
                }
            }
        }
        public void WriteSingleArray(float[] values)
        {
            WriteSingleArray(values, 0, values.Length);
        }
        public void WriteSingleArray(float[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    m_Writer.Write(values[x]);
                }
            }
        }
        public void WriteDoubleArray(double[] values)
        {
            WriteDoubleArray(values, 0, values.Length);
        }
        public void WriteDoubleArray(double[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    m_Writer.Write(values[x]);
                }
            }
        }
        public void WriteAsciiArray(String[] values, int codepage)
        {
            WriteAsciiArray(values, 0, values.Length, codepage);
        }
        public void WriteAsciiArray(String[] values, int index, int count, int codepage)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteAscii(values[x], codepage);
                }
            }
        }
        public void WriteAsciiArray(String[] values)
        {
            WriteAsciiArray(values, 0, values.Length, 1252);
        }
        public void WriteAsciiArray(String[] values, int index, int count)
        {
            WriteAsciiArray(values, index, count, 1252);
        }
        public void WriteUnicodeArray(String[] values)
        {
            WriteUnicodeArray(values, 0, values.Length);
        }
        public void WriteUnicodeArray(String[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteUnicode(values[x]);
                }
            }
        }

        public void WriteUInt8Array(object[] values)
        {
            WriteUInt8Array(values, 0, values.Length);
        }
        public void WriteUInt8Array(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteUInt8(values[x]);
                }
            }
        }
        public void WriteCharArray(char[] values)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                m_Writer.Write(values);
            }
        }
        public void WriteInt8Array(object[] values)
        {
            WriteInt8Array(values, 0, values.Length);
        }
        public void WriteInt8Array(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteInt8(values[x]);
                }
            }
        }
        public void WriteUInt16Array(object[] values)
        {
            WriteUInt16Array(values, 0, values.Length);
        }
        public void WriteUInt16Array(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteUInt16(values[x]);
                }
            }
        }
        public void WriteInt16Array(object[] values)
        {
            WriteInt16Array(values, 0, values.Length);
        }
        public void WriteInt16Array(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteInt16(values[x]);
                }
            }
        }
        public void WriteUInt32Array(object[] values)
        {
            WriteUInt32Array(values, 0, values.Length);
        }
        public void WriteUInt32Array(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteUInt32(values[x]);
                }
            }
        }
        public void WriteInt32Array(object[] values)
        {
            WriteInt32Array(values, 0, values.Length);
        }
        public void WriteInt32Array(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteInt32(values[x]);
                }
            }
        }
        public void WriteUInt64Array(object[] values)
        {
            WriteUInt64Array(values, 0, values.Length);
        }
        public void WriteUInt64Array(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteUInt64(values[x]);
                }
            }
        }
        public void WriteInt64Array(object[] values)
        {
            WriteInt64Array(values, 0, values.Length);
        }
        public void WriteInt64Array(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteInt64(values[x]);
                }
            }
        }
        public void WriteSingleArray(object[] values)
        {
            WriteSingleArray(values, 0, values.Length);
        }
        public void WriteSingleArray(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteSingle(values[x]);
                }
            }
        }
        public void WriteDoubleArray(object[] values)
        {
            WriteDoubleArray(values, 0, values.Length);
        }
        public void WriteDoubleArray(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteDouble(values[x]);
                }
            }
        }
        public void WriteAsciiArray(object[] values, int codepage)
        {
            WriteAsciiArray(values, 0, values.Length, codepage);
        }
        public void WriteAsciiArray(object[] values, int index, int count, int codepage)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteAscii(values[x].ToString(), codepage);
                }
            }
        }
        public void WriteAsciiArray(object[] values)
        {
            WriteAsciiArray(values, 0, values.Length, 1252);
        }
        public void WriteAsciiArray(object[] values, int index, int count)
        {
            WriteAsciiArray(values, index, count, 1252);
        }
        public void WriteUnicodeArray(object[] values)
        {
            WriteUnicodeArray(values, 0, values.Length);
        }

        public void WriteUnicodeArray(object[] values, int index, int count)
        {
            lock (m_Lock)
            {
                if (m_Locked)
                {
                    throw new Exception("Cannot Write to a locked Packet.");
                }
                for (int x = index; x < index + count; ++x)
                {
                    WriteUnicode(values[x].ToString());
                }
            }
        }
    }
}
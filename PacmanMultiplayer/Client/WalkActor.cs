﻿using static System.Reflection.MethodBase;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;
using System;
using Client;

namespace Client
{
    public abstract class WalkActor : Actor
    {
        protected MapDirection m_Direction;

        protected WalkActor(UInt32 id, int posX, int posY)
            : base(id, posX, posY)
        {
            m_Direction = MapDirection.NONE;
        }

        public override void Draw(Graphics g)
        {
            m_PointLocation.X = m_PosX * 30.00F - 15;
            m_PointLocation.Y = m_PosY * 30.00F - 15;
            /*
            m_PointLocation.X = m_GraphicsX * 30.00F - 15;
            m_PointLocation.Y = m_GraphicsY * 30.00F - 15;
            */
            g.DrawImage(m_Picture.Image, m_PointLocation);
        }

        protected void Move(float x, float y)
        {
            if (x < m_GraphicsX)
            {
                m_Direction = MapDirection.WEST;
            }
            else if (x > m_GraphicsX)
            {
                m_Direction = MapDirection.EAST;
            }
            else if (y < m_GraphicsY)
            {
                m_Direction = MapDirection.NORTH;
            }
            else // (y > m_GraphicsY)
            {
                m_Direction = MapDirection.SOUTH;
            }

            m_GraphicsX = x;
            m_GraphicsY = y;

            m_PointLocation.X = m_GraphicsX;
            m_PointLocation.Y = m_GraphicsY;

            switch (m_Direction)
            {
                case MapDirection.NORTH:
                    m_PosY = (int)Math.Ceiling(GrY);
                    break;

                case MapDirection.EAST:
                    m_PosX = (int)Math.Floor(GrX);
                    break;

                case MapDirection.SOUTH:
                    m_PosY = (int)Math.Floor(GrY);
                    break;

                case MapDirection.WEST:
                    m_PosX = (int)Math.Ceiling(GrX);
                    break;
            }
        }

        protected void move(int nextX, int nextY)
        {
            if (m_PosX < nextX)
            {
                m_Direction = MapDirection.EAST;
            }
            else if (m_PosX > nextX)
            {
                m_Direction = MapDirection.WEST;
            }
            else if (m_PosY > nextY)
            {
                m_Direction = MapDirection.NORTH;
            }
            else if (m_PosY < nextY)
            {
                m_Direction = MapDirection.SOUTH;
            }

            m_PosX = nextX;
            m_PosY = nextY;
        }
    }
}
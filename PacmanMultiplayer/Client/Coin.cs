﻿using System.Windows.Forms;
using System.Drawing;
using Client;
using System;

namespace Client
{

    public class Coin : Actor
    {
        private static ImageList m_ImagesList = null;


        public Coin(MapItem id, int posX, int posY)
            : base((UInt32)id, posX, posY)
        {
            m_Picture = new PictureBox();
            m_Picture.Name = "CoinImage" + m_Id;
            m_Picture.Size = new Size(8, 8);
            m_Picture.SizeMode = PictureBoxSizeMode.AutoSize;
            m_Picture.BackColor = Color.Black;

            if (m_ImagesList == null)
                loadImages();

            if (id == MapItem.BANUT_MIC)
                m_Picture.Image = m_ImagesList.Images[0];
            else
                m_Picture.Image = m_ImagesList.Images[1];
        }


        public override void Draw(Graphics g)
        {
            m_PointLocation.X = m_PosX * 30 - 15;
            m_PointLocation.Y = m_PosY * 30 - 15;

            g.DrawImage(m_Picture.Image, m_PointLocation);
        }


        private void loadImages()
        {
            m_ImagesList = new ImageList();

            m_ImagesList.Images.Add(
                Image.FromFile(Util.ProjectResPath + "Block1.png"));

            m_ImagesList.Images.Add(
                Image.FromFile(Util.ProjectResPath + "Block2.png"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public class ClientInfo
    {
        public UInt32 Id;
        public string Nick;

        public override string ToString()
        {
            return string.Format("#{0} {1}", Id, Nick);
        }
    }

    public class GameRoomInfo
    {
        public string Owner;
        public UInt32 Id;
        public int NoOfPlayers;
        public ListViewItem GameRoomInfoView;
        public List<ClientInfo> Players;

        public GameRoomInfo(UInt32 id, string owner, int numOfPlayers, List<ClientInfo> players)
        {
            Owner = owner;
            Id = id;
            NoOfPlayers = numOfPlayers;
            Players = players;

            GameRoomInfoView = new ListViewItem("#" + id + "  " + owner);
            GameRoomInfoView.SubItems.Add(numOfPlayers.ToString());
            GameRoomInfoView.ForeColor = Color.Black;
        }

        public void UpdateNumberOfPlayers(int numOfPlayers)
        {
            NoOfPlayers = numOfPlayers;
            GameRoomInfoView.SubItems[0].Text = NoOfPlayers.ToString();
        }

        /*
        public void UpdateView()
        {
            ListItemView.SubItems[1].Text = NoOfPlayers + ""; ;
        }
        */

        public override string ToString()
        {
            return string.Format("GameRoom[#{0} - {1}]", Id, Owner);
        }
    }
}
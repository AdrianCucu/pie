﻿using static System.Reflection.MethodBase;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections;
using System.Threading;
using System.Drawing;
using System.Linq;
using System.Data;
using System.Text;
using System;
using Client;
using Network;

namespace Client
{
    public partial class GameWindow : Form
    {
        private const int FPS = 15;

        private const int HIGHSCORE_LABEL_HEIGHT = 50;

        private static GameWindow mInstance = null;
        //private static bool mIsShown;

        /*
        private double timePerTick;
        private double delta;
        private long currTime;
        private long lastTime;
        */

        private static GameBoard m_GameBoard;
        private static List<Coin> m_CoinsList;
        private static Dictionary<UInt32, Pacman> m_PacmansMap;
        private static Dictionary<UInt32, Ghost> m_GhostsMap;

        private static readonly ConcurrentQueue<Packet> queue = new ConcurrentQueue<Packet>();

        private static Keys m_LastKey = Keys.None;

        private static UInt32 gameTime;

        private static Client mClient;

        private static Thread m_PaintThread;

        private static Graphics m_Graphics;

        private System.Timers.Timer m_Timer;

        private static bool m_GameOn;

        private static int m_FormWidth;
        private static int m_FormHeight;

        private static Font m_DrawFont;
        private static SolidBrush m_DrawWhiteBrush;
        private static SolidBrush m_DrawBlackBrush;
        private static StringFormat m_DrawStringFormat;

        public bool IsOpen
        {
            get => m_GameOn;
        }

        //public bool IsShown
        //{
        //    get => mIsShown;
        //}

        public static GameWindow Instance
        {
            get
            {
                if (mInstance == null)
                {
                    Console.WriteLine("Creating thge instance");
                    mInstance = new GameWindow();
                }
                //mInstance.Show();
                //mInstance.Focus();

                return mInstance;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) 
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            if (m_PaintThread != null)
            {
                m_PaintThread.Abort();
            }

            base.Dispose(disposing);
            mInstance = null;
        }

        private void GameWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;

            m_GameOn = false;
            if (m_PaintThread != null)
            {
                m_PaintThread.Abort();
            }

            Hide();
            ClientForm.GameOpened = false;

            e.Cancel = false;

            Console.WriteLine("Game Window closed....");
        }

        //public new void Show()
        //{
        //    if (mIsShown)
        //    {
        //        base.Show();
        //    }
        //    else
        //    {
        //        base.Show();
        //        mIsShown = true;
        //    }
        //}

        private GameWindow()
        {
            InitializeComponent();
            // Initialize game components
            //mIsShown = false;

            //this.SuspendLayout();
            Text = "Pacman --";

            m_GameBoard = new GameBoard();

            m_FormWidth  = m_GameBoard.Width;
            m_FormHeight = m_GameBoard.Height + HIGHSCORE_LABEL_HEIGHT;

            ClientSize = new Size(m_FormWidth, m_FormHeight);

            // Add picture box in which graphics context will be
            PictureBox pic = new PictureBox();
            pic.BackColor = Color.DarkOrange;
            pic.Size = new Size(m_FormWidth, m_FormHeight);
            pic.Location = new Point(0, 0);
            this.Controls.Add(pic);

            // Create graphics context		
            m_Graphics = pic.CreateGraphics();
            m_Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            m_Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

            m_DrawFont = new Font("Arial", 16);
            m_DrawWhiteBrush = new SolidBrush(Color.White);
            m_DrawBlackBrush = new SolidBrush(Color.Black);
            m_DrawStringFormat = new StringFormat();
            Console.WriteLine("Setting the m_GameOn = false for #{0}", mClient.Id);
            m_GameOn = false;
            //m_PaintThread = new Thread(new ThreadStart(paintLoop));

            //m_PacmansMap = new Dictionary<UInt32, Pacman>();
            //m_GhostsMap = new Dictionary<UInt32, Ghost>();
            //m_CoinsList = new List<Coin>();
        }

        public static void InitGame()
        {
            m_PacmansMap = new Dictionary<UInt32, Pacman>();
            m_GhostsMap = new Dictionary<UInt32, Ghost>();
            m_CoinsList = new List<Coin>();
            m_GameOn = true;
            Console.WriteLine("[DEBUG] GameWindow InitGame");
        }

        public void StartGame(UInt32 seconds)
        {
            Console.WriteLine("Starting the game in 5 seconds...");
            return;
            mInstance.ShowDialog();
            
            Thread.Sleep(5000);
            //m_Graphics.FillRectangle(new SolidBrush(Color.Red), 0, 0, 100, 200);
            
            //Application.Run(mInstance);
            Console.WriteLine("Done...");
            return;
            try
            {
                if (mInstance != null)
                {
                    if (mInstance.InvokeRequired)
                    {
                        // Synchronous
                        //mInstance.Invoke((Action)delegate { SafeInvoke(uiElement, updater, forceSynchronous); });
                        //mInstance.BeginInvoke((Action)delegate { SafeInvoke(uiElement, updater, forceSynchronous); });
                        mInstance.Invoke(new Action(() => { mInstance.Show(); }));
                    }
                    else
                    {
                        if (mInstance.IsDisposed)
                        {
                            throw new ObjectDisposedException("Control is already disposed.");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("mInstance is NULL !!!!!!!!!!!!!!!!!!!!!!!!!!!");
                }

                this.Invoke(new Action(() => mInstance.Show()));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} -> {1}", GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }

            return;
            m_GameOn = true;
            m_PaintThread.Start();
            Console.WriteLine("The game will last {0} seconds.", seconds);
            /*
            gameTime = seconds;
            m_Timer = new System.Timers.Timer(gameTime * 1000);
            m_Timer.Elapsed += new System.Timers.ElapsedEventHandler(gameOver);
            m_Timer.Enabled = true;
            */
        }

        public void GameOver()
        {
            GameClosed();
            //m_Timer.Enabled = false;
        }

        public void GameClosed()
        {
            try
            {
                m_GameOn = false;

                if (m_PaintThread != null && m_PaintThread.IsAlive)
                {
                    m_PaintThread.Abort("Game closed");
                    m_PaintThread.Join();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0}::@::{1}",
                    GetCurrentMethod().Name, GetType().FullName);
                Console.WriteLine(ex);
                Console.WriteLine("Exception in {0}::@::{1}",
                    GetCurrentMethod().Name, GetType().FullName);
            }
        }

        public static void StartGameWindow(UInt32 seconds)
        {
            Console.WriteLine("Starting the game #{0}...", mClient.Id);
            m_GameOn = true;
            m_PaintThread = new Thread(new ThreadStart(paintLoop));
            m_PaintThread.Start();
            Console.WriteLine("The game will last {0} seconds.", seconds);
            /*
            gameTime = seconds;
            m_Timer = new System.Timers.Timer(gameTime * 1000);
            m_Timer.Elapsed += new System.Timers.ElapsedEventHandler(gameOver);
            m_Timer.Enabled = true;
            */
        }

        private void gameOver(object sender, EventArgs e)
        {
            /*
            string scoreMessage = String.Empty;
            foreach (KeyValuePair<UInt32, Pacman> mapIdPacman
                in m_PacmansMap)
            {
                scoreMessage += String.Format("{0} score: {1}\n",
                            mapIdPacman.Value.Nick,
                            mapIdPacman.Value.Score
                );
            }
            MessageBox.Show("-- Game Over --" + "\n" + scoreMessage);
            m_Timer.Stop();
            */
            m_Timer.Stop();

            foreach (KeyValuePair<UInt32, Pacman> mapIdPacman
                in m_PacmansMap)
            {
                Console.WriteLine("{0}) {1} score {2}.",
                    mapIdPacman.Value.Id,
                    mapIdPacman.Value.Nick,
                    mapIdPacman.Value.Score
                );
            }
        }

        public void DisposeGame()
        {
            Dispose(true);
        }

        private static void drawScore()
        {
            float x = 5.00F;
            float y = 5.00F;

            m_Graphics.FillRectangle(
                m_DrawBlackBrush, 0, 0, m_FormWidth, HIGHSCORE_LABEL_HEIGHT);

            foreach (KeyValuePair<UInt32, Pacman> mapIdPacman in m_PacmansMap)
            {
                string drawScore = String.Format("{0}: {1}", mapIdPacman.Value.Nick, mapIdPacman.Value.Score);

                m_Graphics.DrawString(
                     drawScore, m_DrawFont, m_DrawWhiteBrush, x, y, m_DrawStringFormat);
                x += 200.00F;
            }
        }

        /*
        private static long nanoTime()
        {
            long nano = 10000L * Stopwatch.GetTimestamp();
            nano /= TimeSpan.TicksPerMillisecond;
            nano *= 100L;
            return nano;
        }
        */

        private static void paintLoop()
        {
            //Functia doar deseneaza toate elementele jocului:
            //Face frame swap pentru a nu mai flicari imaginea
            //Prima data deseneaza pe un bitmap 'newFrame':
            //Componentele in ordine asta:
            // - Harta jocului
            // - Monezile
            // - Playerii(Pacmanii)
            // - Fantomele
            // dupa deseneaza frame-ul pe care au fost desenate 
            // toate acestea peste un alt frame	
            // si va face acest lucru de mai multe ori pe secunda	
            // in functie de catr FPS-uri are jocul

            PointF pointF = new PointF(-1, -1);
            Bitmap newFrame;
            Graphics g;

            double timePerTick;
            double delta;
            long currTime;
            long lastTime;

            delta = 0;
            timePerTick = 1000000000.00 / FPS;
            lastTime = DateTime.Now.Ticks * 100;

            long last_time = Environment.TickCount;
            long current_time;
            int framesRendered = 0;

            try
            {
                while (m_GameOn)
                {
                    //Packet pack;
                    //if (queue.TryDequeue(out pack))
                    //{
                    //    Console.WriteLine("[DEBUG] paintLoop [am pachet]  => {0}", mClient.Id);
                    //} else
                    //{
                    //    Console.WriteLine("[DEBUG] paintLoop [NUam pachet] => {0}", mClient.Id);
                    //}
                    //Thread.Sleep(500);
                    //continue;
                    ///////////////////////////////////////////////////
                    Packet p;
                    while (queue.TryDequeue(out p))
                    {
                        switch (p.Type)
                        {
                            case PacketType.GameInfo.PLAYER_DIRECTION:
                                {
                                    int num_of_players = p.ReadInt32();

                                    while (num_of_players-- > 0)
                                    {
                                        UInt32 playerId = p.ReadUInt32();
                                        int pX = p.ReadInt32();
                                        int pY = p.ReadInt32();
                                        int score = p.ReadInt32();

                                        movePacman(playerId, pX, pY, score);
                                    }
                                }
                                break;

                            case PacketType.GameInfo.GHOST_DIRECTION:
                                {
                                    int num_of_ghosts = p.ReadInt32();

                                    while (num_of_ghosts-- > 0)
                                    {
                                        UInt32 ghostVal = p.ReadUInt32();
                                        int ghostX = p.ReadInt32();
                                        int ghostY = p.ReadInt32();
                                        //float ghostX = p.ReadSingle();        
                                        //float ghostY = p.ReadSingle();        

                                        MapItem coinType = (MapItem)p.ReadUInt8();

                                        if (coinType == MapItem.BANUT_MIC || coinType == MapItem.BANUT_MARE)
                                        {
                                            int coinY = p.ReadInt32();
                                            int coinX = p.ReadInt32();

                                            addCoin(coinY, coinX, coinType);
                                        }
                                        moveGhost(ghostVal, ghostX, ghostY);
                                    }
                                }
                                break;

                            case PacketType.GameInfo.PLAYER_RESPAWN:
                                {
                                    UInt32 playerId = p.ReadUInt32();

                                    respawnPlayer(playerId);
                                }
                                break;

                            default:
                                break;
                        }
                    }
                    
                    //////////////////////////////////////////////////
                    currTime = DateTime.Now.Ticks * 100;
                    delta += (currTime - lastTime) / timePerTick;
                    lastTime = currTime;

                    current_time = Environment.TickCount;

                    if (current_time >= last_time + 1000)
                    {
                        //Console.WriteLine("{0} fps", framesRendered);
                        framesRendered = 0;
                        last_time = current_time;
                    }

                    if (delta < 1)
                        continue;

                    --delta;

                    newFrame = new Bitmap(m_GameBoard.Width, m_GameBoard.Height);
                    g = Graphics.FromImage(newFrame);
                    g.DrawImage(m_GameBoard.Picture.Image, 0, 0);

                    foreach (Coin coin in m_CoinsList)
                        coin.Draw(g);

                    foreach (KeyValuePair<UInt32, Pacman> mapIdPacman in m_PacmansMap)
                    {
                        if (mapIdPacman.Value.IsMoving)
                            mapIdPacman.Value.UpdatePacmanImage();
                        mapIdPacman.Value.Draw(g);
                    }

                    foreach (KeyValuePair<UInt32, Ghost> mapIdGhost in m_GhostsMap)
                        mapIdGhost.Value.Draw(g);

                    drawScore();
                    //Noul frame 'newFrame' deseneaza-l peste contextul grafic
                    m_Graphics.DrawImage(newFrame, 0, HIGHSCORE_LABEL_HEIGHT);
                    ++framesRendered;
                }
                //draw frame loop
            }
            catch (ThreadAbortException abortException)
            {

                Console.WriteLine("[DEBUG] {0} #4 m_GameOn = {1}", GetCurrentMethod().Name, m_GameOn);
                Console.WriteLine((string)abortException.ExceptionState);
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception in {0} ->{1}", mInstance.GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }


            Console.WriteLine("[DEBUG] {0} #5 m_GameOn = {1}", GetCurrentMethod().Name, m_GameOn);
            Console.WriteLine("<---------> Game Ended <--------->");
        }

        public static void SetClient(Client client)
        {
            mClient = client;
        }

        private static void addCoin(int y, int x, MapItem tipBanut)
        {
            try
            {
                lock (/*this*/ mInstance)
                {
                    Coin newCoin = new Coin(tipBanut, x, y);
                    m_CoinsList.Add(newCoin);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} ->{1}", mInstance.GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        public static void AddPlayer(UInt32 id, int x, int y, string nickname)
        {
            try
            {
               // lock (/*this*/ mInstance)
                //{
                    Pacman newPacman = new Pacman(id, x, y, nickname);
                    m_PacmansMap.Add(id, newPacman);
               // }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception in {0} ->{1}", mInstance.GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        public static void AddGhost(UInt32 id, int x, int y)
        {
            try
            {
               // lock (/*this*/ mInstance)
                //{
                    Ghost newGhost = new Ghost(id, x, y);
                    //m_GhostsList.Add(newGhost);
                    m_GhostsMap.Add(id, newGhost);
               // }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Exception in {0} ->{1}", mInstance.GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        public static void AddToPacketQueue(Packet p)
        {
            queue.Enqueue(p);
        }

        private static void movePacman(UInt32 id, int x, int y, int score)
        {
            try
            {
                Pacman pacman;

                if (m_PacmansMap.TryGetValue(id, out pacman))
                {
                    //Console.WriteLine("Player #{0} ==> X: {1}, Y: {2}", pacman.Id, pacman.X, pacman.Y);
                    pacman.Move(x, y);
                    if (score > 0)
                    {
                        foreach (Coin coin in m_CoinsList)
                        {
                            if (coin.X == x && coin.Y == y)
                            {
                                m_CoinsList.Remove(coin);
                                break;
                            }
                        }
                        pacman.UpdateScore(score);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} ->{1}", mInstance.GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        private static void moveGhost(UInt32 id, int x, int y)
        {
            try
            {
                Ghost ghost;
                if (m_GhostsMap.TryGetValue(id, out ghost))
                {
                    ghost.Move(x, y);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} ->{1}", mInstance.GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        private static void respawnPlayer(UInt32 id)
        {
            try
            {
                Pacman pacman;
                if (m_PacmansMap.TryGetValue(id, out pacman))
                {
                    pacman.Respawn();
                    m_LastKey = Keys.None;

                    foreach (Coin coin in m_CoinsList)
                    {

                        if (coin.X == pacman.X && coin.Y == pacman.Y)
                        {
                            m_CoinsList.Remove(coin);
                            break;

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} ->{1}", mInstance.GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            MapDirection dir = MapDirection.NONE;
            //base.OnKeyDown(e);

            switch (e.KeyCode)
            {
                case Keys.Up:
                case Keys.W:
                    dir = MapDirection.NORTH;
                    break;

                case Keys.Right:
                case Keys.D:
                    dir = MapDirection.EAST;
                    break;

                case Keys.Down:
                case Keys.S:
                    dir = MapDirection.SOUTH;
                    break;

                case Keys.Left:
                case Keys.A:
                    dir = MapDirection.WEST;
                    break;

                default: return;
            }

            if (e.KeyCode != m_LastKey)
            {
                if (mClient != null)
                {
                    mClient.SendMoveRequest(dir);
                }

                m_LastKey = e.KeyCode;
            }
        }

        private void GameWindow_Load(object sender, EventArgs e)
        {
            Console.WriteLine("Form loaded: #{0}", mClient.Id);
        }
    }
}


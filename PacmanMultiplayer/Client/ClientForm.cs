﻿using System;
using System.Collections.Generic;
using static System.Reflection.MethodBase;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Net;
using System;
using Network;

namespace Client
{
    public partial class ClientForm : Form
    {
        public static bool GameOpened = false;

        private Client mClient;
        //private GameWindow gameWindow = new GameWindow();
        //private Dictionary<UInt32, ListViewItem> gameRooms;

        //private Dictionary<UInt32, GameRoomInfo> allGameRoomsMap;
        //private List<GameRoomInfo> allGameRooms;

        private List<GameRoomInfo> mGameRooms = new List<GameRoomInfo>();

        //private GameRoom currentGameRoom;

        public ClientForm()
        {
            InitializeComponent();
            this.Text += "[NOT CONNECTED]";
            //gameRooms = new Dictionary<UInt32, ListViewItem>();

            //allGameRooms = new List<GameRoomInfo>();
            //allGameRoomsMap = new Dictionary<UInt32, GameRoomInfo>();

            System.Windows.Forms.ColumnHeader columnOwner = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            System.Windows.Forms.ColumnHeader columnNoPlayers = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnOwner.Text = "Owner";
            columnOwner.Width = 200;
            columnNoPlayers.Text = "Number of players";
            columnNoPlayers.Width = 100;

            gameRoomsList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { columnOwner, columnNoPlayers });
        }

        protected override void Dispose(bool disposing)
        {
            Console.WriteLine("Disposing...");

            if (mClient != null && mClient.Connected)
                mClient.Disconnect(true);

            //if (gameWindow != null && gameWindow.IsOpen)
            //    gameWindow.GameClosed();

            if (disposing && (components != null))
                components.Dispose();

            base.Dispose(disposing);
        }

        public void Play()
        {
            buttonPlay.Enabled = true;
        }

        private void buttonCreateGameRoom_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Let's Create room!");
            if (mClient == null || !mClient.Connected)
            {
                ErrorMessageBox("You are not connected!");
                return;
            }

            mClient.SendCreateGameRoomRequest();

            /*
            List<UInt32> player = new List<UInt32>();
            player.Add(1);

            AddGameRoom(1, player);
            */
        }

        private void buttonJoinGameRoom_Click(object sender, EventArgs e)
        {
            buttonJoinGameRoom.Enabled = false;

            if (mClient == null || !mClient.Connected)
            {
                ErrorMessageBox("You are not connected!");
                return;
            }
            if (this.gameRoomsList.SelectedItems.Count == 0)
            {
                ErrorMessageBox("No room selected");
            }
            else
            {
                foreach (GameRoomInfo gameRoom in mGameRooms)
                {
                    if (gameRoom.GameRoomInfoView == gameRoomsList.SelectedItems[0])
                    {
                        Console.WriteLine(gameRoom.Id + " " + gameRoom.NoOfPlayers + " " + gameRoom.Owner);
                        mClient.SendJoinGameRoomRequest(gameRoom.Id);
                        break;
                    }
                }
                
                /*
                UInt32 ownerId = getSelectedRoomOwnerId(this.gameRoomsList.SelectedItems[0]);
                Console.WriteLine("Need to send request to Join room: #{0}", ownerId);
                if (ownerId != 0)
                {
                    m_Client.SendJoinGameRoomRequest(ownerId);

                    Console.WriteLine("Sending request..");
                }
                else
                {

                    Console.WriteLine("NOT Sending request..");
                }
                */

                //UInt32 roomId = getSelectedRoomId(this.gameRoomsList.SelectedItems[0]);
                //if (roomId != 0)
                //{
                //    Console.WriteLine("Need to send request to Join room: #{0}", roomId);
                //    mClient.SendJoinGameRoomRequest(roomId);
                //}
            }
        }

        //private UInt32 getSelectedRoomId(ListViewItem selectedItem)
        //{
        //    UInt32 roomId = 0;
        //    foreach (KeyValuePair<UInt32, GameRoomInfo> pair in this.allGameRoomsMap)
        //    {
        //        if (pair.Value.ListItemView == selectedItem)
        //        {
        //            roomId = pair.Key;
        //        }
        //    }
        //    return roomId;
        //}

        /*
        private void buttonCloseGameRoom_Click(object sender, EventArgs e)
        {
            buttonCloseGameRoom.Enabled = false;

            if (mClient == null || !mClient.Connected)
            {
                ErrorMessageBox("You are not connected!");
                return;
            }
            Console.WriteLine("Let's close room!");
            mClient.SendCloseGameRoomRequest();
            ResetRoom();
        }
        */

        private void buttonExitGameRoom_Click(object sender, EventArgs e)
        {
            if (mClient == null || !mClient.Connected)
            {
                ErrorMessageBox("You are not connected!");
                return;
            }

            Console.WriteLine("Let's EXIT room!");

            mClient.SendExitGameRoomRequest();
            buttonExitGameRoom.Enabled = false;
            ResetRoom();
        }

        private void OnLobbyGameRoomCreated(object sender, GameRoomInfo gameRoom)
        {
            this.Invoke(new Action(() => {
                mGameRooms.Add(gameRoom);
                gameRoomsList.Items.Add(gameRoom.GameRoomInfoView);
            }));
        }

        private void OnLobbyGameRoomUpdated(object sender, GameRoomInfo gameRoom)
        {
            this.Invoke(new Action(() =>
            {
                foreach (GameRoomInfo room in mGameRooms)
                {
                    if (room.Id == gameRoom.Id)
                    {
                        gameRoom.UpdateNumberOfPlayers(gameRoom.NoOfPlayers);
                    }
                }
            }));
        }

        private void OnLobbyGameRoomAck(object sender, List<GameRoomInfo> gameRooms)
        {
            this.Invoke(new Action(() =>
            {
                Console.WriteLine("GAME rooms: {0}", gameRooms.Count);
                ClearGameRoomsList();
                mGameRooms = gameRooms;

                if (gameRooms.Count > 0)
                {
                    foreach (GameRoomInfo room in gameRooms)
                    {
                        gameRoomsList.Items.Add(room.GameRoomInfoView);
                    }
                }
            }));
        }
        
        private void OnLobbyGameRoomDeleted(object sender, GameRoomInfo gameRoom)
        {
            this.Invoke(new Action(() =>
            {
                foreach (GameRoomInfo room in mGameRooms)
                {
                    if (room.Id == gameRoom.Id)
                    {
                        gameRoomsList.Items.Remove(room.GameRoomInfoView);
                        mGameRooms.Remove(room);
                        break;
                    }
                }
            }));
        }

        private void OnDisconnected(object sender, string message)
        {
            this.Invoke(new Action(() => {
                //StopGame();
                buttonConnect.Enabled = true;
                buttonDisconnect.Enabled = false;
                buttonSendMessage.Enabled = false;
                buttonPlay.Enabled = false;

                textBoxServerIp.ReadOnly = false;
                textBoxServerPort.ReadOnly = false;
                textBoxNick.ReadOnly = false;

                ClearGameRoomsList();

                labelRoom.Text = "";
                listGameRoomPlayers.Items.Clear();
                buttonPlay.Enabled = false;
                
                buttonCreateGameRoom.Enabled = false;
                
                buttonExitGameRoom.Enabled = false;
                textBoxGameRoomSendMessage.Enabled = false;
                textBoxGameChat.Clear();
                listBoxClients.Items.Clear();

                //listBoxClients.Items.Clear();
                //gameRoomsList.Items.Clear();
                //this.ResetRoom();
                Text = "Disconnected";
                this.ErrorMessageBox(message);
            }));
        }

        //public void AddNewGameRoomCreated(UInt32 roomId, string ownerNick, int noOfPlayers = 1)
        //{
            //this.Invoke(new Action(() =>
            //{
            //    lock (this)
            //    {
            //        GameRoomInfo gameRoomInfo;

            //        if (allGameRoomsMap.TryGetValue(roomId, out gameRoomInfo))
            //        {
            //            gameRoomInfo.NoOfPlayers = noOfPlayers;
            //            gameRoomInfo.UpdateView();
            //        }
            //        else
            //        {
            //            gameRoomInfo = new GameRoomInfo();
            //            gameRoomInfo.Id = roomId;
            //            gameRoomInfo.Owner = ownerNick;
            //            gameRoomInfo.NoOfPlayers = noOfPlayers;
            //            gameRoomInfo.ListItemView = new ListViewItem(ownerNick);
            //            gameRoomInfo.ListItemView.ForeColor = Color.Black;
            //            gameRoomInfo.ListItemView.SubItems.Add(noOfPlayers + "");
            //            allGameRooms.Add(gameRoomInfo);
            //            allGameRoomsMap.Add(roomId, gameRoomInfo);
            //        }
            //    }
            //    this.refreshGameRoomsList();
            //}));
        //}
        
        //public void ClearGameRooms()
        //{
        //    allGameRoomsMap.Clear();
        //    allGameRooms.Clear();
        //}
        
        //public void UpdateGameRoom(UInt32 roomId, int noOfPlayers)
        //{
            /*
            lock (this)
            {
                GameRoomInfo info = allGameRoomsMap[roomId];
                info.NoOfPlayers = noOfPlayers;
                info.UpdateView();
            }

            this.Invoke(new Action(() =>
            {
                this.refreshGameRoomsList();
            }));
            */

            // Update the game room object 
            //GameRoomInfo gameRoomInfo = allGameRoomsMap[roomId];
            //gameRoomInfo.NoOfPlayers = noOfPlayers;
            //gameRoomInfo.UpdateView();

            //if (!this.InvokeRequired)
            //{
            //    refreshGameRoomsList();
            //}
            //else
            //{
            //    Action a = new Action(() => refreshGameRoomsList());
            //    this.Invoke(a);
            //}
        //}
        
        //public void DeleteGameRoom(UInt32 roomId)
        //{
            /*
            this.gameRoomsList.Items.Remove(gameRooms[ownerId]);
            gameRooms.Remove(ownerId);
            */
            /*
            lock (this)
            {
                GameRoomInfo info = allGameRoomsMap[roomId];
                allGameRooms.Remove(info);
                allGameRoomsMap.Remove(roomId);
            }

            this.Invoke(new Action(() =>
            {
                this.refreshGameRoomsList();
            }));
            */

            //GameRoomInfo gameRoomInfo = allGameRoomsMap[roomId];
            //allGameRooms.Remove(gameRoomInfo);
            //allGameRoomsMap.Remove(roomId);

            //if (!this.InvokeRequired)
            //{
            //    refreshGameRoomsList();
            //}
            //else
            //{
            //    Action a = new Action(() => refreshGameRoomsList());
            //    this.Invoke(a);
            //}
        //}

        public void ClearGameRoomsList()
        {
            gameRoomsList.Items.Clear();
            mGameRooms.Clear();
            /*
            this.Invoke(new Action(() => {
                this.allGameRooms.Clear();
                this.allGameRoomsMap.Clear();
                this.gameRoomsList.Items.Clear();
            })); 
            */
            //if (!this.InvokeRequired)
            //{
            //    clearGameRoomsList();
            //}
            //else
            //{
            //    Action a = new Action(() => clearGameRoomsList());
            //    this.Invoke(a);
            //}
        }

        //private void clearGameRoomsList()
        //{
        //    allGameRooms.Clear();
        //    allGameRoomsMap.Clear();
        //    gameRoomsList.Items.Clear();
        //}
        
        //private void refreshGameRoomsList()
        //{
            //gameRoomsList.Items.Clear();

            //foreach (GameRoomInfo gameRoomInfo in allGameRooms)
            //    gameRoomsList.Items.Add(gameRoomInfo.ListItemView);
        //}

        /*
        public void AddGameRoom(UInt32 roomId, string ownerNick)
        {
            this.Invoke(new Action(() =>
            {
                ListViewItem item = new ListViewItem(ownerNick);
                item.ForeColor = Color.Black;
                item.SubItems.Add("1");
                this.gameRoomsList.Items.Add(item);
                gameRooms.Add(roomId, item);
                this.gameRoomsList.Refresh();
                Console.WriteLine("count: {0}", this.gameRoomsList.Items.Count);
            }));
        }*/

        /*
        public void ClientLeftGameRoom(UInt32 roomId)
        {
            ListViewItem listItem = gameRooms[roomId];
            int n = Int32.Parse(listItem.SubItems[1].Text);
            listItem.SubItems[1].Text = String.Format("{0}", n + 1);
        }
        */

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (textBoxServerIp.Text == null || String.IsNullOrEmpty(textBoxServerIp.Text.Trim()))
            {
                ErrorMessageBox("Please enter a valid IP address");
                return;
            }

            if (textBoxServerPort.Text == null || String.IsNullOrEmpty(textBoxServerPort.Text.Trim()))
            {
                ErrorMessageBox("Please enter a PORT number");
                return;
            }

            if (textBoxNick.Text == null || String.IsNullOrEmpty(textBoxNick.Text.Trim()))
            {
                ErrorMessageBox("Please choose a nickname");
                return;
            }

            try
            {
                IPAddress ip = IPAddress.Parse(textBoxServerIp.Text);
                int port = Int32.Parse(textBoxServerPort.Text);
                string nickname = textBoxNick.Text;

                Console.WriteLine("===============================");
                Console.WriteLine("IP  : {0}", ip);
                Console.WriteLine("PORT: {0}", port);
                Console.WriteLine("NICK: {0}", nickname);
                Console.WriteLine("===============================");

                Console.WriteLine("Debug [1]");

                if (mClient == null)
                {
                    //TODO
                    mClient = new Client();
                    Client.OnLobbyGameRoomCreated = this.OnLobbyGameRoomCreated;
                    Client.OnLobbyGameRoomUpdated = this.OnLobbyGameRoomUpdated;
                    Client.OnLobbyGameRoomDeleted = this.OnLobbyGameRoomDeleted;

                    Client.OnLobbyGameRoomAck = this.OnLobbyGameRoomAck;

                    Client.OnGameRoomCreated = this.OnGameRoomCreated;
                    Client.OnGameRoomJoined = this.OnGameRoomJoined;

                    Client.OnOwnGameRoomClientJoined = this.OnOwnGameRoomClientJoined;
                    Client.OnOwnGameRoomClientLeft = this.OnOwnGameRoomClientLeft;
                    Client.OnOwnGameRoomMessage = this.OnOwnGameRoomMessage;
                    Client.OnOwnGameRoomClosed = this.OnOwnGameRoomClosed;
                    
                    Client.OnDisconnect = this.OnDisconnected;
                    Client.OnOtherClientInfoReceived = this.OnOtherClientInfoReceived;

                    Client.OnPrivateMessageReceived = this.OnPrivateMessageReceived;

                    Client.OnClientLeft = this.OnClientLeft;

                    Client.OnError = this.OnError;

                    Client.OnGameStart = this.OnGameStart;
                }

                Console.WriteLine("Debug [2]");

                if (mClient.Connect(ip, port, nickname))
                {

                    Console.WriteLine("Debug [3]");
                    SuccessText(mClient.ToString());

                    buttonConnect.Enabled = false;
                    buttonDisconnect.Enabled = true;
                    textBoxServerIp.ReadOnly = true;
                    textBoxServerPort.ReadOnly = true;
                    textBoxNick.ReadOnly = true;
                    //Text =
                    //    String.Format("{0}:[{1}] <==> {2}:[{3}]  ID: #{4} Nick: {5}",
                    //    m_Client.LocalIp, m_Client.LocalPort,
                    //    m_Client.RemoteIp, m_Client.RemotePort,
                    //    m_Client.Id, m_Client.Nick
                    //);
                    base.Text = mClient.ToString();
                }
                else
                {

                    Console.WriteLine("Debug [4]");
                    switch (mClient.Error)
                    {
                        case PacketType.ConnectionError.NICKNAME_INVALID:
                            ErrorMessageBox(
                                String.Format(
                                    "Nickname '{0}' is invalid", nickname));
                            break;

                        case PacketType.ConnectionError.NICKNAME_TAKEN:
                            ErrorMessageBox(
                                String.Format(
                                    "Nickname '{0}' is taken", nickname));
                            break;

                        case PacketType.ConnectionError.NICKNAME_TOO_LONG:
                        case PacketType.ConnectionError.NICKNAME_TOO_SHORT:
                            ErrorMessageBox("Nickname must have between 3 and 20");
                            break;

                        case PacketType.ConnectionError.SERVER_FULL:
                            ErrorMessageBox("Server is FULL");
                            break;

                        default:
                            ErrorMessageBox("Failed to connect");
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0}::@::{1}",
                    GetCurrentMethod().Name, GetType().FullName);
                Console.WriteLine(ex);
                Console.WriteLine("Exception in {0}::@::{1}",
                    GetCurrentMethod().Name, GetType().FullName);

                ErrorMessageBox(ex.Message);
            }
        }

        private void OnPrivateMessageReceived(object sender,  Tuple<string, string> msg)
        {
            textBoxInfo.Invoke(new Action(() => MsgText(msg.Item1 + "  " + msg.Item2)));
        }
        
        private void listBoxClients_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBoxClients.SelectedItem == null)
                buttonSendMessage.Enabled = false;
            else
                buttonSendMessage.Enabled = true;
        }
        
        private void buttonSendMessage_Click(object sender, EventArgs e)
        {
            if (textBoxMessage == null || String.IsNullOrEmpty(textBoxMessage.Text) || String.IsNullOrEmpty(textBoxMessage.Text.Trim()))
            {
                return;
            }

            string item = listBoxClients.GetItemText(listBoxClients.SelectedItem);

            if (item == null)
                return;

            string msg = textBoxMessage.Text.Trim();
            string str = Regex.Match(item, @"\[([0-9]*)\]").Groups[1].Value;

            UInt32 id = UInt32.Parse(str);

            Console.WriteLine(id + " => " + msg);

            mClient.SendPrivateMessage(id, msg);
            textBoxMessage.Text = null;
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            if (mClient != null && mClient.Connected)
            {
                mClient.Disconnect(false);
               /*
                //StopGame();
                buttonConnect.Enabled = true;
                buttonDisconnect.Enabled = false;
                buttonSendMessage.Enabled = false;
                buttonPlay.Enabled = false;

                textBoxServerIp.ReadOnly = false;
                textBoxServerPort.ReadOnly = false;
                textBoxNick.ReadOnly = false;

                //listBoxClients.Items.Clear();
                //gameRoomsList.Items.Clear();
                //this.ResetRoom();
                Text = "Disconnected";
                */
            }
        }

        /*
        public void listIDs(string s)
        {
            try
            {
                Invoke(new Action(() =>
                    {
                        foreach (string id in s.Split(':'))
                        {
                            if (id != "")
                            {
                                listBoxClients.Items.Add(
                        String.Format("[{0}] User{1}", id, id)
                    );
                            }
                        }
                    }));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0}::@::{1}",
                    GetCurrentMethod().Name, GetType().FullName);
                Console.WriteLine(ex);
                Console.WriteLine("Exception in {0}::@::{1}",
                    GetCurrentMethod().Name, GetType().FullName);

            }
        }
        */

        /*
        public void AddId(UInt32 id)
        {
            Invoke(new Action(() =>
            {
                listBoxClients.Items.Add(
            String.Format("[{0}] User{1}", id, id)
        );
            }));
        }
        */

        private void OnClientLeft(object sender, ClientInfo client)
        {
            Invoke(new Action(() =>
            {
                Refresh(client.Id);
            }));
        }

        private void OnOtherClientInfoReceived(object sender, ClientInfo client)
        {
            Invoke(new Action(() =>
            {
                listBoxClients.Items.Add(
                    String.Format("[{0}]: {1}", client.Id, client.Nick)
                );
            }));
        }

        /*
        public void AddClient(UInt32 id, string nick)
        {
            Invoke(new Action(() =>
            {
                listBoxClients.Items.Add(
                    String.Format("[{0}]: {1}", id, nick)
                );
            }));
        }
        */
        
        
        public void Refresh()
        {
            Invoke(new Action(() =>
            {
                listBoxClients.Items.Clear();
                buttonDisconnect.Enabled = false;
                buttonConnect.Enabled = true;
                Text = "Disconnected";
            }));
            
        }
        
        public void Refresh(UInt32 id)
        {
            try
            {
                string rem = "[" + id + "]";
                listBoxClients.Invoke(new Action(() =>
                {
                    foreach (string item in listBoxClients.Items)
                    {
                        if (item.Contains(rem))
                        {
                            listBoxClients.Items.Remove(item);
                            break;
                        }
                    }
                }));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0}::@::{1}",
                    GetCurrentMethod().Name, GetType().FullName);
                Console.WriteLine(ex);
                Console.WriteLine("Exception in {0}::@::{1}",
                    GetCurrentMethod().Name, GetType().FullName);
            }
        }

        public void Error(string message)
        {
            this.Invoke(new Action(() => ErrorMessageBox(message)));
        }
        
        private void ErrorMessageBox(string errMsg)
        {
            MessageBox.Show(this, errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void SuccessMessageBox(string successMsg)
        {
            MessageBox.Show(this, successMsg, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        public void InfoMsg(string msg)
        {
            textBoxInfo.Invoke(new Action(() => InfoText(msg)));
        }

        public void SuccessMsg(string msg)
        {
            textBoxInfo.Invoke(new Action(() => SuccessText(msg)));
        }

        private void ErrorText(string errMsg)
        {
            textBoxInfo.SelectionStart = textBoxInfo.TextLength;
            textBoxInfo.SelectionLength = 0;
            textBoxInfo.SelectionColor = Color.Red;
            textBoxInfo.AppendText("[" + DateTime.Now + "] ERROR # ");
            textBoxInfo.AppendText(errMsg + Environment.NewLine);
            textBoxInfo.SelectionColor = textBoxInfo.ForeColor;
        }

        private void SuccessText(string successMsg)
        {
            textBoxInfo.SelectionStart = textBoxInfo.TextLength;
            textBoxInfo.SelectionLength = 0;
            textBoxInfo.SelectionColor = Color.Green;
            textBoxInfo.AppendText("[" + DateTime.Now + "] SUCCESS # ");
            textBoxInfo.AppendText(successMsg + Environment.NewLine);
            textBoxInfo.SelectionColor = textBoxInfo.ForeColor;
        }

        private void InfoText(string infoMsg)
        {
            textBoxInfo.SelectionStart = textBoxInfo.TextLength;
            textBoxInfo.SelectionLength = 0;
            textBoxInfo.SelectionColor = Color.Blue;
            textBoxInfo.AppendText("[" + DateTime.Now + "] INFO # ");
            textBoxInfo.AppendText(infoMsg + Environment.NewLine);
            textBoxInfo.SelectionColor = textBoxInfo.ForeColor;
        }

        private void MsgText(string infoMsg)
        {
            textBoxInfo.SelectionStart = textBoxInfo.TextLength;
            textBoxInfo.SelectionLength = 0;
            textBoxInfo.SelectionColor = Color.Magenta;
            textBoxInfo.AppendText("[" + DateTime.Now + "] PM # ");
            textBoxInfo.AppendText(infoMsg + Environment.NewLine);
            textBoxInfo.SelectionColor = textBoxInfo.ForeColor;
        }

        public void InitGame()
        {
            ////Application.Run(gameWindow); 
            //Invoke(new Action(() =>
            //{
            //    gameWindow = new GameWindow();
            //    m_Client.SetGameWindow(gameWindow);
            //}));
        }

        public void StartGame()
        {
            ////Application.Run(gameWindow); 
            //Invoke(new Action(() =>
            //{
            //    //gameWindow = new GameWindow();
            //    gameWindow.Show();
            //    //gameWindow.ShowDialog();
            //    //this.Hide();
            //}));
        }

        public void StopGame()
        {
            ////Application.Run(gameWindow); 

            //if (gameWindow != null)
            //{
            //    Console.WriteLine("Now closing...");
            //    this.Invoke(new Action(() =>
            //    {
            //        gameWindow.Close();

            //    }));
            //    //this.Show();
            //}
        }


        private void OnGameStart(object sender, UInt32 gameTime)
        {
            this.Invoke(new Action(() => {
                if (!ClientForm.GameOpened)
                {
                    Console.WriteLine("starting the game");
                
                        GameWindow.Instance.Show();
                        GameWindow.StartGameWindow(gameTime);
                    ClientForm.GameOpened = true;
                }
                else
                {
                    Console.WriteLine("Game Already started");
                }
            }));
        }

        public static void StartGameWindow(UInt32 gameTime)
        {
            if (!ClientForm.GameOpened)
            {
                Console.WriteLine("starting the game");
                /*
                new Thread(() =>
                {
                    try
                    {
                        Application.Run(GameWindow.Instance);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("FMMM: {0}", ex);
                    }
                }
            ).Start();
                */
                GameWindow gw = GameWindow.Instance;
                
                gw.Invoke(new Action(() => {

                    GameWindow.Instance.Show();
                    GameWindow.StartGameWindow(gameTime);
                }));
                
                ClientForm.GameOpened = true;
            }
            else
            {
                Console.WriteLine("Game Already started");
            }
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            if (mClient == null || !mClient.Connected)
            {
                ErrorMessageBox("You are not connected!");
                return;
            }

            Console.WriteLine("Let's start the room!");

            mClient.SendStartGameRequest();
        }

        private void gameRoomsList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (gameRoomsList.SelectedItems == null || gameRoomsList.SelectedItems.Count == 0)
                this.buttonJoinGameRoom.Enabled = false;
            else
                this.buttonJoinGameRoom.Enabled = true;
        }

        private void buttonGameRoomSendMessage_Click(object sender, EventArgs e)
        {
            string msg = this.textBoxGameRoomSendMessage.Text.Trim();
            Console.WriteLine("De trimis in game room mesajul '{0}'", msg);
            //TODO
            mClient.SendGameRoomMessage(mClient.GameRoom.Id, msg);
            this.textBoxGameRoomSendMessage.Text = null;
        }

        private void textBoxGameRoomSendMessage_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBoxGameRoomSendMessage.Text) || String.IsNullOrEmpty(textBoxGameRoomSendMessage.Text.Trim()))
            {
                this.buttonGameRoomSendMessage.Enabled = false;
            }
            else
            {
                this.buttonGameRoomSendMessage.Enabled = true;
            }
        }

        private void OnGameRoomCreated(object sender, GameRoomInfo gameRoom)
        {
            this.Invoke(new Action(() => {
                GameRoomLog(String.Format("You created new game room"));
                SetOwnGameRoom(gameRoom);
            }));
        }

        private void OnGameRoomJoined(object sender, GameRoomInfo gameRoom)
        {
            this.Invoke(new Action(() =>
            {
                GameRoomLog("You joined");
                SetOwnGameRoom(mClient.GameRoom);
            }));
        }

        private void OnOwnGameRoomClientJoined(object sender, ClientInfo client)
        {
            this.Invoke(new Action(() =>
            {
                GameRoomLog(String.Format("{0}(#{1}) joined room", client.Nick, client.Id));
                SetOwnGameRoom(mClient.GameRoom);
            }));
        }

        private void OnOwnGameRoomClientLeft(object sender, ClientInfo client)
        {
            this.Invoke(new Action(() =>
            {
                GameRoomLog(String.Format("{0}(#{1}) left room", client.Nick, client.Id));
                SetOwnGameRoom(mClient.GameRoom);
            }));
        }

        private void OnOwnGameRoomClosed(object sender, EventArgs e)
        {
            this.Invoke(new Action(() =>
            {
                SetOwnGameRoom(null);
            }));
        }

        private void SetOwnGameRoom(GameRoomInfo gameRoom)
        {
            if (gameRoom == null)
            {
                labelRoom.Text = "";
                listGameRoomPlayers.Items.Clear();
                return;
            }

            labelRoom.Text = "GAME ROOM " + gameRoom.Id;
            listGameRoomPlayers.Items.Clear();

            ListViewItem own = new ListViewItem(gameRoom.Owner);
            own.Font = new Font(Font.Name, 10.00F, FontStyle.Bold, GraphicsUnit.Pixel);

            listGameRoomPlayers.Items.Add(own);

            if (gameRoom.Players != null)
            {
                foreach (ClientInfo client in gameRoom.Players)
                {
                    if (client.Id != gameRoom.Id)
                    {
                        ListViewItem item = new ListViewItem(client.Nick + "(#" + client.Id + ")");
                        listGameRoomPlayers.Items.Add(item);
                    }
                }
            }
            
            this.buttonPlay.Enabled = true;
            this.buttonExitGameRoom.Enabled = true;
            this.textBoxGameRoomSendMessage.Enabled = true;
        }

        //public void SetRoom(UInt32 roomId, string owner, int noOfPlayers, List<string> players)
        //{
        //this.Invoke(new Action(() =>
        //{
        //    if (players.Count == 0)
        //    {
        //        this.buttonPlay.Enabled = true;
        //        this.buttonExitGameRoom.Enabled = true;
        //        this.buttonCloseGameRoom.Enabled = true;
        //        this.textBoxGameRoomSendMessage.Enabled = true;
        //        this.textBoxGameChat.Clear();
        //    }
        //    else
        //    {
        //        this.buttonPlay.Enabled = false;
        //        this.buttonExitGameRoom.Enabled = true;
        //        this.buttonCloseGameRoom.Enabled = false;
        //        this.textBoxGameRoomSendMessage.Enabled = true;
        //        this.textBoxGameChat.Clear();
        //    }
        //    //this.labelRoom.Text = "GAME Room " + roomId;
        //    this.currentGameRoom = new GameRoom(roomId, owner, noOfPlayers, players);
        //    refreshRoom();
        //}));
        //}
        
        public void CloseGameRoom()
        {
            //this.Invoke(new Action(() =>
            //{
            //    this.GameRoomLog(this.currentGameRoom.Owner + " closed the room.");
            //    this.currentGameRoom = null;
            //    refreshRoom();
            //}));
        }

        public void ResetRoom()
        {
            this.Invoke(new Action(() =>
            {
                //this.currentGameRoom = null;
                labelRoom.Text = "";
                buttonPlay.Enabled = false;
                buttonExitGameRoom.Enabled = false;
                //buttonCloseGameRoom.Enabled = false;
                buttonGameRoomSendMessage.Enabled = false;
                textBoxGameRoomSendMessage.Enabled = false;
                textBoxGameChat.Clear();
                listGameRoomPlayers.Items.Clear();
                //refreshRoom();
            }));
        }

        private void RefreshRoom()
        {
            //if (this.currentGameRoom == null)
            //{
            //    this.labelRoom.Text = "GAME ROOM";
            //    this.listGameRoomPlayers.Items.Clear();
            //    this.buttonPlay.Enabled = false;
            //    this.buttonExitGameRoom.Enabled = false;
            //    this.buttonCloseGameRoom.Enabled = false;
            //    this.textBoxGameRoomSendMessage.Enabled = false;
            //    this.textBoxGameChat.Clear();
            //}
            //else
            //{
            //    this.labelRoom.Text = "GAME ROOM " + this.currentGameRoom.Id;
            //    this.listGameRoomPlayers.Items.Clear();

            //    ListViewItem own = new ListViewItem(this.currentGameRoom.Owner);
            //    own.Font = new Font(Font.Name, 10.00F, FontStyle.Bold, GraphicsUnit.Pixel);

            //    this.listGameRoomPlayers.Items.Add(own);

            //    if (this.currentGameRoom.Players != null)
            //    {
            //        foreach (string name in this.currentGameRoom.Players)
            //        {
            //            ListViewItem item = new ListViewItem(name);
            //            this.listGameRoomPlayers.Items.Add(item);
            //        }
            //    }
                //this.textBoxGameChat.Clear();
            //}
        }

        private void OnOwnGameRoomMessage(object sender, Tuple<string, string> msg)
        {
            GameRoomMessage(msg.Item1, msg.Item2);
        }

        private void OnError(object sender, string errorMsg)
        {
            Error(errorMsg);
        }
    
        public void GameRoomMessage(string from, string msg)
        {
            this.Invoke(new Action(() =>
            {
                textBoxGameChat.SelectionStart = textBoxGameChat.TextLength;
                textBoxGameChat.SelectionLength = 0;
                textBoxGameChat.SelectionColor = Color.Green;
                textBoxGameChat.AppendText(String.Format("[{0}] ", DateTime.Now));
                textBoxGameChat.AppendText(String.Format("{0}: {1}", from, msg));
                textBoxGameChat.AppendText(Environment.NewLine);
                textBoxGameChat.SelectionColor = textBoxGameChat.ForeColor;
            }));
        }

        public void GameRoomLog(string log)
        {
            this.Invoke(new Action(() =>
            {
                textBoxGameChat.SelectionStart = textBoxGameChat.TextLength;
                textBoxGameChat.SelectionLength = 0;
                textBoxGameChat.SelectionColor = Color.BlueViolet;
                textBoxGameChat.AppendText(String.Format("[{0}] ", DateTime.Now));
                textBoxGameChat.AppendText(log);
                textBoxGameChat.AppendText(Environment.NewLine);
                textBoxGameChat.SelectionColor = textBoxGameChat.ForeColor;
            }));
        }
    }
}
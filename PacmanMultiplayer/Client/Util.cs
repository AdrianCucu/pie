﻿using static System.Reflection.MethodBase;
using System.Windows.Forms;
using System.Drawing;
using System;
using Client;

namespace Client
{
    public enum MapDirection
    {
        NONE = 0, NORTH, EAST, SOUTH, WEST
    }

    public class Util
    {
        public static readonly string ProjectResPath =
        System.IO.Directory.GetCurrentDirectory() +
        System.IO.Path.DirectorySeparatorChar +
        "Resources" +
        System.IO.Path.DirectorySeparatorChar;

        /*
        public static readonly string ProjectResPath =
            System.IO.Directory.GetCurrentDirectory() +
            System.IO.Path.DirectorySeparatorChar +
            ".." +
            System.IO.Path.DirectorySeparatorChar +
            ".." +
            System.IO.Path.DirectorySeparatorChar +
            "Resources" +
            System.IO.Path.DirectorySeparatorChar;  
        */
    }
}

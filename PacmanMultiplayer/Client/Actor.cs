﻿using static System.Reflection.MethodBase;
using System.Windows.Forms;
using System.Drawing;
using System;
using Client;

namespace Client
{
    public abstract class Actor
    {
        protected PictureBox m_Picture;

        protected UInt32 m_Id;

        protected int m_PosX;
        protected int m_PosY;

        protected float m_GraphicsX;
        protected float m_GraphicsY;

        protected PointF m_PointLocation;


        public PictureBox Picture
        {
            get => m_Picture;
        }
        public int Height
        {
            get => m_Picture.Image.Height;
        }
        public int Width
        {
            get => m_Picture.Image.Width;
        }
        public UInt32 Id
        {
            get => m_Id;
        }
        public int X
        {
            get => m_PosX;
        }
        public int Y
        {
            get => m_PosY;
        }
        public float GrX
        {
            get => m_GraphicsX;
        }
        public float GrY
        {
            get => m_GraphicsY;
        }
        public PointF Point
        {
            get => m_PointLocation;
        }

        protected Actor(UInt32 id, int posX, int posY)
        {
            m_Id = id;
            m_PosX = posX;
            m_PosY = posY;
            m_GraphicsX = m_PosX * 1.00F;
            m_GraphicsY = m_PosY * 1.00F;
            m_PointLocation = new PointF(m_GraphicsX, m_GraphicsY);
        }

        public abstract void Draw(System.Drawing.Graphics g);
    }
}

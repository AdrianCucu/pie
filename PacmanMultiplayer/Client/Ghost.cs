﻿using static System.Reflection.MethodBase;
using System.Windows.Forms;
using System.Drawing;
using System;
using Client;

namespace Client
{
    public class Ghost : WalkActor
    {
        private static ImageList m_ImagesList = null;


        public Ghost(UInt32 id, int posX, int posY)
            : base(id, posX, posY)
        {
            m_Picture = new PictureBox();
            m_Picture.Name = "GhostImage" + m_Id;
            m_Picture.SizeMode = PictureBoxSizeMode.AutoSize;
            m_Picture.BackColor = Color.Black;

            if (m_ImagesList == null)
                loadImages();

            setGhost();
        }


        private void setGhost()
        {
            switch ((MapItem)m_Id)
            {
                case MapItem.GHOST1:
                    m_Picture.Image = m_ImagesList.Images[0];
                    break;

                case MapItem.GHOST2:
                    m_Picture.Image = m_ImagesList.Images[4];
                    break;

                case MapItem.GHOST3:
                    m_Picture.Image = m_ImagesList.Images[8];
                    break;

                case MapItem.GHOST4:
                    m_Picture.Image = m_ImagesList.Images[12];
                    break;
            }
        }


        private void loadImages()
        {
            m_ImagesList = new ImageList();

            m_ImagesList.ImageSize = new Size(27, 28);

            String imageName;
            Image newImage;

            for (int g = 0; g < 4; ++g)
            {
                for (int dir = 1; dir <= 4; ++dir)
                {
                    imageName =
                        String.Format(
                            Util.ProjectResPath + "Ghost{0}{1}.png", g, dir
                        );
                    newImage = Image.FromFile(imageName);
                    m_ImagesList.Images.Add(newImage);
                }
            }
        }


        public void Move(int x, int y)
        {
            base.move(x, y);
            updateGhostImage();
        }


        private void updateGhostImage()
        {
            switch ((MapItem)m_Id)
            {
                case MapItem.GHOST1:
                    m_Picture.Image =
                        m_ImagesList.Images[(int)m_Direction - 1];
                    break;

                case MapItem.GHOST2:
                    m_Picture.Image =
                        m_ImagesList.Images[4 + (int)m_Direction - 1];
                    break;

                case MapItem.GHOST3:
                    m_Picture.Image =
                        m_ImagesList.Images[8 + (int)m_Direction - 1];
                    break;

                case MapItem.GHOST4:
                    m_Picture.Image =
                        m_ImagesList.Images[12 + (int)m_Direction - 1];
                    break;
            }
        }
    }
}

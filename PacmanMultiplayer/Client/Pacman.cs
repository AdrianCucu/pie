﻿using static System.Reflection.MethodBase;
using System.Windows.Forms;
using System.Drawing;
using System;
using Client;

namespace Client
{
    public class Pacman : WalkActor
    {
        private static ImageList m_ImagesList;

        private string m_Nick;

        private int m_StartX;
        private int m_StartY;
        private int m_Score;
        private int m_ImageOn;
        private bool m_IsMoving;

        public int Score
        {
            get => m_Score;
        }
        public string Nick
        {
            get => m_Nick;
        }
        public bool IsMoving
        {
            get => m_IsMoving;
        }


        public Pacman(UInt32 id, int posX, int posY, string nick)
            : base(id, posX, posY)
        {
            m_ImageOn = 0;
            m_IsMoving = false;
            m_Score = 0;
            m_Nick = nick;
            m_StartX = m_PosX;
            m_StartY = m_PosY;

            m_ImagesList = new ImageList();
            loadImages();

            m_Picture = new PictureBox();
            m_Picture.Name = "PacmanImage" + m_Id;
            m_Picture.SizeMode = PictureBoxSizeMode.AutoSize;
            m_Picture.BackColor = Color.Black;

            SetPacman();
        }


        public void Move(int x, int y)
        {
            if (x == 0 && y == 0)
            {
                m_IsMoving = false;
                return;
            }

            if (x == -1 && y == -1)
            {
                m_IsMoving = false;
                Respawn();
                return;
            }
            base.move(x, y);
            m_IsMoving = true;
        }


        public void UpdateScore(int score)
        {
            m_Score = score;
        }


        private void SetPacman()
        {
            m_PosX = m_StartX;
            m_PosY = m_StartY;

            m_GraphicsX = m_StartX * 1.00F;
            m_GraphicsY = m_StartY * 1.00F;

            m_Direction = MapDirection.NONE;

            m_Picture.Image = m_ImagesList.Images[5];
        }


        public void Respawn()
        {
            SetPacman();
        }


        private void loadImages()
        {
            m_ImagesList.ImageSize = new Size(27, 28);

            String imageName;
            Image newImage;

            for (int i = 1; i <= 4; ++i)
                for (int j = 0; j < 3; ++j)
                {
                    /*
		    imageName = 
			String.Format(
			    Util.ProjectResPath + 
			    "Pacman" + id + 
			    System.IO.Path.DirectorySeparatorChar + 
			    "Pacman{0}{1}.png", i, j
			);
                    */
                    imageName =
                        String.Format(
                            Util.ProjectResPath +
                            "Pacman1" +
                            System.IO.Path.DirectorySeparatorChar +
                            "Pacman{0}{1}.png", i, j
                        );

                   
                    newImage = Image.FromFile(imageName);

                    if (newImage == null)
                    {
                    }
                        Console.WriteLine(imageName);

                    m_ImagesList.Images.Add(newImage);
                }
        }


        public void UpdatePacmanImage()
        {
            if (m_Direction == MapDirection.NONE)
            {
                m_Picture.Image = m_Picture.Image = m_ImagesList.Images[5];
                m_ImageOn = 0;
            }
            else
            {
                //Console.WriteLine("PACMAN IMAGE NO: {0}", (((int)m_Direction - 1) * 3) + m_ImageOn);
                m_Picture.Image =
                    m_ImagesList.Images[(((int)m_Direction - 1) * 3) + m_ImageOn];

                ++m_ImageOn;

                if (m_ImageOn == 2)
                    m_ImageOn = 0;
            }
        }
    }
}

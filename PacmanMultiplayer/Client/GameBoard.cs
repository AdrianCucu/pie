﻿using System.Windows.Forms;
using System.Drawing;
using System;
using Client;

namespace Client
{
    public enum MapItem
    {
        ZID = 9,
        GOL = 0,
        BANUT_MIC = 7,
        BANUT_MARE = 8,
        GHOST1 = 10,
        GHOST2 = 11,
        GHOST3 = 12,
        GHOST4 = 13
    }


    public class GameBoard
    {
        public const int DIM_Y = 21;
        public const int DIM_X = 37;
        public int[,] Matrix = null;
        public const int dimY = 50;

        private PictureBox BoardImage;
        public PictureBox Picture
        {
            get => BoardImage;
        }
        public Point Location
        {
            get => BoardImage.Location;
            set => BoardImage.Location = value;
        }
        public int Height
        {
            get => BoardImage.Image.Height;
        }
        public int Width
        {
            get => BoardImage.Image.Width;
        }


        public GameBoard()
        {
            BoardImage = new PictureBox();
            CreateBoardImage(1);
        }


        public void CreateBoardImage(int Level)
        {
            BoardImage.Name = "BoardImage";
            BoardImage.SizeMode = PictureBoxSizeMode.AutoSize;
            //BoardImage.Dock = DockStyle.Fill;

            switch (Level)
            {
                case 1:
                    BoardImage.Image =
                    Image.FromFile(Util.ProjectResPath + "Board2.png");
                    break;
            }
        }


        public void setMatrix(int[,] Matrix)
        {
            this.Matrix = Matrix;
        }


        public void Print()
        {
            int x, y;
            int scale = 1;

            for (y = 0; y < DIM_Y * scale; ++y)
            {
                for (x = 0; x < DIM_X * scale; ++x)
                {
                    switch (Matrix[y / scale, x / scale])
                    {
                        case (int)MapItem.ZID:
                            Console.Write("X");
                            break;
                        case (int)MapItem.GOL:
                            Console.Write(" ");
                            break;
                        case (int)MapItem.BANUT_MIC:
                            Console.Write("c");
                            break;
                        case (int)MapItem.BANUT_MARE:
                            Console.Write("C");
                            break;
                        /*
                        case (int)MapItem.PLAYER1:
                            Console.Write("1");
                            break; 
                        case (int)MapItem.PLAYER2:
                            Console.Write("2");
                            break;  
                        case (int)MapItem.PLAYER3: 
                            Console.Write("3");
                            break; 
                        case (int)MapItem.PLAYER4: 
                            Console.Write("4");
                            break; 
                        */
                        default:
                            Console.Write(".");
                            break;
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }


        public override string ToString()
        {
            int x, y;
            string str = String.Empty;

            for (y = 0; y < DIM_Y; ++y)
            {
                for (x = 0; x < DIM_X - 1; ++x)
                {
                    str += this.Matrix[y, x] + " ";
                }
                str += this.Matrix[y, x];
                if (y != DIM_Y - 1)
                {
                    str += "\n";
                }
            }
            return str;
        }
    }
}

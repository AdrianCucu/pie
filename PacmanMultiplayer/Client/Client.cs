﻿using static System.Reflection.MethodBase;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System.Net;
using System;
using Network;

namespace Client
{
    public class ObjectAsync
    {
        public Socket socket;
        public byte[] buffer;
        public int size;
        public int offset;
    }

    public class Client
    {
        private readonly byte[] mRecvBuffer = new byte[4096];

        private GameRoomInfo mGameRoom;

        private Socket mSocket;

        private IPEndPoint mRemoteEndPoint;
        private IPEndPoint mLocalEndPoint;

        private UInt32 mId;
        private string mNick;
        
        //private GameWindow mGameWindow;

        private ObjectAsync mObjAsync = new ObjectAsync();
        private object mSendSyncRoot = new object();
        private ushort mLastError;
        private volatile int mLostConnectionFired;

        // Thrid person action
        public static EventHandler<GameRoomInfo> OnLobbyGameRoomCreated;
        public static EventHandler<GameRoomInfo> OnLobbyGameRoomUpdated;
        public static EventHandler<GameRoomInfo> OnLobbyGameRoomDeleted;
        public static EventHandler<List<GameRoomInfo>> OnLobbyGameRoomAck;

        // First person action
        public static EventHandler<GameRoomInfo> OnGameRoomCreated;
        public static EventHandler<GameRoomInfo> OnGameRoomJoined;
        public static EventHandler<GameRoomInfo> OnGameRoomLeft;
        public static EventHandler<GameRoomInfo> OnGameRoomClosed; // TODO

        // First person implied
        public static EventHandler<ClientInfo> OnOwnGameRoomClientJoined;
        public static EventHandler<ClientInfo> OnOwnGameRoomClientLeft;
        public static EventHandler<Tuple<string, string>> OnOwnGameRoomMessage;
        public static EventHandler OnOwnGameRoomClosed;

        public static EventHandler<ClientInfo> OnOtherClientInfoReceived; // TODO

        public static EventHandler<ClientInfo> OnClientLeft;

        public static EventHandler<string> OnDisconnect;

        public static EventHandler<string> OnHello;
        
        public static EventHandler<string> OnError;

        public static EventHandler<UInt32> OnGameStart;

        public static EventHandler<Tuple<string, string>> OnPrivateMessageReceived;
        
        public UInt32 Id
        {
            get => mId;
        }
        public IPAddress LocalIp
        {
            get => mLocalEndPoint.Address;
        }
        public int LocalPort
        {
            get => mLocalEndPoint.Port;
        }
        public IPAddress RemoteIp
        {
            get => mRemoteEndPoint.Address;
        }
        public int RemotePort
        {
            get => mRemoteEndPoint.Port;
        }
        public bool Connected
        {
            get => mSocket.Connected;
        }
        public string Nick
        {
            get => mNick;
        }
        public ushort Error
        {
            get => mLastError;
        }
        public GameRoomInfo GameRoom
        {
            get => mGameRoom;
        }

        public Client()
        {
            // Create a TCP/IP socket
            //mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            mLostConnectionFired = 0;
        }
        
        //public void SetGameWindow(GameWindow gameWindow)
        //{
        //    m_GameWindow = gameWindow;
        //}
        
        public bool Connect(IPAddress ip, int port, string nickname)
        {
            try
            {
                if (true) //(mSocket == null)
                {
                    // Create a TCP/IP socket
                    mSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                }

                if (!mSocket.Connected)
                {
                    IPEndPoint remoteEP = new IPEndPoint(ip, port);

                    mSocket.Connect(remoteEP);

                    if (mSocket.Connected)
                    {
                        Console.WriteLine("Sucesfully connected the socket");

                        SendConnectionRequest(nickname);

                        int size = mSocket.Receive(mRecvBuffer);

                        if (size <= 0)
                        {
                            Console.WriteLine("Did not receive anything.");
                            mSocket.Disconnect(true);
                            return false;
                        }

                        if (size >= Packet.HEADER_LEN)
                        {
                            //Lets read
                            PacketReader pr = new PacketReader(mRecvBuffer);
                            int packet_size = pr.ReadUInt16();
                            int packet_type = pr.ReadUInt16();

                            if (size >= packet_size)
                            {
                                if ((ushort)packet_type != PacketType.Connection.WELCOME)
                                {
                                    mLastError = (ushort)packet_type;
                                    mSocket.Disconnect(true);
                                    return false;
                                }

                                if ((ushort)packet_type ==
                                    PacketType.Connection.WELCOME)
                                {
                                    UInt32 id = pr.ReadUInt32();

                                    mId = id;
                                    mNick = nickname;

                                    mRemoteEndPoint = mSocket.RemoteEndPoint as IPEndPoint;
                                    mLocalEndPoint = mSocket.LocalEndPoint as IPEndPoint;
                                
                                    mObjAsync.socket = mSocket;
                                    mObjAsync.buffer = mRecvBuffer;
                                    mObjAsync.offset = 0;
                                    mObjAsync.size = 0;

                                    mSocket.BeginReceive(
                                        mObjAsync.buffer,
                                        mObjAsync.offset,
                                        mObjAsync.buffer.Length - mObjAsync.offset,
                                        SocketFlags.None,
                                        AsyncRecv,
                                        mObjAsync
                                    );

                                    SendFirstHello();
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0}->{1}",
                        GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }

            if (mSocket.Connected)
            {
                mSocket.Disconnect(true);
            }
            return false;
        }

        public void Disconnect(bool shutdown)
        {
            try
            {
                lock (this)
                {
                    if (mSocket.Connected)
                    {
                        SendDisconnect();

                        //sender.Shutdown(SocketShutdown.Both);
                        //m_Socket.Close();
                        //Disconnect with reuseSocket  = true
                        Console.WriteLine("Disconneting the socket ...");
                        //mSocket.Shutdown(SocketShutdown.Both);
                        mSocket.Disconnect(true);
                        Console.WriteLine("Socket disconnected");

                        if (!shutdown)
                        {
                            if (OnDisconnect != null)
                            {
                                OnDisconnect(this, "You closed connection with server");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }
        
        public void SendPrivateMessage(UInt32 id, string msg)
        {
            Packet p = new Packet(PacketType.Request.PM);
            p.WriteUInt32(id);
            p.WriteAscii(msg);
            SendPacket(p);
        }

        public void SendFirstHello()
        {
            Packet p = new Packet(PacketType.Connection.FIRST_HELLO);
            SendPacket(p);
        }

        public void SendConnectionRequest(string nick)
        {
            Packet p = new Packet(PacketType.Request.CONNECT);
            p.WriteAscii(nick);
            SendPacket(p);
        }

        public void SendStartGameRequest()
        {
            Packet p = new Packet(PacketType.Request.START_GAME);
            SendPacket(p);
        }

        public void SendGameRoomMessage(UInt32 roomId, string msg)
        {
            Packet p = new Packet(PacketType.Request.MESSAGE_ROOM);
            p.WriteUInt32(roomId);
            p.WriteAscii(msg);
            SendPacket(p);
        }

        public void SendCreateGameRoomRequest()
        {
            Packet p = new Packet(PacketType.Request.CREATE_ROOM);
            SendPacket(p);
        }

        public void SendJoinGameRoomRequest(UInt32 gameId)
        {
            Packet p = new Packet(PacketType.Request.JOIN_ROOM);
            p.WriteUInt32(gameId);
            SendPacket(p);
        }

        public void SendExitGameRoomRequest()
        {
            Packet p = new Packet(PacketType.Request.EXIT_ROOM);
            SendPacket(p);
            mGameRoom = null;
        }

        /*
        public void SendCloseGameRoomRequest()
        {
            Packet p = new Packet(PacketType.Request.CLOSE_ROOM);
            //p.WriteUInt32(m_Id);
            SendPacket(p);
            //Console.WriteLine("Close Game Room Request sent");
            mGameRoom = null;
        }
        */
        
        public void SendMoveRequest(MapDirection dir)
        {
            Packet p = new Packet(PacketType.Request.MOVE_REQUEST);
            p.WriteInt32((int)dir);
            SendPacket(p);
        }

        public void SendDisconnect()
        {
            Packet p = new Packet(PacketType.Connection.CLIENT_DISCONNECT);
            SendPacket(p);
        }

        private void SendPacket(Packet p)
        {
            byte[] raw = p.Pack();

            if (mSocket.Connected && raw != null)
            {
                //int size = m_Socket.Send(raw, 0, raw.Length, 0);
                mSocket.BeginSend(raw, 0, raw.Length, 0, AsyncSend, null);
                //Console.WriteLine("Sent first {0} bytes", size);
            }
        }

        private void StartReceiveMessageAsync()
        {
            mSocket.BeginReceive(
                mObjAsync.buffer,
                mObjAsync.offset,
                mObjAsync.buffer.Length - mObjAsync.offset,
                SocketFlags.None,
                AsyncRecv,
                mObjAsync
            );
        }

        private void FireOnLostConnection()
        {
            if (Interlocked.CompareExchange(ref mLostConnectionFired, 1, 0) == 0)
            {
                Console.WriteLine("##################################");
                Console.WriteLine("Connection lost with");
                Console.WriteLine("# ID: {0}, Nick: {1}", mId, mNick);
                Console.WriteLine("##################################");

                //TODO: interpred disconnection in game
                if (OnDisconnect != null)
                {
                    OnDisconnect(this, "Connection lost");
                }
                //OnLostConnection();
            }
        }

        //private void OnLostConnection()
        //{
            //if (mGameWindow != null)
            //{
            //    mGameWindow.GameClosed();
            //    mClientForm.infoMsg("GAME CLOSED  " + DateTime.Now);

            //    mClientForm.StopGame();

            //    mClientForm.infoMsg("Connection lost");
            //    mClientForm.refresh();
            //}
        //}

        private void AsyncRecv(IAsyncResult res)
        {
            try
            {
                ObjectAsync m_ObjAsync = (ObjectAsync)res.AsyncState;

                if (!m_ObjAsync.socket.Connected)
                {
                    FireOnLostConnection();
                    return;
                }

                SocketError se;
                int bytesRecv = m_ObjAsync.socket.EndReceive(res, out se);

                if (se != SocketError.Success)
                {
                    FireOnLostConnection();
                    return;
                }

                if (bytesRecv <= 0)
                {
                    FireOnLostConnection();
                    return;
                }

                m_ObjAsync.size += bytesRecv;

                int index = 0;
                int bsize = m_ObjAsync.size;

                while (index + Packet.HEADER_LEN <= m_ObjAsync.size)
                {
                    int packet_size =
                        (m_ObjAsync.buffer[index + 1] << 8) |
                        (m_ObjAsync.buffer[index]);

                    if (index + packet_size > m_ObjAsync.size)
                    {
                        break;
                    }

                    //Console.WriteLine("Received a complete packet.");

                    int packet_type =
                        (m_ObjAsync.buffer[index + 3] << 8) |
                        (m_ObjAsync.buffer[index + 2]);

                    int payload_size = packet_size - Packet.HEADER_LEN;

                    PacketReader pr =
                        new PacketReader(
                            m_ObjAsync.buffer,
                            index + Packet.HEADER_LEN,
                            payload_size
                        );

                    Packet p =
                        new Packet(
                            (ushort)packet_type,
                            pr.ReadBytes(payload_size)
                        );

                    p.Lock();
                    ParsePacket(p);

                    index += packet_size;
                    bsize -= packet_size;
                }

                if (index > 0 && bsize > 0)
                {
                    int i;
                    for (i = 0; i < bsize; ++i)
                        m_ObjAsync.buffer[i] = m_ObjAsync.buffer[index++];
                    m_ObjAsync.offset = i - 1;
                }
                else
                {
                    m_ObjAsync.offset = bsize;
                }

                m_ObjAsync.size = bsize;

                //Console.WriteLine("offset = {0}", m_ObjAsync.offset);

                // begin reveive again
                mSocket.BeginReceive(
                    m_ObjAsync.buffer,
                    m_ObjAsync.offset,
                    m_ObjAsync.buffer.Length - m_ObjAsync.offset,
                    SocketFlags.None,
                    out se,
                    AsyncRecv,
                    m_ObjAsync
                );

                if (se != SocketError.Success && se != SocketError.IOPending)
                {
                    FireOnLostConnection();
                }
            }
            catch (System.ObjectDisposedException objDisposedEx)
            {
                Console.WriteLine("Socket has been closed");
                Console.WriteLine(objDisposedEx);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0}->{1}",
                        GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        private void AsyncSend(IAsyncResult res)
        {
            try
            {
                lock (this)//(m_SendSyncRoot)
                {
                    if (!mSocket.Connected)
                    {
                        FireOnLostConnection();
                        return;
                    }

                    SocketError se;
                    int bytesSent = mSocket.EndSend(res, out se);

                    if (se != SocketError.Success)
                    {
                        FireOnLostConnection();
                        return;
                    }

                    if (bytesSent <= 0)
                    {
                        FireOnLostConnection();
                        return;
                    }
                }
            }
            catch (System.ObjectDisposedException objDisposedEx)
            {
                Console.WriteLine("Socket has been closed");
                Console.WriteLine(objDisposedEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        private void ParsePacket(Packet p)
        {
            try
            {
                switch (p.Type)
                {
                    case PacketType.Connection.SERVER_DISCONNECT:
                        {
                            // When server send disconnect
                            if (OnDisconnect != null)
                            {
                                OnDisconnect(this, "Server closed connection");
                            }
                        }
                        break;
                    case PacketType.Connection.HELLO:
                        {
                            // When server send ping packet
                            Console.WriteLine("PING from server");
                            //if (OnHello != null) OnHello(this, "PING");                       
                        }
                        break;
                    case PacketType.Request.PM:
                        {
                            // Lobby private message
                            UInt32 clientId = p.ReadUInt32();
                            string clientNick = p.ReadAscii();
                            string message = p.ReadAscii();

                            Console.WriteLine("[*]Received new Message  #{0} {1}: \"{2}\"", clientId, clientNick, message);

                            if (OnPrivateMessageReceived != null)
                            {
                                OnPrivateMessageReceived(this, new Tuple<string, string>(clientNick, message));
                            }
                        }
                        break;
                    case PacketType.Info.ACK:
                        {
                            // Info about all the clients
                            int n = p.ReadInt32();
                            
                            for (int i = 0; i < n; ++i)
                            {
                                UInt32 clientId = p.ReadUInt32();
                                string clientNick = p.ReadAscii();

                                ClientInfo client = new ClientInfo();
                                client.Id = clientId;
                                client.Nick = clientNick;

                                if (OnOtherClientInfoReceived != null)
                                {
                                    OnOtherClientInfoReceived(this, client);
                                }
                            }
                        }
                        break;
                    case PacketType.Info.CLIENT_LEFT:
                        {
                            // When a client leave the server
                            UInt32 clientId = p.ReadUInt32();

                            ClientInfo client = new ClientInfo();
                            client.Id = clientId;

                            if (OnClientLeft != null)
                            {
                                OnClientLeft(this, client);
                            }          
                        }
                        break;






                    case PacketType.GameInfo.ALL_PLAYERS_POSITION:
                        {
                            int num_of_players = p.ReadInt32();
                            while (num_of_players-- > 0)
                            {
                                UInt32 id = p.ReadUInt32();
                                int x = p.ReadInt32();
                                int y = p.ReadInt32();
                                string nick = p.ReadAscii();

                                Console.WriteLine("PLAYER {0}) {1} X: {2}, Y: {3}", id, nick, x, y);
                                GameWindow.AddPlayer(id, x, y, nick);

                                //m_GameWindow.AddPlayer(id, x, y, nick);
                                //TODO: callback in GAME window
                            }
                        }
                        break;
                    case PacketType.GameInfo.ALL_GHOSTS_POSITION:
                        {
                            int num_of_ghosts = p.ReadInt32();

                            while (num_of_ghosts-- > 0)
                            {
                                UInt32 ghostVal = p.ReadUInt32();
                                int ghostX = p.ReadInt32();
                                int ghostY = p.ReadInt32();

                                Console.WriteLine("GHOST  {0})     X: {1}, Y: {2}", ghostVal, ghostX, ghostY);
                                GameWindow.AddGhost(ghostVal, ghostX, ghostY);
                                //TODO: callback in GAME WIndw
                                //m_GameWindow.AddGhost(ghostVal, ghostX, ghostY);
                            }
                        }
                        break;
                    /*
                    case PacketType.GameInfo.PLAYER_DIRECTION:
                        {
                            int num_of_players = p.ReadInt32();

                            while (num_of_players-- > 0)
                            {
                                UInt32 playerId = p.ReadUInt32();
                                int pX = p.ReadInt32();
                                int pY = p.ReadInt32();
                                int score = p.ReadInt32();

                                m_GameWindow.PlayerMove(playerId, pX, pY, score);
                            }
                        }
                        break;

                    case PacketType.GameInfo.GHOST_DIRECTION:
                        {
                            int num_of_ghosts = p.ReadInt32();

                            while (num_of_ghosts-- > 0)
                            {
                                UInt32 ghostVal = p.ReadUInt32();
                                int ghostX = p.ReadInt32();
                                int ghostY = p.ReadInt32();

                                MapItem coinType = (MapItem)p.ReadUInt8();

                                if (coinType == MapItem.BANUT_MIC ||
                                    coinType == MapItem.BANUT_MARE)
                                {
                                    int coinY = p.ReadInt32();
                                    int coinX = p.ReadInt32();

                                    m_GameWindow.AddCoin(coinY, coinX, coinType);
                                }

                                debug(String.Format("GHOST {0}) X: {1}, Y: {2}", ghostVal, ghostX, ghostY));

                                m_GameWindow.GhostMove(ghostVal, ghostX, ghostY);
                            }
                        }
                        break;

                    case PacketType.GameInfo.PLAYER_RESPAWN:
                        {
                            UInt32 playerId = p.ReadUInt32();
                            debug(String.Format("Player: {0} HAS BEEN RESPAWNED", playerId));

                            m_GameWindow.RespawnPlayer(playerId);
                        }
                        break;
                    */

                    ///////////////////////////////////////////////////////////////////////
                    case PacketType.GameInfo.PLAYER_DIRECTION:
                    case PacketType.GameInfo.GHOST_DIRECTION:
                    case PacketType.GameInfo.PLAYER_RESPAWN:
                        //TODO: callback in GAME WIndow
                        //m_GameWindow.AddToPacketQueue(p);
                        GameWindow.AddToPacketQueue(p);
                        //Console.WriteLine("Added to packet ququq ");
                        break;
                    ///////////////////////////////////////////////////////////////////////
                    ///



                    case PacketType.Game.GAME_ABORTED:
                        {
                            //TODO: callback in GAME Window
                            //m_GameWindow.GameClosed();
                            //m_ClientForm.StopGame();
                            //m_ClientForm.infoMsg("GAME CLOSED  " + DateTime.Now);
                            //m_ClientForm.GameRoomLog("GAME CLOSED");

                            Console.WriteLine("[DEBUG] --=-- GAME_ABORTED --=--");
                        }
                        break;
                    case PacketType.Game.GAME_OVER:
                        {
                            //TODO: alt callback
                            //debug("GAME OVER");
                            //m_GameWindow.GameOver();
                            //m_ClientForm.StopGame();
                            //m_ClientForm.infoMsg("GAME OVER " + DateTime.Now);
                            //m_ClientForm.GameRoomLog("GAME OVER");

                            Console.WriteLine("[DEBUG] --=-- GAME_OVER --=--");
                        }
                        break;
                    case PacketType.Game.GAME_INIT:
                        {
                            //TODO: alte callback-uri
                            //debug("-=- GAME INIT -=-");

                            //m_ClientForm.InitGame();
                            //m_GameWindow.setClient(this);

                            //m_ClientForm.GameRoomLog("GAME INITIALIZNG...");

                            Console.WriteLine("[DEBUG] --=-- GAME_INIT --=--");
                            GameWindow.SetClient(this);
                            GameWindow.InitGame();
                        }
                        break;
                    case PacketType.Game.GAME_START:
                        {

                            UInt32 game_time = p.ReadUInt32();
                            //GameWindow.Instance.StartGame(game_time);

                            //ClientForm.StartGameWindow(game_time);
                            if (OnGameStart != null)
                                OnGameStart(this, game_time);
                            Console.WriteLine("[DEBUG] --=-- GAME_START --=--: {0}", game_time);

                            //Console.WriteLine("Game countdown: {0} sec.", game_time);
                            //TODO: callback
                            //debug(String.Format(" - GAME START:-  {0}", game_time));
                            //m_ClientForm.StartGame();
                            ////m_ClientForm.infoMsg("GAME STARTED " + DateTime.Now);
                            //m_ClientForm.GameRoomLog("GAME STARTED");

                            ////m_GameWindow.setClient(this);
                            //m_GameWindow.StartGameWindow(game_time);

                            //Console.WriteLine("[DEBUG] --=-- GAME_START --=--");
                        }
                        break;






                    case PacketType.Response.CREATE_ROOM_REQUEST_OK:
                        {
                            // GameRoom request accepted by server so this client created new game room
                            GameRoomInfo gameRoom = new GameRoomInfo(mId, mNick, 1, null);

                            this.mGameRoom = gameRoom;

                            if (OnGameRoomCreated != null)
                            {
                                OnGameRoomCreated(this, gameRoom);
                            }
                        }
                        break;
                    case PacketType.Info.GAME_ROOM_CREATED:
                        {
                            // When new GameRoom was created by other client
                            UInt32 roomId = p.ReadUInt32();
                            string ownerNick = p.ReadAscii();

                            GameRoomInfo gameRoom = new GameRoomInfo(roomId, ownerNick, 1, null);

                            if (OnLobbyGameRoomCreated != null)
                            {
                                OnLobbyGameRoomCreated(this, gameRoom);
                            }
                        }
                        break;
                    case PacketType.Info.GAME_ROOM_CLOSED:
                        {
                            // Received when current GameRoom was closed
                            Console.WriteLine("[#{0}] {1} Tocmai ce mi-am inchis camera", mId, mNick);
                            if (OnOwnGameRoomClosed != null)
                            {
                                OnOwnGameRoomClosed(this, null);
                            }
                        }
                        break;
                    case PacketType.Info.GAME_ROOM_CLIENT_JOINED:
                        {
                            // Received when another client joined current GameRoom
                            UInt32 clientId = p.ReadUInt32();
                            string clientNick = p.ReadAscii();
                            UInt32 roomId = p.ReadUInt32();

                            Console.WriteLine("#{0} {1} JOINED YOUR ROOM", clientId, clientNick);

                            if (mGameRoom != null)
                            {
                                ClientInfo clientInfo = new ClientInfo();
                                clientInfo.Id = clientId;
                                clientInfo.Nick = clientNick;
                                if (mGameRoom.Players == null)
                                {
                                    mGameRoom.Players = new List<ClientInfo>();
                                }

                                mGameRoom.Players.Add(clientInfo);

                                if (OnOwnGameRoomClientJoined != null)
                                {
                                    OnOwnGameRoomClientJoined(this, clientInfo);
                                }
                            }
                        }
                        break;
                    case PacketType.Info.GAME_ROOM_CLIENT_LEFT:
                        {
                            // Received when another client leave current GameRoom
                            UInt32 clientId = p.ReadUInt32();
                            string clientNick = p.ReadAscii();
                            UInt32 roomId = p.ReadUInt32();
                            
                            Console.WriteLine("#{0} {1} LEFT YOUR ROOM", clientId, clientNick);

                            if (mGameRoom != null)
                            {
                                foreach (ClientInfo client in mGameRoom.Players)
                                {
                                    if (client.Id == clientId)
                                    {
                                        mGameRoom.Players.Remove(client);
                                        
                                        if (OnOwnGameRoomClientLeft != null)
                                        {
                                            OnOwnGameRoomClientLeft(this, client);
                                        }
                                        
                                        break;
                                    }
                                }
                            }
                        }
                        break;
                    case PacketType.Info.GAME_ROOM_INFO:
                        {
                            // Info about this client new joined GameRoom
                            UInt32 roomId = p.ReadUInt32();

                            int numPlayersInThisRoom = p.ReadInt32();

                            GameRoomInfo gameRoom = new GameRoomInfo(mId, mNick, 1, null);

                            this.mGameRoom = gameRoom;
                            this.mGameRoom.Players = new List<ClientInfo>();

                            for (int i = 0; i < numPlayersInThisRoom; ++i)
                            {
                                UInt32 playerId = p.ReadUInt32();
                                string playerNick = p.ReadAscii();

                                ClientInfo client = new ClientInfo();
                                client.Id = playerId;
                                client.Nick = playerNick;

                                Console.WriteLine(client);

                                this.mGameRoom.Players.Add(client);
                            }

                            UInt32 ownerId = p.ReadUInt32();
                            string ownerNick = p.ReadAscii();

                            this.mGameRoom.Id = ownerId;
                            this.mGameRoom.Owner = ownerNick;

                            if (OnGameRoomJoined != null)
                            {
                                OnGameRoomJoined(this, mGameRoom);
                            }
                        }
                        break;
                    case PacketType.Info.GAME_ROOMS_ACK:
                        {
                            // Infos about all lobby game rooms

                            int num_of_game_rooms = p.ReadInt32();

                            List<GameRoomInfo> gameRooms = new List<GameRoomInfo>();

                            if (num_of_game_rooms > 0)
                            {
                                while (num_of_game_rooms-- > 0)
                                {
                                    UInt32 roomId = p.ReadUInt32();
                                    string ownerNick = p.ReadAscii();
                                    int noOfPlayers = p.ReadInt32();

                                    gameRooms.Add(new GameRoomInfo(roomId, ownerNick, noOfPlayers, null));
                                }
                            }
                            
                            if (OnLobbyGameRoomAck != null)
                            {
                                OnLobbyGameRoomAck(this, gameRooms);
                            }
                        }
                        break;
                    case PacketType.Request.MESSAGE_ROOM:
                        {
                            // Message inside game room
                            string from = p.ReadAscii();
                            string message = p.ReadAscii();

                            if (OnOwnGameRoomMessage != null)
                            {
                                OnOwnGameRoomMessage(this, new Tuple<string, string>(from, message));
                            }
                        }
                        break;
                    //case PacketType.Response.JOIN_ROOM_REQUEST_FAILED:
                    //    {
                    //        if (OnError != null)
                    //        {
                    //            OnError(this, "Can't join game room");
                    //        }
                    //    }
                    //    break;
                    default:
                        Console.WriteLine("[UNKNOWN MESSAGE TYPE]");
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} {1}",
                        GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        public override string ToString()
        {
            return String.Format("{0}:{1} => {2}:{3} (#{4}) {5}", LocalIp, LocalPort, RemoteIp, RemotePort, Id, Nick);
        }
    }
}
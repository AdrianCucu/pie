﻿using System.Windows.Forms;
using System;	

namespace Client
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonConnect = new System.Windows.Forms.Button();
            this.labelServerIp = new System.Windows.Forms.Label();
            this.textBoxServerIp = new System.Windows.Forms.TextBox();
            this.textBoxServerPort = new System.Windows.Forms.TextBox();
            this.labelServerPort = new System.Windows.Forms.Label();
            this.textBoxInfo = new System.Windows.Forms.RichTextBox();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.buttonSendMessage = new System.Windows.Forms.Button();
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.listBoxClients = new System.Windows.Forms.ListBox();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.buttonCreateGameRoom = new System.Windows.Forms.Button();
            this.buttonJoinGameRoom = new System.Windows.Forms.Button();
            this.buttonExitGameRoom = new System.Windows.Forms.Button();
            this.gameRoomsList = new System.Windows.Forms.ListView();
            this.textBoxNick = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listGameRoomPlayers = new System.Windows.Forms.ListView();
            this.columnRoomPlayer = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.textBoxGameRoomSendMessage = new System.Windows.Forms.TextBox();
            this.labelRoom = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonGameRoomSendMessage = new System.Windows.Forms.Button();
            this.textBoxGameChat = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(15, 100);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(89, 23);
            this.buttonConnect.TabIndex = 0;
            this.buttonConnect.Text = "Connect";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // labelServerIp
            // 
            this.labelServerIp.AutoSize = true;
            this.labelServerIp.Location = new System.Drawing.Point(166, 26);
            this.labelServerIp.Name = "labelServerIp";
            this.labelServerIp.Size = new System.Drawing.Size(51, 13);
            this.labelServerIp.TabIndex = 1;
            this.labelServerIp.Text = "Server IP";
            // 
            // textBoxServerIp
            // 
            this.textBoxServerIp.Location = new System.Drawing.Point(169, 48);
            this.textBoxServerIp.Name = "textBoxServerIp";
            this.textBoxServerIp.Size = new System.Drawing.Size(129, 20);
            this.textBoxServerIp.TabIndex = 2;
            this.textBoxServerIp.Text = "127.0.0.1";
            // 
            // textBoxServerPort
            // 
            this.textBoxServerPort.Location = new System.Drawing.Point(320, 48);
            this.textBoxServerPort.Name = "textBoxServerPort";
            this.textBoxServerPort.Size = new System.Drawing.Size(129, 20);
            this.textBoxServerPort.TabIndex = 4;
            this.textBoxServerPort.Text = "3200";
            // 
            // labelServerPort
            // 
            this.labelServerPort.AutoSize = true;
            this.labelServerPort.Location = new System.Drawing.Point(317, 26);
            this.labelServerPort.Name = "labelServerPort";
            this.labelServerPort.Size = new System.Drawing.Size(71, 13);
            this.labelServerPort.TabIndex = 3;
            this.labelServerPort.Text = "Server PORT";
            // 
            // textBoxInfo
            // 
            this.textBoxInfo.Location = new System.Drawing.Point(15, 141);
            this.textBoxInfo.Name = "textBoxInfo";
            this.textBoxInfo.Size = new System.Drawing.Size(434, 177);
            this.textBoxInfo.TabIndex = 5;
            this.textBoxInfo.Text = "";
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Location = new System.Drawing.Point(501, 173);
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.Size = new System.Drawing.Size(232, 20);
            this.textBoxMessage.TabIndex = 6;
            // 
            // buttonSendMessage
            // 
            this.buttonSendMessage.Enabled = false;
            this.buttonSendMessage.Location = new System.Drawing.Point(501, 219);
            this.buttonSendMessage.Name = "buttonSendMessage";
            this.buttonSendMessage.Size = new System.Drawing.Size(89, 23);
            this.buttonSendMessage.TabIndex = 7;
            this.buttonSendMessage.Text = "Send message";
            this.buttonSendMessage.UseVisualStyleBackColor = true;
            this.buttonSendMessage.Click += new System.EventHandler(this.buttonSendMessage_Click);
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.Enabled = false;
            this.buttonDisconnect.Location = new System.Drawing.Point(360, 100);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(89, 23);
            this.buttonDisconnect.TabIndex = 8;
            this.buttonDisconnect.Text = "Disconnect";
            this.buttonDisconnect.UseVisualStyleBackColor = true;
            this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnect_Click);
            // 
            // listBoxClients
            // 
            this.listBoxClients.FormattingEnabled = true;
            this.listBoxClients.Location = new System.Drawing.Point(501, 48);
            this.listBoxClients.Name = "listBoxClients";
            this.listBoxClients.Size = new System.Drawing.Size(232, 95);
            this.listBoxClients.TabIndex = 9;
            this.listBoxClients.SelectedIndexChanged += new System.EventHandler(this.listBoxClients_SelectedIndexChanged);
            // 
            // buttonPlay
            // 
            //this.buttonPlay.Enabled = false;
            this.buttonPlay.Location = new System.Drawing.Point(764, 424);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(77, 29);
            this.buttonPlay.TabIndex = 10;
            this.buttonPlay.Text = "Start game";
            this.buttonPlay.UseVisualStyleBackColor = true;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // buttonCreateGameRoom
            // 
            this.buttonCreateGameRoom.Location = new System.Drawing.Point(15, 539);
            this.buttonCreateGameRoom.Name = "buttonCreateGameRoom";
            this.buttonCreateGameRoom.Size = new System.Drawing.Size(100, 30);
            this.buttonCreateGameRoom.TabIndex = 11;
            this.buttonCreateGameRoom.Text = "Create room";
            this.buttonCreateGameRoom.Click += new System.EventHandler(this.buttonCreateGameRoom_Click);
            // 
            // buttonJoinGameRoom
            // 
            this.buttonJoinGameRoom.Enabled = false;
            this.buttonJoinGameRoom.Location = new System.Drawing.Point(157, 539);
            this.buttonJoinGameRoom.Name = "buttonJoinGameRoom";
            this.buttonJoinGameRoom.Size = new System.Drawing.Size(100, 30);
            this.buttonJoinGameRoom.TabIndex = 12;
            this.buttonJoinGameRoom.Text = "Join room";
            this.buttonJoinGameRoom.Click += new System.EventHandler(this.buttonJoinGameRoom_Click);
            // 
            // buttonExitGameRoom
            // 
            this.buttonExitGameRoom.Enabled = false;
            this.buttonExitGameRoom.Location = new System.Drawing.Point(633, 539);
            this.buttonExitGameRoom.Name = "buttonExitGameRoom";
            this.buttonExitGameRoom.Size = new System.Drawing.Size(100, 30);
            this.buttonExitGameRoom.TabIndex = 14;
            this.buttonExitGameRoom.Text = "Leave Room";
            this.buttonExitGameRoom.Click += new System.EventHandler(this.buttonExitGameRoom_Click);
            // 
            // gameRoomsList
            // 
            this.gameRoomsList.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.gameRoomsList.FullRowSelect = true;
            this.gameRoomsList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.gameRoomsList.Location = new System.Drawing.Point(15, 334);
            this.gameRoomsList.MultiSelect = false;
            this.gameRoomsList.Name = "gameRoomsList";
            this.gameRoomsList.Size = new System.Drawing.Size(434, 177);
            this.gameRoomsList.TabIndex = 15;
            this.gameRoomsList.UseCompatibleStateImageBehavior = false;
            this.gameRoomsList.View = System.Windows.Forms.View.Details;
            this.gameRoomsList.SelectedIndexChanged += new System.EventHandler(this.gameRoomsList_SelectedIndexChanged);
            // 
            // textBoxNick
            // 
            this.textBoxNick.Location = new System.Drawing.Point(15, 48);
            this.textBoxNick.Name = "textBoxNick";
            this.textBoxNick.Size = new System.Drawing.Size(129, 20);
            this.textBoxNick.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Nickname";
            // 
            // listGameRoomPlayers
            // 
            this.listGameRoomPlayers.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnRoomPlayer});
            this.listGameRoomPlayers.FullRowSelect = true;
            this.listGameRoomPlayers.Location = new System.Drawing.Point(501, 277);
            this.listGameRoomPlayers.MultiSelect = false;
            this.listGameRoomPlayers.Name = "listGameRoomPlayers";
            this.listGameRoomPlayers.Size = new System.Drawing.Size(250, 108);
            this.listGameRoomPlayers.TabIndex = 17;
            this.listGameRoomPlayers.UseCompatibleStateImageBehavior = false;
            this.listGameRoomPlayers.View = System.Windows.Forms.View.Details;
            // 
            // columnRoomPlayer
            // 
            this.columnRoomPlayer.Text = "Player";
            this.columnRoomPlayer.Width = 200;
            // 
            // textBoxGameRoomSendMessage
            // 
            this.textBoxGameRoomSendMessage.Enabled = false;
            this.textBoxGameRoomSendMessage.Location = new System.Drawing.Point(501, 482);
            this.textBoxGameRoomSendMessage.Multiline = true;
            this.textBoxGameRoomSendMessage.Name = "textBoxGameRoomSendMessage";
            this.textBoxGameRoomSendMessage.Size = new System.Drawing.Size(250, 39);
            this.textBoxGameRoomSendMessage.TabIndex = 18;
            this.textBoxGameRoomSendMessage.TextChanged += new System.EventHandler(this.textBoxGameRoomSendMessage_TextChanged);
            // 
            // labelRoom
            // 
            this.labelRoom.AutoSize = true;
            this.labelRoom.Location = new System.Drawing.Point(498, 261);
            this.labelRoom.Name = "labelRoom";
            this.labelRoom.Size = new System.Drawing.Size(35, 13);
            this.labelRoom.TabIndex = 19;
            this.labelRoom.Text = "Room";
            this.labelRoom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(498, 466);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "Game chat";
            // 
            // buttonGameRoomSendMessage
            // 
            this.buttonGameRoomSendMessage.Enabled = false;
            this.buttonGameRoomSendMessage.Location = new System.Drawing.Point(766, 482);
            this.buttonGameRoomSendMessage.Name = "buttonGameRoomSendMessage";
            this.buttonGameRoomSendMessage.Size = new System.Drawing.Size(75, 39);
            this.buttonGameRoomSendMessage.TabIndex = 21;
            this.buttonGameRoomSendMessage.Text = "Send";
            this.buttonGameRoomSendMessage.UseVisualStyleBackColor = true;
            this.buttonGameRoomSendMessage.Click += new System.EventHandler(this.buttonGameRoomSendMessage_Click);
            // 
            // textBoxGameChat
            // 
            this.textBoxGameChat.Location = new System.Drawing.Point(501, 391);
            this.textBoxGameChat.Name = "textBoxGameChat";
            this.textBoxGameChat.ReadOnly = true;
            this.textBoxGameChat.Size = new System.Drawing.Size(250, 62);
            this.textBoxGameChat.TabIndex = 22;
            this.textBoxGameChat.Text = "";
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 600);
            this.Controls.Add(this.textBoxGameChat);
            this.Controls.Add(this.buttonGameRoomSendMessage);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelRoom);
            this.Controls.Add(this.textBoxGameRoomSendMessage);
            this.Controls.Add(this.listGameRoomPlayers);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonPlay);
            this.Controls.Add(this.listBoxClients);
            this.Controls.Add(this.buttonDisconnect);
            this.Controls.Add(this.buttonSendMessage);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.textBoxInfo);
            this.Controls.Add(this.textBoxServerPort);
            this.Controls.Add(this.labelServerPort);
            this.Controls.Add(this.textBoxServerIp);
            this.Controls.Add(this.labelServerIp);
            this.Controls.Add(this.buttonConnect);
            this.Controls.Add(this.buttonCreateGameRoom);
            this.Controls.Add(this.buttonJoinGameRoom);
            this.Controls.Add(this.buttonExitGameRoom);
            this.Controls.Add(this.textBoxNick);
            this.Controls.Add(this.gameRoomsList);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ClientForm";
            this.Text = "Client Lobby";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.Button buttonSendMessage;
        private System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.Label labelServerIp;
        private System.Windows.Forms.Label labelServerPort;
        private System.Windows.Forms.TextBox textBoxServerIp;
        private System.Windows.Forms.TextBox textBoxServerPort;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.RichTextBox textBoxInfo;
        private System.Windows.Forms.ListBox listBoxClients;

        private System.Windows.Forms.Button buttonCreateGameRoom;
        private System.Windows.Forms.Button buttonJoinGameRoom;
        //private System.Windows.Forms.Button buttonCloseGameRoom;
        private System.Windows.Forms.Button buttonExitGameRoom;

        private System.Windows.Forms.ListView gameRoomsList;
        //private ColumnHeader columnOwner;
        //private ColumnHeader columnNoPlayers;

        private System.Windows.Forms.TextBox textBoxNick;
        private Label label1;
        private ListView listGameRoomPlayers;
        private ColumnHeader columnRoomPlayer;
        private TextBox textBoxGameRoomSendMessage;
        private Label labelRoom;
        private Label label2;
        private Button buttonGameRoomSendMessage;
        private RichTextBox textBoxGameChat;
    }
}
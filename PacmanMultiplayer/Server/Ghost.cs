﻿using System.Collections.Generic;
using System;
using Server;

namespace Server
{
    public class Ghost : Actor
    {
        private const int SPAWN_COIN_RATE = 10;
        private const int SPAWN_SUPER_COIN_RATE = 4;
        public static Random Randomizer = new Random((int)DateTime.Now.Ticks & 0xffff);

        private System.Timers.Timer m_Timer;

        private bool m_IsKiller;

        private MapItem m_GhostVal;

        private Pacman m_PlayerToHaunt;

        private Stack<MapDirection> m_PathToAttack;

        private int m_HauntX;
        private int m_HauntY;

        public MapItem Val
        {
            get => m_GhostVal;
        }


        public Ghost(MapItem id, int posX, int posY, GameBoard gameBoard)
            : base((UInt32)id, posX, posY, gameBoard)
        {
            m_GhostVal = id;
            m_IsKiller = false;
            m_HauntX = -1;
            m_HauntY = -1;
            m_PlayerToHaunt = null;
            m_PathToAttack = null;

            m_Timer = new System.Timers.Timer(20 * 1000);
            m_Timer.Enabled = false;
            m_Timer.Elapsed += new System.Timers.ElapsedEventHandler(stopKill);
        }


        public void Attack(Pacman player)
        {
            if (m_PosX != player.X || m_PosY != player.Y)
            {
                m_PathToAttack =
                        Util.BfsSearchPath(
                            m_GameMap,
                            m_PosX,
                            m_PosY,
                            player.X,
                            player.Y
                        );

                m_PlayerToHaunt = player;
                m_HauntX = player.X;
                m_HauntY = player.Y;

                m_IsKiller = true;
                m_Timer.Enabled = true;

            }
            /*
            else if (m_PathToAttack != null) 
            {
                m_PathToAttack.Clear();
                m_IsKiller = false;
                m_Timer.Enabled = false;
	    } 
            */
        }


        public void StopHaunt()
        {
            m_Timer.Enabled = false;
            m_IsKiller = false;
            m_PathToAttack.Clear();
        }


        private void stopKill(object sender, EventArgs e)
        {
            m_IsKiller = false;
            m_Timer.Enabled = false;
        }


        public Coin MoveGhost()
        {
            MapDirection nextDirection = m_CurrentDirection;

            if (m_IsKiller)
            {
                // We need to recalculate the path
                if (m_PlayerToHaunt.X != m_HauntX || m_PlayerToHaunt.Y != m_HauntY)
                {
                    m_PathToAttack =
                            Util.BfsSearchPath(
                                m_GameMap,
                                m_PosX,
                                m_PosY,
                                m_PlayerToHaunt.X,
                                m_PlayerToHaunt.Y
                            );
                }

                if (m_PathToAttack == null || m_PathToAttack.Count == 0)
                {
                    // We catch the pacman
                    m_PlayerToHaunt.DoRespawn();
                    m_Timer.Enabled = false;
                    m_IsKiller = false;
                    m_CurrentDirection = MapDirection.NONE;
                    nextDirection = MapDirection.NONE;
                }
                else
                {
                    /*
                    if (Math.Floor(m_GraphicsX) == m_GraphicsX &&
                        Math.Floor(m_GraphicsY) == m_GraphicsY)
                    {
                    */
                    nextDirection = m_PathToAttack.Pop();
                    /*
                    }
                    */
                }

                // They wont drop anythng
                //return null;
            }
            else
            {
                if (!check_direction(m_CurrentDirection))
                {
                    int[] choices = { 0, 0, 0, 0 };
                    int num_of_coices = 0;
                    // We need to change Direction(another than m_Direction)
                    switch (m_CurrentDirection)
                    {
                        case MapDirection.NONE:
                            if (check_direction(MapDirection.NORTH))
                                choices[num_of_coices++] = (int)MapDirection.NORTH;

                            if (check_direction(MapDirection.SOUTH))
                                choices[num_of_coices++] = (int)MapDirection.SOUTH;

                            if (check_direction(MapDirection.WEST))
                                choices[num_of_coices++] = (int)MapDirection.WEST;

                            if (check_direction(MapDirection.EAST))
                                choices[num_of_coices++] = (int)MapDirection.EAST;

                            break;

                        case MapDirection.NORTH:
                            /*
                            if (check_direction(MapDirection.SOUTH))       
                                choices[num_of_coices++] = (int)MapDirection.SOUTH;
                            */
                            if (check_direction(MapDirection.WEST))
                                choices[num_of_coices++] = (int)MapDirection.WEST;

                            if (check_direction(MapDirection.EAST))
                                choices[num_of_coices++] = (int)MapDirection.EAST;

                            break;

                        case MapDirection.SOUTH:
                            /*
                            if (check_direction(MapDirection.NORTH))       
                                choices[num_of_coices++] = (int)MapDirection.NORTH;
                            */
                            if (check_direction(MapDirection.WEST))
                                choices[num_of_coices++] = (int)MapDirection.WEST;

                            if (check_direction(MapDirection.EAST))
                                choices[num_of_coices++] = (int)MapDirection.EAST;

                            break;

                        case MapDirection.EAST:
                            if (check_direction(MapDirection.SOUTH))
                                choices[num_of_coices++] = (int)MapDirection.SOUTH;

                            /*
                            if (check_direction(MapDirection.WEST)) 
                                choices[num_of_coices++] = (int)MapDirection.WEST;
                            */

                            if (check_direction(MapDirection.NORTH))
                                choices[num_of_coices++] = (int)MapDirection.NORTH;

                            break;

                        case MapDirection.WEST:
                            if (check_direction(MapDirection.SOUTH))
                                choices[num_of_coices++] = (int)MapDirection.SOUTH;

                            if (check_direction(MapDirection.NORTH))
                                choices[num_of_coices++] = (int)MapDirection.NORTH;

                            /*
                            if (check_direction(MapDirection.EAST))  
                                choices[num_of_coices++] = (int)MapDirection.EAST;
                            */
                            break;
                    }

                    nextDirection =
                        (MapDirection)choices[Randomizer.Next(0, num_of_coices)];
                }
                else if (Math.Floor(m_GraphicsY) == m_GraphicsY &&
                         Math.Floor(m_GraphicsX) == m_GraphicsX)
                {
                    int[] choices = { 0, 0, 0, 0 };
                    int num_of_coices = 0;

                    switch (m_CurrentDirection)
                    {
                        case MapDirection.NORTH:
                            if (check_direction(MapDirection.NORTH))
                                choices[num_of_coices++] = (int)MapDirection.NORTH;

                            if (check_direction(MapDirection.WEST))
                                choices[num_of_coices++] = (int)MapDirection.WEST;

                            if (check_direction(MapDirection.EAST))
                                choices[num_of_coices++] = (int)MapDirection.EAST;

                            break;

                        case MapDirection.SOUTH:
                            if (check_direction(MapDirection.SOUTH))
                                choices[num_of_coices++] = (int)MapDirection.SOUTH;

                            if (check_direction(MapDirection.WEST))
                                choices[num_of_coices++] = (int)MapDirection.WEST;

                            if (check_direction(MapDirection.EAST))
                                choices[num_of_coices++] = (int)MapDirection.EAST;

                            break;

                        case MapDirection.EAST:
                            if (check_direction(MapDirection.SOUTH))
                                choices[num_of_coices++] = (int)MapDirection.SOUTH;

                            if (check_direction(MapDirection.EAST))
                                choices[num_of_coices++] = (int)MapDirection.EAST;

                            if (check_direction(MapDirection.NORTH))
                                choices[num_of_coices++] = (int)MapDirection.NORTH;

                            break;

                        case MapDirection.WEST:
                            if (check_direction(MapDirection.SOUTH))
                                choices[num_of_coices++] = (int)MapDirection.SOUTH;

                            if (check_direction(MapDirection.NORTH))
                                choices[num_of_coices++] = (int)MapDirection.NORTH;

                            if (check_direction(MapDirection.WEST))
                                choices[num_of_coices++] = (int)MapDirection.WEST;

                            break;
                    }

                    nextDirection =
                        (MapDirection)choices[Randomizer.Next(0, num_of_coices)];
                }
            }

            m_CurrentDirection = nextDirection;

            return move(m_CurrentDirection);
        }



        /*
        private Coin move(MapDirection direction) 
        {
            int oldX, oldY, newX, newY, chance;
        Coin droppedCoin = null;
            
            oldX = m_PosX;
            oldY = m_PosY;
            
        switch (direction) 
            {
            case MapDirection.NORTH:
                m_GraphicsY -= Util.MOVE_OFFSET;
                m_PosY = (int)Math.Ceiling(m_GraphicsY);
        break;

            case MapDirection.EAST:
                m_GraphicsX += Util.MOVE_OFFSET;
                m_PosX = (int)Math.Floor(m_GraphicsX);
                break;

            case MapDirection.SOUTH: 
                m_GraphicsY += Util.MOVE_OFFSET;
                m_PosY = (int)Math.Floor(m_GraphicsY);
                break;

            case MapDirection.WEST:
                m_GraphicsX -= Util.MOVE_OFFSET;
                m_PosX = (int)Math.Ceiling(m_GraphicsX);
                break;

            }
        
            newX = m_PosX;
            newY = m_PosY;

            if (m_GameMap.Matrix[oldY, oldX] == (int)MapItem.GOL) 
            {
                chance = Randomizer.Next(0, 100);

                if (chance < SPAWN_COIN_RATE) 
                {
                    m_GameMap.Matrix[oldY, oldX] = (int)MapItem.BANUT_MIC;
                    droppedCoin = new Coin(MapItem.BANUT_MIC, oldY, oldX);
                    
        } 
                else if (chance < SPAWN_COIN_RATE + SPAWN_SUPER_COIN_RATE) 
                {
                    m_GameMap.Matrix[oldY, oldX] = (int)MapItem.BANUT_MARE;
                    droppedCoin = new Coin(MapItem.BANUT_MARE, oldY, oldX);
                }
            }   
        
            return droppedCoin;
    }

        */


        private Coin move(MapDirection direction)
        {
            int oldX, oldY, newX, newY, chance;
            Coin droppedCoin = null;

            oldX = m_PosX;
            oldY = m_PosY;

            switch (direction)
            {
                case MapDirection.NORTH:
                    --m_PosY;
                    break;

                case MapDirection.EAST:
                    ++m_PosX;
                    break;

                case MapDirection.SOUTH:
                    ++m_PosY;
                    break;

                case MapDirection.WEST:
                    --m_PosX;
                    break;
            }

            newX = m_PosX;
            newY = m_PosY;

            if (m_GameMap.Matrix[oldY, oldX] == (int)MapItem.GOL)
            {
                chance = Randomizer.Next(0, 100);

                if (chance < SPAWN_COIN_RATE)
                {
                    m_GameMap.Matrix[oldY, oldX] = (int)MapItem.BANUT_MIC;
                    droppedCoin = new Coin(MapItem.BANUT_MIC, oldY, oldX);
                }
                else if (chance < SPAWN_COIN_RATE + SPAWN_SUPER_COIN_RATE)
                {
                    m_GameMap.Matrix[oldY, oldX] = (int)MapItem.BANUT_MARE;
                    droppedCoin = new Coin(MapItem.BANUT_MARE, oldY, oldX);
                }
            }
            return droppedCoin;
        }


        private bool check_direction(MapDirection direction)
        {
            switch (direction)
            {
                case MapDirection.NORTH:
                    return direction_ok(m_PosX, m_PosY - 1);

                case MapDirection.EAST:
                    return direction_ok(m_PosX + 1, m_PosY);

                case MapDirection.SOUTH:
                    return direction_ok(m_PosX, m_PosY + 1);

                case MapDirection.WEST:
                    return direction_ok(m_PosX - 1, m_PosY);

                default:
                    return false;
            }
        }


        private bool direction_ok(int x, int y)
        {
            switch ((MapItem)m_GameMap.Matrix[y, x])
            {
                case MapItem.ZID:
                case MapItem.GHOST1:
                case MapItem.GHOST2:
                case MapItem.GHOST3:
                case MapItem.GHOST4:
                    return false;
                default:
                    return true;
            }
        }
    }
}

﻿using static System.Reflection.MethodBase;
using System.Collections.Generic;
using System;
using Server;

namespace Server
{

    public enum MapDirection
    {
        NONE = 0, NORTH, EAST, SOUTH, WEST
    }

    class Point
    {
        public int X, Y;

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }

    class Node
    {
        public MapDirection Dir;
        public Point Coord;
        public Node Parent;
    }


    public class Util
    {
        public const float MOVE_OFFSET = 0.1F;
        //S E   N   V  
        public static readonly int[] DIR_X = { 0, 1, 0, -1 };
        public static readonly int[] DIR_Y = { 1, 0, -1, 0 };


        public static readonly MapDirection[] DIRECTION = {
            MapDirection.SOUTH, MapDirection.EAST,
            MapDirection.NORTH, MapDirection.WEST
        };

        public static bool[,] was = new bool[GameBoard.DIM_Y, GameBoard.DIM_X];

        public static readonly string ProjectResPath =
        System.IO.Directory.GetCurrentDirectory() +
        System.IO.Path.DirectorySeparatorChar +
        "Resources" +
        System.IO.Path.DirectorySeparatorChar;

        /*
        public static readonly string ProjectResPath =
            System.IO.Directory.GetCurrentDirectory() +
            System.IO.Path.DirectorySeparatorChar +
            ".." +
            System.IO.Path.DirectorySeparatorChar +
            ".." +
            System.IO.Path.DirectorySeparatorChar +
            "Resources" +
            System.IO.Path.DirectorySeparatorChar;
	*/

        public static Stack<MapDirection>
        BfsSearchPath(
            GameBoard map,
            int startX, int startY,
            int targetX, int targetY)
        {
            int dir, newX, newY, i, j;
            Queue<Node> queue = new Queue<Node>();
            Stack<MapDirection> back = new Stack<MapDirection>();
            Node startNode, currentNode, newNode;

            startNode = new Node();
            startNode.Coord = new Point(startX, startY);
            startNode.Dir = 0;
            startNode.Parent = null;

            for (i = 0; i < GameBoard.DIM_Y; ++i)
                for (j = 0; j < GameBoard.DIM_X; ++j)
                    was[i, j] = false;

            queue.Enqueue(startNode);
            was[startY, startX] = true;

            while (queue.Count > 0)
            {
                currentNode = queue.Dequeue();
                Point currentPoint = currentNode.Coord;

                if (currentPoint.X == targetX && currentPoint.Y == targetY)
                {
                    while (currentNode != startNode)
                    {
                        back.Push(currentNode.Dir);
                        currentNode = currentNode.Parent;
                    }

                    /*
                    Console.WriteLine("---- DEBUG ----");
                    int gX, gY;
                    int d;
                    gX = this.X;
                    gY = this.Y;
                    while (back.Count > 0) 
                    {
                        map.Matrix[gY, gX] = 11;
                        map.Print();

                        d = back.Pop();
                        switch(d) 
                        {
                        case 1: gY--; break; // Nord
                        case 2: gX++; break; // Est
                        case 3: gY++; break; // Sud
                        case 4: gX--; break; // Vest
                        }                           
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                    }
                    Console.WriteLine("---- DEBUG ----");           
                    */
                    return back;
                }

                for (dir = 0; dir < 4; ++dir)
                {
                    newX = currentPoint.X + DIR_X[dir];
                    newY = currentPoint.Y + DIR_Y[dir];

                    if (!map.IsValidPoint(newX, newY) || was[newY, newX])
                    {
                        continue;
                    }

                    was[newY, newX] = true;

                    newNode = new Node();
                    newNode.Coord = new Point(newX, newY);
                    newNode.Parent = currentNode;
                    newNode.Dir = DIRECTION[dir];

                    queue.Enqueue(newNode);
                }
            }

            return null;
        }
    }
}

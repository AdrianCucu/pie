﻿using static System.Reflection.MethodBase;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Net;
using System;
using Network;

namespace Server
{
    public class ObjAsync
    {
        public Socket socket;
        public byte[] buffer;
        public int size;
        public int offset;
    }


    public class ClientContext
    {
        public const int BUFFER_SIZE = 4096;
    
        public ObjAsync mObjAsync;
        private object mGameRoomContext;
        private Socket mSocket;
        private IPEndPoint mRemoteEndPoint;
        private UInt32 mId;
        private string mNick;
        private Server mOwner;
        private bool mInGameRoom;
        private volatile int mLostConnectionFired;
        /*
        private bool m_SendingInProgress;
        private Queue<NetPacket> m_OutgoingPackets;
        private DateTime time;
        private readonly object m_SendSyncRoot = new object();
        private readonly object receiveSyncRoot = new object();
        */
    

        public GameRoom Room
        {
            get => mGameRoomContext as GameRoom;
        }

        public UInt32 Id
        {
            get => mId;
        }
        public IPAddress Ip
        {
            get => mRemoteEndPoint.Address;
        }
        public int Port
        {
            get => mRemoteEndPoint.Port;
        }
        public bool InGame
        {
            get => mInGameRoom;
        }
        public bool IsConnected
        {
            get => mSocket.Connected;
        }
        public string Nick
        {
            get => mNick;
        }
        
        public ClientContext()
        {
            mObjAsync = new ObjAsync();
            mObjAsync.buffer = new byte[BUFFER_SIZE];
            mObjAsync.offset = 0;
            mObjAsync.size = 0;

            //m_OutgoingPackets = new Queue<NetPacket>(); 
            //m_SendingInProgress = false; 
            mInGameRoom = false;
            mLostConnectionFired = 0;
        }
        
        public ClientContext(Socket socket, UInt32 id)
            : this()
        {
            SetSocket(socket);
            SetId(id);
        }
        
        public void SetSocket(Socket socket)
        {
            mSocket = socket;
            mObjAsync.socket = mSocket;
            mRemoteEndPoint = mSocket.RemoteEndPoint as IPEndPoint;
        }
        
        public void SetId(UInt32 id)
        {
            mId = id;
        }
        
        public void SetNick(string nick)
        {
            mNick = nick;
        }
        
        public void SetOwner(Server owner)
        {
            mOwner = owner;
        }
        
        public void SetGameRoom(object context)
        {
            mGameRoomContext = context;
            mInGameRoom = true;
        }

        public void ExitGameRoom()
        {
            if (mGameRoomContext != null)
            {
                mGameRoomContext = null;
            }
            mInGameRoom = false;
        }
        
        public void Begin()
        {
            if (mSocket == null)
            {
                throw new InvalidOperationException("[ERROR] m_Socket is NULL");
            }

            SocketError se;
            mSocket.BeginReceive(
                   mObjAsync.buffer,
                    mObjAsync.offset,
                    mObjAsync.buffer.Length - mObjAsync.offset,
                    SocketFlags.None,
                    out se,
                    AsyncRecv,
                    mObjAsync
            );

            if (se != SocketError.Success && se != SocketError.IOPending)
            {
                Disconnect();
                FireOnLostConnection();
                return;
            }
        }
        
        private void ParsePacket(Packet p)
        {
            switch (p.Type)
            {
                case PacketType.Connection.FIRST_HELLO:
                    mOwner.OnFirstHello(this);
                    break;
                case PacketType.Connection.CLIENT_DISCONNECT:
                    mOwner.DisconnectClient(this);
                    Disconnect();
                    break;
                case PacketType.Request.PM:
                    {
                        UInt32 receiverId = p.ReadUInt32();
                        string message = p.ReadAscii();
                        mOwner.SendPrivateMessage(this, receiverId, message);
                    }
                    break;
                case PacketType.Request.MOVE_REQUEST:
                    {
                        if (mInGameRoom)
                        {
                            int dir = p.ReadInt32();
                            //debug(String.Format("MOVE REQUEST {0}", dir));
                                
                            ((GameRoom)mGameRoomContext).MoveRequest(this, dir);
                        }
                    }
                    break;
                case PacketType.Request.CREATE_ROOM:
                    {
                        if (!mInGameRoom)
                        {
                            //       debug("CREATE ROOM REQUEST ACCEPTED\nCreating room...");

                            mOwner.CreateGameRoom(this);
                        }
                        else
                        {
                            //debug("CAN'T CREATE ROOM BECAUSE IS IN A GAME ROOM ALREADY");
                        }
                    }
                    break;
                case PacketType.Request.JOIN_ROOM:
                    {
                        if (!mInGameRoom)
                        {
                            UInt32 roomId = p.ReadUInt32();

                            //debug(String.Format("JOIN GAME ROOM: {0}", roomId));

                            mOwner.JoinGameRoom(this, roomId);
                        }
                        else
                        {
                            //debug("CAN'T JOIN ROOM BECAUSE IS IN A GAME ROOM ALREADY");
                        }
                    }
                    break;
                    /*
                case PacketType.Request.CLOSE_ROOM:
                    {
                        if (m_InGameRoom)
                        {
                            //UInt32 roomId = p.ReadUInt32();
                            //debug(String.Format(
                            //    "CLOSE THE ROOM {0}",
                            //    ((GameRoom)m_GameRoomContext).Id
                            //));

                            m_Owner.CloseGameRoom(this,
                                m_GameRoomContext as GameRoom);
                        }
                        else
                        {
                            //debug("CAN'T CLOSE ANY ROOM BECAUSE IS NOT IN GAME ROOM");
                        }
                    }
                    break;
                    */
               case PacketType.Request.EXIT_ROOM:
                    {
                        Console.WriteLine("EXIT GAME ROOM REQUEST");
                        if (mInGameRoom)
                        {

                            Console.WriteLine("EXIT GAME ROOM REQUEST ok");
                            //debug(String.Format(
                            //    "LEAVE THE ROOM {0}",
                            //    ((GameRoom)m_GameRoomContext).Id
                            //));

                            mOwner.ExitGameRoom(this, mGameRoomContext as GameRoom);
                        }
                        else
                        {
                            //debug("CAN'T LEAVE ANY ROOM BECAUSE IS NOT IN GAME ROOM");
                        }
                    }
                    break;
                case PacketType.Request.MESSAGE_ROOM:
                    {
                        if (mInGameRoom)
                        {
                            UInt32 roomId = p.ReadUInt32();
                            string message = p.ReadAscii();

                            ((GameRoom)mGameRoomContext).SendRoomMessage(this, message);
                        }
                    }
                    break;
                case PacketType.Request.START_GAME:
                    {
                        if (mInGameRoom)
                        {
                            if (((GameRoom)mGameRoomContext).Owner == this)
                            {
                                Debug(String.Format("START GAME REQUEST ACCEPTED FO ROOM: {0}", ((GameRoom)mGameRoomContext).Id));

                                if (((GameRoom)mGameRoomContext).ClientsCount > 1)
                                {
                                    Debug("Let's start the game!!!!!!!");
                                    (mGameRoomContext as GameRoom).StartGame();
                                }
                                else
                                {
                                    Debug("Can't start the game because it's only 1 player");
                                }
                            }
                            else
                            {
                                Debug("Only the owner of this room can start game!!!!!!!!!!!!!");
                            }
                        }
                        else
                        {
                            Debug("CAN'T START GAME BECAUSE HE IS NOT IN A GAME ROOM");
                        }
                    }
                    break;
                default:
                    {
                        Console.WriteLine("Packet type unknown [{0:X4]", p.Type);
                    }
                    break;
            }
        }

        private void Debug(string message)
        {
            Console.WriteLine("Client {0}) {1} {2}", mId, mNick, message);
        }
        
        public void SendWelcome()
        {
            Packet p = new Packet(PacketType.Connection.WELCOME);
            p.WriteAscii("Welcome to my server");
            SendPacket(p);
        }
        
        public void SendGameRoomJoinFail()
        {
            Packet p = new Packet(PacketType.Response.JOIN_ROOM_REQUEST_FAILED);
            SendPacket(p); 
        }
        
        public void SendHello()
        {
            Packet p = new Packet(PacketType.Connection.HELLO);
            SendPacket(p);
        }
        
        public void SendGameOver()
        {
            Packet p = new Packet(PacketType.Game.GAME_OVER);
            SendPacket(p);
        }
        
        public void SendDisconnect(string disconectMsg = "You was disconnected")
        {
            Packet p = new Packet(PacketType.Connection.SERVER_DISCONNECT);
            p.WriteAscii(disconectMsg);
            SendPacket(p);
        }
        
        public void SendMessageFrom(ClientContext from, string msg)
        {
            Packet p = new Packet(PacketType.Request.PM);
            p.WriteUInt32(from.Id);
            p.WriteAscii(from.Nick);
            p.WriteAscii(msg);
            SendPacket(p);
        }
        
        public void SendOneClientDisconnected(UInt32 id)
        {
            Packet p = new Packet(PacketType.Info.CLIENT_LEFT);
            p.WriteUInt32(id);
            SendPacket(p);
        }
        
        public void SendGameRoomCreated(ClientContext owner)
        {
            Packet p = new Packet(PacketType.Info.GAME_ROOM_CREATED);
            p.WriteUInt32(owner.Id);
            p.WriteAscii(owner.Nick);
            SendPacket(p);
        }

        public void SendOwnerClosedRoom()
        {
            Packet p = new Packet(PacketType.Info.GAME_ROOM_CLOSED);
            SendPacket(p);
        }
        
        public void SendGameRoomClientJoined(ClientContext client)
        {
            Packet p = new Packet(PacketType.Info.GAME_ROOM_CLIENT_JOINED);
            p.WriteUInt32(client.Id);
            p.WriteAscii(client.Nick);
            p.WriteUInt32((mGameRoomContext as GameRoom).Id);
            SendPacket(p);
        }
        
        public void SendGameRoomClientLeft(ClientContext client, UInt32 gameRoomId)
        {
            Packet p = new Packet(PacketType.Info.GAME_ROOM_CLIENT_LEFT);
            p.WriteUInt32(client.Id);
            p.WriteAscii(client.Nick);
            p.WriteUInt32(gameRoomId);
            SendPacket(p);
        }
        
        public void SendPlayerExitedRoom(ClientContext client)
        {
            Packet p = new Packet(PacketType.Info.GAME_ROOM_CLIENT_LEFT);
            p.WriteUInt32(client.Id);
            p.WriteAscii(client.Nick);
            p.WriteUInt32((mGameRoomContext as GameRoom).Id);
            SendPacket(p);
        }
        
        public void SendPacket(Packet p)
        {
            byte[] raw = p.Pack();

            if (mSocket.Connected && raw != null)
            {   
                //int size = m_Socket.Send(raw, 0, raw.Length, 0);
                mSocket.BeginSend(raw, 0, raw.Length, 0, AsyncSend, mSocket);
            }
        }
        
     //   public void Send(Packet p) 
     //   {
     //       if (!m_Socket.Connected) 
	    //    return; 
     //       //int bsend = m_Socket.Send(byteData, 0, byteData.Length, 0);
     //       //m_Socket.BeginSend(rawData, 0, rawData.Length, 0, AsyncSend, m_Socket);
            
     //       lock (this)
     //       { 
     //           m_OutgoingPackets.Enqueue(p);         
     //           ProcessOutgoingList();
     //       }
    	//}

        //public void ProcessOutgoingList()
        //{
        //    try
        //    {
        //        lock (this)
        //        {
        //            if (m_SendingInProgress)
        //                return;
                    
        //            if (m_OutgoingPackets.Count > 0)
        //            {
        //                m_SendingInProgress = true;
                    
        //                Packet p = m_OutgoingPackets.Dequeue();
                        
        //            }
        //        }
        //    }   
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }
    
        //}

        private void AsyncRecv(IAsyncResult res)
        {
            try
            {
                //ObjAsync m_ObjAsync = (ObjAsync)res.AsyncStat

                if (!mSocket.Connected)
                {
                    FireOnLostConnection();
                    return;
                }

                SocketError se;
                int bytesRecv = mSocket.EndReceive(res, out se);
            
                if (se != SocketError.Success)
                {
                    FireOnLostConnection();
                    return;
                }

                if (bytesRecv <= 0)
                {
                    FireOnLostConnection();
                    return;
                }

                //Console.WriteLine("Received: {0} bytes.", brecv);
                //dumpRecvBuffer(0, size); 

                mObjAsync.size += bytesRecv;

                int index = 0;
                int bsize = mObjAsync.size;

                while (index + Packet.HEADER_LEN <= mObjAsync.size)
                {
                    int packet_size = (mObjAsync.buffer[index + 1] << 8) | (mObjAsync.buffer[index]);

                    if (index + packet_size > mObjAsync.size)
                    {
                        break;
                    }

                    int packet_type = (mObjAsync.buffer[index + 3] << 8) | (mObjAsync.buffer[index + 2]);

                    int payload_size = packet_size - Packet.HEADER_LEN;

                    PacketReader pr = new PacketReader(mObjAsync.buffer, index + Packet.HEADER_LEN, payload_size);

                    Packet p = new Packet((ushort)packet_type, pr.ReadBytes(payload_size));

                    p.Lock();
                    ParsePacket(p);

                    index += packet_size;
                    bsize -= packet_size;
                }

                if (index > 0 && bsize > 0)
                {
                    int i;
                    for (i = 0; i < bsize; ++i)
                        mObjAsync.buffer[i] = mObjAsync.buffer[index++];
                    mObjAsync.offset = i - 1;
                }
                else
                {
                    mObjAsync.offset = bsize;
                }

                mObjAsync.size = bsize;

                // begin receive again
                mSocket.BeginReceive(
                    mObjAsync.buffer,
                    mObjAsync.offset,
                    mObjAsync.buffer.Length - mObjAsync.offset,
                    SocketFlags.None,
                    out se,
                    AsyncRecv,
                    mObjAsync
                );

                if (se != SocketError.Success && se != SocketError.IOPending)
                {
                    FireOnLostConnection();
                }
            }
            catch (System.ObjectDisposedException objDisposedEx)
            {
                Console.WriteLine("Socket has been closed");
                Console.WriteLine(objDisposedEx);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0}->{1}",
                        GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }
        
        private void AsyncSend(IAsyncResult res)
        {
            try
            {
                lock (this)
                {
                    if (!mSocket.Connected)
                    {
                        FireOnLostConnection();
                        return;
                    }

                    SocketError se;
                    int bytesSent = mSocket.EndSend(res, out se);

                    if (se != SocketError.Success)
                    {
                        FireOnLostConnection();
                        return;
                    }

                    if (bytesSent <= 0)
                    {
                        FireOnLostConnection();
                        return;
                    }
                }
            }
            catch (System.ObjectDisposedException objDisposedEx)
            {
                Console.WriteLine("Socket has been closed");
                Console.WriteLine(objDisposedEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name); 
                Console.WriteLine(ex);
            }
        }
        
        private void FireOnLostConnection()
        {
            if (Interlocked.CompareExchange(ref mLostConnectionFired, 1, 0) == 0)
            {
                Console.WriteLine("##################################");
                Console.WriteLine("Connection lost with");
                Console.WriteLine("# {0})  {1}", mId, mNick);
                Console.WriteLine("##################################");

                OnLostConnection();
            }
        }
        
        public void OnLostConnection()
        {
            try
            {
                /*
                if (m_InGameRoom)
                {
                    //Disconnect from game
                    
                    PlayerContext playerCtx = this.Context;

                    if (playerCtx != null)
                    {
                        Console.WriteLine("Disconnecting #{0} from game", m_Id);
                        Console.WriteLine("Is a player so need to DC");
                        playerCtx.OnDisconnect();
                    }
                    
                    GameRoom gameRoom = m_GameRoomContext as GameRoom;
                    if (gameRoom != null)
                    {
                
                    }
                }
                */

                if (mOwner != null)
                {
                    Console.WriteLine("Disconnecting me");
                    mOwner.DisconnectClient(this);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name); 
                Console.WriteLine(ex);
            }
            finally
            {
                mInGameRoom = false;
            }
        }
        
        public void Disconnect()
        {
            try
            {
                lock (this)
                {
                    //Disconnecting the socket...
                    if (mSocket != null)
                    {
                        if (mSocket.Connected)
                        {
                            mSocket.Shutdown(SocketShutdown.Both);
                            //m_Socket.Disconnect(true);
                            mSocket.Close();
                        }
                    }
                    FireOnLostConnection();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name); 
                Console.WriteLine(ex);
            }
            finally
            {
                mInGameRoom = false;
            }
        }

        public override string ToString()
        {
            return String.Format("#{0} [{1}:{2}]  {3}\n", this.Id, this.Ip, this.Port, this.Nick);
        }
    }
}
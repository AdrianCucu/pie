﻿using static System.Reflection.MethodBase;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using System.Text;
using System.IO;
using System;
using Network;
using System.Diagnostics;

namespace Server
{
    public class GameRoom
    {
        //private const int GAME_PLAYERS = 10000000;
        private const int GAME_DURATION = 200; //sec
        private const double GAME_DEELAY = 0.1;

        private UInt32 mGameRoomId;
        private ClientContext mGameRoomOwner;

        private System.Timers.Timer mGameTimer;

        private Thread mGameThread;

        private List<ClientContext> mClientsList;

        private Dictionary<ClientContext, Pacman> mPacmansMap;
        private List<Ghost> mGhostsList;

        private readonly ConcurrentQueue<Tuple<ClientContext, Int32>> queue = new ConcurrentQueue<Tuple<ClientContext, Int32>>();

        private object mLock;

        private GameBoard mGameBoard;

        private long mGameStartTime;

        private bool mGameRunning;

        private bool mGameAborted;


        public bool IsOpen
        {
            get => !mGameRunning;
        }
        public ClientContext Owner
        {
            get => mGameRoomOwner;
        }
        public UInt32 Id
        {
            get => mGameRoomId;
        }
        public int ClientsCount
        {
            get => mClientsList.Count;
        }


        public GameRoom()
        {
            mGameRunning = false;
            mGameAborted = false;

            mGameThread = new Thread(new ThreadStart(GameLoop));
            mClientsList = new List<ClientContext>();

            mGameBoard = new GameBoard();
            mPacmansMap = new Dictionary<ClientContext, Pacman>();
            mGhostsList = new List<Ghost>();

            mGameTimer = new System.Timers.Timer();
            mGameTimer.Elapsed += new System.Timers.ElapsedEventHandler(gameEndedTimer_Tick);
        }

        public GameRoom(ClientContext owner)
            : this()
        {
            SetOwner(owner);
        }

        public void SetOwner(ClientContext owner)
        {
            mGameRoomOwner = owner;
            mGameRoomId = owner.Id;
            owner.SetGameRoom(this);
            mClientsList.Add(owner);
        }

        public void Join(ClientContext client)
        {
            try
            {
                Console.WriteLine("Client: {0} wants to join game room", client.Nick);
                if (IsOpen)
                {
                    client.SetGameRoom(this);
                    foreach (ClientContext clientCtx in mClientsList)
                    {
                        Console.WriteLine("Sending to: {0} that {1} joined room", clientCtx.Nick, client.Nick);

                        clientCtx.SendGameRoomClientJoined(client);
                    }

                    mClientsList.Add(client);

                    // Send to the new client all info about this room
                    Packet p = new Packet(PacketType.Info.GAME_ROOM_INFO);
                    p.WriteUInt32(mGameRoomId);

                    p.WriteInt32(mClientsList.Count);
                    foreach (ClientContext clientCtx in mClientsList)
                    {
                        p.WriteUInt32(clientCtx.Id);
                        p.WriteAscii(clientCtx.Nick);
                    }

                    p.WriteUInt32(mGameRoomOwner.Id);
                    p.WriteAscii(mGameRoomOwner.Nick);

                    client.SendPacket(p);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        public void Left(ClientContext client)
        {
            try
            {
                if (mGameRunning)
                {
                    StopGame();
                }

                mClientsList.Remove(client);

                if (client == mGameRoomOwner)
                {
                    Console.WriteLine(
                        "Client {0} want to leave room but is the OWNER",
                        client.Nick
                    );

                    foreach (ClientContext clientCtx in mClientsList)
                    {
                        clientCtx.SendOwnerClosedRoom();
                        clientCtx.ExitGameRoom();
                    }
                }
                else
                {
                    foreach (ClientContext clientCtx in mClientsList)
                        clientCtx.SendPlayerExitedRoom(client);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        public void Close()
        {
            try
            {
                Console.WriteLine("Closing the game room {0}.", mGameRoomId);

                if (mGameRunning)
                {
                    StopGame();
                }

                mGameRoomOwner.ExitGameRoom();

                foreach (ClientContext clientCtx in mClientsList)
                {
                    clientCtx.SendOwnerClosedRoom();
                    clientCtx.ExitGameRoom();
                }

                Console.WriteLine("Game room {0} closed.", mGameRoomId);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        public void SendRoomMessage(ClientContext client, string message)
        {
            Packet p = new Packet(PacketType.Request.MESSAGE_ROOM);
            p.WriteAscii(client.Nick);
            p.WriteAscii(message);
            foreach (ClientContext clientCtx in mClientsList)
            {
                clientCtx.SendPacket(p);
            }
        }

        public void InitGame()
        {
            try
            {
                Console.WriteLine(" Initializing the game...");

                mGameTimer.Interval = GAME_DURATION * 1000;

                mGameBoard.makeCleanMap();

                mGhostsList.Clear();

                foreach (Tuple<int, int, MapItem> ghostInfo in mGameBoard.putGhostsOnMap())
                {
                    int x = ghostInfo.Item1;
                    int y = ghostInfo.Item2;
                    MapItem ghostVal = ghostInfo.Item3;
                    Ghost newGhost = new Ghost(ghostVal, x, y, mGameBoard);
                    mGhostsList.Add(newGhost);
                }

                foreach (ClientContext clientCtx in mClientsList)
                {
                    Pacman pacman = mGameBoard.putPacmanOnMap(clientCtx.Id);
                    mPacmansMap[clientCtx] = pacman;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        public void StartGame()
        {
            try
            {
                Console.WriteLine("[DEBUG]Starting the game ...");
                InitGame();

                Console.WriteLine("[DEBUG]Sending GAME init ...");
                SendGameInit();

                Console.WriteLine("[DEBUG]Sending PACMANS positions ...");
                SendPacmansPositions();

                Console.WriteLine("[DEBUG]Sending GHOSTS positions ...");
                SendGhostsPositions();

                Console.WriteLine("[DEBUG]Sending START_GAME[GAME DURATION] ...");
                SendGameStart();
         
                // Starting GameLoop
                mGameRunning = true;
                mGameThread = new Thread(new ThreadStart(GameLoop));
                mGameThread.Start();

                mGameStartTime = Environment.TickCount;
                mGameTimer.Enabled = true;

                Console.WriteLine("Game started at: {0}", DateTime.Now);
                Console.WriteLine("Game duration  : {0} sec", GAME_DURATION);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
                Close();
            }
        }

        public void StopGame()
        {
            try
            {
                if (mGameRunning)
                {
                    mGameTimer.Enabled = false;
                    Console.WriteLine("Stopping the game thread");
                    mGameRunning = false;
                    // Abort the main game thread
                    Console.WriteLine("Aborting the game main thread...");
                    //m_GameThread.Abort("The game has been closed.");
                    //Console.WriteLine("Game Thread Aborted");
                    mGameThread.Join();
                    Console.WriteLine("Successfully aborted :)");


                    mGameTimer.Enabled = false;

                    PrintScoreBoard();

                    SendGameOver();

                    OnGameOver();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        private void gameEndedTimer_Tick(object sender, EventArgs e)
        {
            //m_GameTimer.Enabled = false;
            //onGameOver();
            StopGame();
        }

        public void AbortGame()
        {
            try
            {
                mGameTimer.Enabled = false;
                mGameAborted = true;
                StopGame();

                Packet p = new Packet(PacketType.Game.GAME_ABORTED);

                foreach (ClientContext clientCtx in mClientsList)
                {
                    clientCtx.SendPacket(p);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
            finally
            {
                //m_GameRunning = false;
            }
        }

        private void OnGameOver()
        {
            double game_last = (Environment.TickCount - mGameStartTime) / 1000.00;

            Console.WriteLine("-------------- Game Over --------------");
            if (mGameAborted)
            {
                Console.WriteLine("X-X Game Aborted X-X");
            }
            Console.WriteLine("Game ended at : {0}", DateTime.Now);
            Console.WriteLine("Game duration : {0} seconds.", game_last);
            Console.WriteLine("-------------- Game Over --------------");
        }

        private void MoveGhosts()
        {
            Packet p = new Packet(PacketType.GameInfo.GHOST_DIRECTION);
            p.WriteInt32(4);

            foreach (Ghost ghost in mGhostsList)
            {
                Coin droppedCoin = ghost.MoveGhost();

                p.WriteUInt32((UInt32)ghost.Val);
                //p.WriteSingle(ghost.GrX);
                //p.WriteSingle(ghost.GrY);
                p.WriteInt32(ghost.X);
                p.WriteInt32(ghost.Y);

                if (droppedCoin == null)
                {
                    p.WriteUInt8((byte)0);
                }
                else
                {
                    p.WriteUInt8((byte)droppedCoin.Type);
                    p.WriteInt32(droppedCoin.X);
                    p.WriteInt32(droppedCoin.Y);
                }
            }

            SendGhostDirection(p);
        }

        public void MoveRequest(ClientContext client, int direction)
        {
            queue.Enqueue(new Tuple<ClientContext, Int32>(client, direction));
        }

        private void MovePacmans()
        {
            Packet p = new Packet(PacketType.GameInfo.PLAYER_DIRECTION);
            p.WriteInt32(mPacmansMap.Values.Count);

            foreach (Pacman pacman in mPacmansMap.Values)
            {
                if (pacman.Respawned)
                {
                    pacman.Respawn();

                    foreach (Ghost ghost in mGhostsList)
                    {
                        ghost.StopHaunt();
                    }

                    p.WriteUInt32(pacman.Id);
                    p.WriteInt32(-1);
                    p.WriteInt32(-1);
                    p.WriteInt32(-1);

                }
                else if (pacman.Move())
                {
                    p.WriteUInt32(pacman.Id);
                    p.WriteInt32(pacman.X);
                    p.WriteInt32(pacman.Y);
                    p.WriteInt32(pacman.Score);

                    if (pacman.EatedFoodScore == (int)MapItem.BANUT_MARE)
                    {
                        foreach (Ghost ghost in mGhostsList)
                        {
                            ghost.Attack(pacman);
                        }
                    }
                }
                else
                {
                    p.WriteUInt32(pacman.Id);
                    p.WriteInt32(0);
                    p.WriteInt32(0);
                    p.WriteInt32(0);
                }
            }

            foreach (ClientContext clientCtx in mClientsList)
            {
                clientCtx.SendPacket(p);
            }
        }

        private void GameLoop()
        {
            int loopTime = 0;

            try
            {
                while (mGameRunning)
                {
                    Tuple<ClientContext, Int32> moveReq;

                    while (queue.TryDequeue(out moveReq))
                    {
                        Pacman pacman;
                        if (mPacmansMap.TryGetValue(moveReq.Item1, out pacman))
                        {
                            pacman.SetNextDir((MapDirection)moveReq.Item2);
                        }
                    }

                    if (loopTime > 2)
                    {
                        MoveGhosts();
                    }

                    if (loopTime > 5)
                    {
                        MovePacmans();
                    }

                    if (loopTime > 5)
                        loopTime = 0;

                    Thread.Sleep(TimeSpan.FromSeconds(GAME_DEELAY));
                    ++loopTime;
                }
            }
            catch (ThreadAbortException abortException)
            {
                Console.WriteLine((string)abortException.ExceptionState);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        private void SendPlayerRespawn(UInt32 playerId)
        {
            Packet p = new Packet(PacketType.GameInfo.PLAYER_RESPAWN);
            p.WriteUInt32(playerId);

            foreach (ClientContext clientCtx in mClientsList)
            {
                clientCtx.SendPacket(p);
            }
        }

        private void SendGameOver()
        {
            Packet p = new Packet(PacketType.Game.GAME_OVER);

            foreach (ClientContext clientCtx in mClientsList)
            {
                clientCtx.SendPacket(p);
            }
        }

        private void SendGameStart()
        {
            Packet p = new Packet(PacketType.Game.GAME_START);
            p.WriteUInt32(GAME_DURATION);

            foreach (ClientContext clientCtx in mClientsList)
            {
                clientCtx.SendPacket(p);
            }
        }

        private void SendGameInit()
        {
            Packet p = new Packet(PacketType.Game.GAME_INIT);

            foreach (ClientContext clientCtx in mClientsList)
            {
                clientCtx.SendPacket(p);
            }
        }

        private void SendGhostDirection(Packet p)
        {
            foreach (ClientContext clientCtx in mClientsList)
            {
                clientCtx.SendPacket(p);
            }
        }

        private void SendGhostsPositions()
        {
            Packet p = new Packet(PacketType.GameInfo.ALL_GHOSTS_POSITION);
            p.WriteInt32(mGhostsList.Count);

            foreach (Ghost ghost in mGhostsList)
            {
                p.WriteUInt32((UInt32)ghost.Val);
                p.WriteInt32(ghost.X);
                p.WriteInt32(ghost.Y);
            }

            foreach (ClientContext clientCtx in mClientsList)
            {
                clientCtx.SendPacket(p);
            }
        }

        private void SendPacmansPositions()
        {
            Packet p = new Packet(PacketType.GameInfo.ALL_PLAYERS_POSITION);
            p.WriteInt32(mClientsList.Count);

            foreach (KeyValuePair<ClientContext, Pacman> mapClientPacman in mPacmansMap)
            {
                p.WriteUInt32(mapClientPacman.Key.Id);
                p.WriteInt32(mapClientPacman.Value.X);
                p.WriteInt32(mapClientPacman.Value.Y);
                p.WriteAscii(mapClientPacman.Key.Nick);
            }

            foreach (ClientContext clientCtx in mClientsList)
            {
                clientCtx.SendPacket(p);
            }
        }

        public void ShowDetails()
        {
            Console.WriteLine("****************************************");
            Console.WriteLine("@) Room: {0}", mGameRoomId);
            Console.WriteLine("@) Players:");
            lock (this)
            {
                foreach (ClientContext clientCtx in mClientsList)
                {
                    if (clientCtx == mGameRoomOwner)
                    {
                        Console.WriteLine("(OWNER)");
                    }
                    Console.Write(clientCtx);
                }
            }
            Console.WriteLine("@) Is running: {0}", mGameRunning);
            Console.WriteLine("****************************************");
        }

        private void PrintScoreBoard()
        {
            // Print scores
            foreach (KeyValuePair<ClientContext, Pacman> mapClientPacman in mPacmansMap)
            {
                Console.WriteLine("{0}) {1} has score {2} points.",
                    mapClientPacman.Key.Id,
                    mapClientPacman.Key.Nick,
                    mapClientPacman.Value.Score);
            }
        }

        private void ShowMap()
        {
            if (mGameBoard != null)
            {
                mGameBoard.Print();
            }
        }
    }
}
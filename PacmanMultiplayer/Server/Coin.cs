﻿namespace Server
{
    public class Coin
    {
        private int xCoordinate;
        private int yCoordinate;
        private MapItem type;

        public int X
        {
            get => xCoordinate;
        }
        public int Y
        {
            get => yCoordinate;
        }
        public MapItem Type
        {
            get => type;
        }

        public Coin(MapItem coinType, int coinX, int coinY)
        {
            xCoordinate = coinX;
            yCoordinate = coinY;
            type = coinType;
        }
    }
}

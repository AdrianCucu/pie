﻿using System.Collections.Generic;
using System.Text;
using System.IO;
using System;

namespace Server
{
    public enum MapItem
    {
        GOL = 0,
        ZID = 9,
        BANUT_MIC = 7,
        BANUT_MARE = 8,
        GHOST1 = 10,
        GHOST2 = 11,
        GHOST3 = 12,
        GHOST4 = 13
        /* PLAYER1=1, PLAYER2=2, PLAYER3=3, PLAYER4=4 */
    }

    public class GameBoard
    {
        public const int DIM_Y = 21;
        public const int DIM_X = 37;
        public const int dimY = 50;
        public UInt32[,] Matrix;


        public List<Tuple<int, int, MapItem>> putGhostsOnMap()
        {
            List<Tuple<int, int, MapItem>> ghostPositions =
            new List<Tuple<int, int, MapItem>>();

            ghostPositions.Add(
                new Tuple<int, int, MapItem>(1, 1, MapItem.GHOST1)
            );

            ghostPositions.Add(
            new Tuple<int, int, MapItem>(35, 19, MapItem.GHOST2)
        );

            ghostPositions.Add(
                new Tuple<int, int, MapItem>(1, 19, MapItem.GHOST3)
            );

            ghostPositions.Add(
            new Tuple<int, int, MapItem>(35, 1, MapItem.GHOST4)
            );

            return ghostPositions;
        }


        public Pacman putPacmanOnMap(UInt32 playerId)
        {
            int playerX, playerY;
            Random rnd = new Random((int)DateTime.Now.Ticks & 0xffff);
            do
            {
                playerX = rnd.Next(1, GameBoard.DIM_X);
                playerY = rnd.Next(1, GameBoard.DIM_Y);
            }
            while (Matrix[playerY, playerX] != (int)MapItem.GOL);

            Matrix[playerY, playerX] = playerId;

            Pacman pacman = new Pacman(playerId, playerX, playerY, this);

            return pacman;
        }


        public void makeCleanMap()
        {
            Matrix = new UInt32[DIM_Y, DIM_X];
            int y, x;
            int BufferSize = DIM_Y * DIM_X;
            string line;

            using (var fileStream = File.OpenRead("map.txt"))
            {
                using (var streamReader =
                    new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
                {
                    for (y = 0; y < DIM_Y; ++y)
                    {
                        line = streamReader.ReadLine();

                        for (x = 0; x < DIM_X; ++x)
                        {
                            if (line[x] == 'X')
                                Matrix[y, x] = (int)MapItem.ZID;

                            else
                                Matrix[y, x] = (int)MapItem.GOL;
                        }
                    }
                }
            }
        }


        public bool IsValidPoint(int pointX, int pointY)
        {
            if (pointX < 0 || pointX >= DIM_X ||
                pointY < 0 || pointY >= DIM_Y ||
                Matrix[pointY, pointX] == (int)MapItem.ZID)
                return false;
            return true;
        }


        public void Print()
        {
            int x, y;
            int scale = 1;

            for (y = 0; y < DIM_Y * scale; ++y)
            {
                for (x = 0; x < DIM_X * scale; ++x)
                {
                    switch (Matrix[y / scale, x / scale])
                    {
                        case (int)MapItem.ZID:
                            Console.Write("X");
                            break;
                        case (int)MapItem.GOL:
                            Console.Write(" ");
                            break;
                        case (int)MapItem.BANUT_MIC:
                            Console.Write("c");
                            break;
                        case (int)MapItem.BANUT_MARE:
                            Console.Write("C");
                            break;
                        /*
                       case (int)MapItem.PLAYER1:
                           Console.Write("1");
                       break; 
                       case (int)MapItem.PLAYER2:
                       Console.Write("2");
                       break; 	
                       case (int)MapItem.PLAYER3: 
                       Console.Write("3");
                       break; 
                       case (int)MapItem.PLAYER4: 
                       Console.Write("4");
                       break; 
                       */
                        default:
                            Console.Write(".");
                            break;
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}

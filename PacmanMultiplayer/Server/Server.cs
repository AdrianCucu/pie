﻿using static System.Reflection.MethodBase;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
using System.Linq;
using System.Text;
using System.Net;
using System;
using Network;

namespace Server
{
    public class Server
    {
        private const bool DEBUG = false;
        private const bool SPAM_HELLO = false;//true;
        private const int MAX_SERVER_CAPACITY = 100;
        private const double SERVER_LOOP_DEELAY = 3.00;

        private readonly bool[] mClientsID = new bool[MAX_SERVER_CAPACITY];

        private Socket mServerSocket;
        private IPEndPoint mEndPont;
        private Thread mServerThread;

        private bool mServerRunning;
        private bool mConnectionAllowed;

        private List<ClientContext> mClientsList;

        private List<GameRoom> mGameRooms;

        private object mClientsListLocker = new object();
        private object mGameRoomListLocker = new object();

        public static EventHandler<ClientContext> OnClientDisconnected;
        public static EventHandler<ClientContext> OnClientConnected;

        public IPAddress Ip
        {
            get => mEndPont.Address;
        }
        public int Port
        {
            get => mEndPont.Port;
        }
        public bool IsRunning
        {
            get => mServerRunning;
        }
        public int ClientCount
        {
            get => mClientsList.Count;
        }

        public Server()
        {
            this.CreateServerSocket(IPAddress.Any, 0);
            this.Init();
        }

        public Server(IPAddress Ip, int Port)
        {
            this.CreateServerSocket(Ip, Port);
            this.Init();
        }

        private void CreateServerSocket(IPAddress Ip, int Port)
        {
            try
            {
                mServerSocket = new Socket(Ip.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                IPEndPoint endPoint = new IPEndPoint(Ip, Port);
                mServerSocket.Bind(endPoint);
                mServerSocket.Listen(10);

                mEndPont = mServerSocket.LocalEndPoint as IPEndPoint;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }
        
        private void Init()
        {
            mGameRooms = new List<GameRoom>();
            mClientsList = new List<ClientContext>(MAX_SERVER_CAPACITY);

            for (int i = 0; i < MAX_SERVER_CAPACITY; ++i)
                mClientsID[i] = true;

            mConnectionAllowed = false;
            mServerRunning = false;
        }
        
        public void StartServer()
        {
            if (mServerThread != null && mServerThread.IsAlive)
                StopServer();

            mServerRunning = true;

            mServerThread = new Thread(new ThreadStart(ServerLoop));
            mServerThread.Start();

            StartService();
        }

        public void StopServer()
        {
            try
            {
                if (mServerRunning)
                {
                    // Abort the main server thread
                    mServerThread.Abort("Admin has stopped the server");
                    mServerThread.Join();

                    /*
                    foreach (GameRoom gameRoom in m_GameRooms)
                    {
                        Console.WriteLine("Stoping the server so the game room.");
                        //game.AbortGame();
                        gameRoom.Close();
                        Console.WriteLine("Game room stopped.");
                    }
                    m_GameRooms.Clear();
                    */

                    foreach (ClientContext clientCtx in mClientsList)
                    {
                        // Trimite-le la toti clienti mesaj
                        clientCtx.SendDisconnect();
                        clientCtx.Disconnect();
                    }
                    mClientsList.Clear();

                    for (int i = 0; i < MAX_SERVER_CAPACITY; ++i)
                        mClientsID[i] = true;

                    //mServerForm.refresh();
                    
                    Console.WriteLine("Server stopped sucessfully");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
            finally
            {
                mServerRunning = false;
                mConnectionAllowed = false;
            }
        }

        private void StartService(int acceptThreadCount = 1)
        {
            mConnectionAllowed = true;

            for (int i = 0; i < acceptThreadCount; ++i)
                mServerSocket.BeginAccept(AsyncAccept, null);
        }

        private void AsyncAccept(IAsyncResult res)
        {
            try
            {
                Socket pending = mServerSocket.EndAccept(res);

                if (pending != null)
                {
                    if (!mConnectionAllowed || !pending.Connected)
                    {
                        pending.Disconnect(false);
                        return;
                    }

                    if (this.ClientCount == MAX_SERVER_CAPACITY)
                    {
                        SendConnectionFailed(pending, PacketType.ConnectionError.SERVER_FULL);
                        pending.Disconnect(false);
                        return;
                    }
                    FireOnNewConnection(pending);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
            finally
            {
                mServerSocket.BeginAccept(AsyncAccept, null);
            }
        }

        private Packet parseRaw(byte[] buffer, int offset, int length)
        {
            if (buffer == null || offset + length >= buffer.Length)
                return new Packet(0);

            int size = length - offset;

            if (size >= Packet.HEADER_LEN)
            {
                int packet_size = (buffer[offset + 1] << 8) | (buffer[offset]);

                if (offset + packet_size > buffer.Length)
                    return new Packet(0);

                int packet_type = (buffer[offset + 3] << 8) | (buffer[offset + 2]);

                int payload_size = packet_size - Packet.HEADER_LEN;

                PacketReader pr =
                    new PacketReader(
                            buffer,
                            offset + Packet.HEADER_LEN,
                            payload_size
                        );

                Packet packet =
                    new Packet(
                        (ushort)packet_type,
                        pr.ReadBytes(payload_size)
                    );

                packet.Lock();
                return packet;
            }
            return new Packet(0);
        }

        private void FireOnNewConnection(Socket pending)
        {
            try
            {
                byte[] buffer = new byte[1024];
                int size = pending.Receive(buffer);

                Packet p = parseRaw(buffer, 0, size);
                string nick = p.ReadAscii();

                if (!Utils.CheckNickname(nick))
                {
                    SendConnectionFailed(pending, PacketType.ConnectionError.NICKNAME_INVALID);
                    return;
                }
                
                lock (mClientsListLocker)
                {
                    foreach (ClientContext clientCtx in mClientsList)
                    {
                        if (String.Compare(clientCtx.Nick, nick, true) == 0)
                        {
                            SendConnectionFailed(pending, PacketType.ConnectionError.NICKNAME_TAKEN);
                            pending.Disconnect(false);
                            return;
                        }
                    }
                }

                if (nick.Length > 20)
                {
                    SendConnectionFailed(pending, PacketType.ConnectionError.NICKNAME_TOO_LONG);
                    pending.Disconnect(false);
                    return;
                }

                if (nick.Length < 3)
                {
                    SendConnectionFailed(pending, PacketType.ConnectionError.NICKNAME_TOO_SHORT);
                    pending.Disconnect(false);
                    return;
                }

                UInt32 id = genID();
                SendWelcomeTo(pending, id);

                // Create client context from connected socket
                ClientContext newClient = new ClientContext();
                newClient.SetSocket(pending);
                newClient.SetId(id);
                newClient.SetNick(nick);
                newClient.SetOwner(this);
                newClient.Begin();

                //SendWelcomeTo(pending, id);

                mClientsList.Add(newClient);

                if (OnClientConnected != null)
                    OnClientConnected(this, newClient);
            }
            catch (Exception ex)
            {
                SendConnectionFailed(pending, PacketType.Connection.CONNECTION_FAILED);
                pending.Disconnect(false);
            }
        }

        public void OnFirstHello(ClientContext client)
        {
            if (this.ClientCount > 1)
            {
                lock (mClientsListLocker)
                {
                    ///////////////////////////////////////////////////////////////////////
                    ///Send to the new client info about others already connected clients
                    Packet p = new Packet(PacketType.Info.ACK);
                    p.WriteInt32(mClientsList.Count - 1);

                    foreach (ClientContext clientCtx in mClientsList)
                    {
                        if (client.Id == clientCtx.Id)
                            continue;

                        p.WriteUInt32(clientCtx.Id);
                        p.WriteAscii(clientCtx.Nick);
                    }

                    client.SendPacket(p);
                    ///////////////////////////////////////////////////////////////////////

                    lock (mGameRoomListLocker)
                    {
                        if (mGameRooms.Count > 0)
                        {
                            Packet p2 = new Packet(PacketType.Info.GAME_ROOMS_ACK);

                            Console.WriteLine("Num. of game rooms: {0}", mGameRooms.Count);

                            p2.WriteInt32(mGameRooms.Count);

                            foreach (GameRoom room in mGameRooms)
                            {
                                room.ShowDetails();
                                p2.WriteUInt32(room.Owner.Id);
                                p2.WriteAscii(room.Owner.Nick);
                                p2.WriteInt32(room.ClientsCount);
                            }
                            client.SendPacket(p2);
                        }
                    }

                    Packet packet = new Packet(PacketType.Info.ACK);
                    packet.WriteInt32(1);
                    packet.WriteUInt32(client.Id);
                    packet.WriteAscii(client.Nick);

                    foreach (ClientContext clientCtx in mClientsList)
                    {
                        if (clientCtx.Id != client.Id)
                        {
                            clientCtx.SendPacket(packet);
                        }
                    }
                }
            }
        }

        public void CreateGameRoom(ClientContext client)
        {
            try
            {
                if (client.InGame)
                {
                    Console.WriteLine("Already in a game");
                    return;
                }
                lock (mGameRoomListLocker)
                {
                    GameRoom gameRoom = new GameRoom(client);
                    mGameRooms.Add(gameRoom);
                    Console.WriteLine("{0} just created a new room", client.Nick);

                    Packet p = new Packet(PacketType.Response.CREATE_ROOM_REQUEST_OK);
                    p.WriteUInt32(gameRoom.Id);
                    client.SendPacket(p);

                    lock (mClientsListLocker)
                    {
                        foreach (ClientContext clientCtx in mClientsList)
                        {
                            Console.WriteLine("Sending to: {0}", clientCtx.Id);
                            clientCtx.SendGameRoomCreated(client);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        public void JoinGameRoom(ClientContext client, UInt32 roomId)
        {
            lock (mGameRoomListLocker)
            {
                foreach (GameRoom gameRoom in mGameRooms)
                {
                    if (gameRoom.Id == roomId)
                    {
                        if (!gameRoom.IsOpen)
                        {
                            client.SendGameRoomJoinFail();
                            break;
                        }

                        gameRoom.Join(client);

                        Console.WriteLine("Player #{0} just joined room #{1}", client.Id, roomId);

                        SendGameRoomsAck();
                        break;
                    }
                }
            }
        }

        public void ExitGameRoom(ClientContext client, GameRoom gameRoom)
        {
            try
            {
                lock (mGameRoomListLocker)
                {
                    Console.WriteLine("Client: #{0} {1} want to leave room #{2}",
                        client.Id, client.Nick, gameRoom.Id
                    );

                    gameRoom.Left(client);
                    client.ExitGameRoom();

                    if (gameRoom.Owner == client)
                    {
                        mGameRooms.Remove(gameRoom);
                    }

                    SendGameRoomsAck();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0}->{1}()",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        /*
        public void CloseGameRoom(ClientContext client, GameRoom gameRoom)
        {
            lock (mGameRoomListLocker)
            {
                Console.WriteLine("Client: #{0} {1} want to CLOSE room #{2}",
                    client.Id, client.Nick, gameRoom.Id
                );

                if (gameRoom.Owner != client)
                {
                    Console.WriteLine("Client: #{0} {1} CANT'T CLOSE THE ROOM BECAUSE HE IS NOT THE OWNER",
                        client.Id, client.Nick
                    );
                    return;
                }

                gameRoom.Close();
                client.ExitGameRoom();
                Console.WriteLine("Player {0}){1} just closed room #{1}",
                    client.Id, client.Nick, gameRoom.Id);

                mGameRooms.Remove(gameRoom);
                SendGameRoomsAck();
            }
        }
        */

        public void DisconnectClient(ClientContext client)
        {
            try
            {
                lock (mClientsListLocker)
                {
                    mClientsList.Remove(client);

                    if (client.InGame)
                    {
                        Console.WriteLine("[SERVER] {0} IS IN A GAMEROOM SO I NEED TO TAKE HIM OUT OF HIS ROOM", client.Nick);
                        ExitGameRoom(client, client.Room);
                    }
                    else
                    {
                        Console.WriteLine("[SERVER] {0} IS NOT IN A GAMEROOM", client.Nick);
                    }

                    foreach (ClientContext clientCtx in mClientsList)
                    {
                        clientCtx.SendOneClientDisconnected(client.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
            finally
            {
                //mServerForm.refresh(client.Id);
                mClientsID[client.Id - 1] = true;
                Console.WriteLine("ID: #{0} DISCONNECTED", client.Id);
                if (OnClientDisconnected != null)
                    OnClientDisconnected(this, client);
            }
        }

        public void KickClient(ClientContext client)
        {
            Console.WriteLine("KIcking the client: " + client.ToString());
            client.SendDisconnect();
            client.Disconnect();
            Console.WriteLine("Kicking the clien ......");
            DisconnectClient(client);

            Console.WriteLine("Client has been kicked!!!!");
        }
        
        private void FireOnLostConnection(ClientContext client)
        {
            DisconnectClient(client);
        }

        private void SendGameRoomsAck()
        {
            Packet p2 = new Packet(PacketType.Info.GAME_ROOMS_ACK);

            p2.WriteInt32(mGameRooms.Count);

            foreach (GameRoom room in mGameRooms)
            {
                p2.WriteUInt32(room.Owner.Id);
                p2.WriteAscii(room.Owner.Nick);
                p2.WriteInt32(room.ClientsCount);
            }

            lock (mClientsListLocker)
            {
                foreach (ClientContext clientCtx in mClientsList)
                {
                    if (true)//(!clientCtx.InGame)
                    {
                        clientCtx.SendPacket(p2);
                    }
                }
            }
        }

        private void SendWelcomeTo(Socket pending, UInt32 id)
        {
            Packet packet = new Packet(PacketType.Connection.WELCOME);
            packet.WriteUInt32(id);
            SendPacket(pending, packet);
        }

        private void SendConnectionFailed(Socket pending, ushort packetType)
        {
            Packet packet = new Packet(packetType);
            SendPacket(pending, packet);
        }

        private void SendPacket(Socket pending, Packet packet)
        {
            byte[] raw = packet.Pack();

            if (raw != null)
            {
                //pending.BeginSend(raw, 0, raw.Length, 0, AsyncSend, pending);
                int size = pending.Send(raw, 0, raw.Length, 0);
                Console.WriteLine("Sent {0} bytes.", size);
            }
        }

        public void SendPrivateMessage(ClientContext from, UInt32 toId, string msg)
        {
            lock (mClientsListLocker)
            {
                foreach (ClientContext clientCtx in mClientsList)
                {
                    if (clientCtx.Id == toId)
                    {
                        clientCtx.SendMessageFrom(from, msg);
                        return;
                    }
                }
            }
        }

        private void ServerLoop()
        {
            Console.WriteLine("Server has been started ...");

            try
            {
                while (mServerRunning)
                {
                    if (DEBUG)
                    {
                        Console.WriteLine("###############################");
                        Console.WriteLine("Clients: {0}", this.ClientCount);

                        lock (mClientsListLocker)
                        {
                            foreach (ClientContext clientCtx in mClientsList)
                            {
                                Console.WriteLine("ID: #{0} nickname: {1}", clientCtx.Id, clientCtx.Nick);
                                Console.WriteLine("----------------------");
                            }
                        }

                        //lock (mGameRoomListLocker)
                        //{
                        //    Console.WriteLine("+========================+");
                        //    Console.WriteLine("Number of Rooms: {0}", mGameRooms.Count);
                        //    foreach (GameRoom gameRoom in this.mGameRooms)
                        //    {
                        //        gameRoom.ShowDetails();
                        //    }
                        //    Console.WriteLine("+========================+");
                        //}

                        Console.WriteLine("###############################");
                    }

                    /*
                    clientSockets.RemoveAll(cl =>
                        DateTime.Now.Subtract(cl.time).Seconds > 60
                    );
		            */

                    // =========================== STRESS =========================== 
                    // Stress the server
                    if (SPAM_HELLO)
                    {
                        try
                        {
                            lock (mClientsListLocker)
                            {
                                for (int i = 0; i < 40; ++i)
                                {
                                    foreach (ClientContext clientCtx in mClientsList)
                                    {
                                        clientCtx.SendHello();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception in: {0}->{1}()",
                                GetType().FullName, GetCurrentMethod().Name);
                            Console.WriteLine(ex);
                        }
                    }
                    // =========================== STRESS =========================== 
                    
                    Thread.Sleep(TimeSpan.FromSeconds(SERVER_LOOP_DEELAY));
                }
            }
            catch (ThreadAbortException abortException)
            {
                Console.WriteLine((string)abortException.ExceptionState);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in: {0} ->{1}",
                    GetType().FullName, GetCurrentMethod().Name);
                Console.WriteLine(ex);
            }
        }

        private UInt32 genID()
        {
            UInt32 new_id = 0;

            for (int i = 0; i < MAX_SERVER_CAPACITY; ++i)
            {
                if (mClientsID[i])
                {
                    new_id = (UInt32)(i + 1);
                    mClientsID[i] = false;
                    break;
                }
            }
            return new_id;
        }
    }
}
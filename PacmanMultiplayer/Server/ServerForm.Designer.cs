﻿namespace Server
{
    partial class ServerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
                components.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.buttonKick = new System.Windows.Forms.Button();
            this.labelIp = new System.Windows.Forms.Label();
            this.labelPort = new System.Windows.Forms.Label();
            this.labelClientsNumber = new System.Windows.Forms.Label();
            this.textBoxIp = new System.Windows.Forms.TextBox();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.textBoxInfo = new System.Windows.Forms.RichTextBox();
            //this.listBoxClients = new System.Windows.Forms.ListBox();
            this.clientList = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(298, 34);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start server";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click +=
        new System.EventHandler(this.buttonStart_Click);
            this.buttonStart.Enabled = true;
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(379, 34);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 1;
            this.buttonStop.Text = "Stop server";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click +=
            new System.EventHandler(this.buttonStop_Click);
            this.buttonStop.Enabled = false;
            // 
            // buttonExit
            // 
            this.buttonExit.Location = new System.Drawing.Point(460, 34);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(75, 23);
            this.buttonExit.TabIndex = 1;
            this.buttonExit.Text = "Exit Server";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click +=
        new System.EventHandler(this.buttonExit_Click);
            this.buttonExit.Enabled = true;
            // 
            // buttonKick
            // 
            this.buttonKick.Location = new System.Drawing.Point(460, 228);
            this.buttonKick.Name = "buttonKick";
            this.buttonKick.Size = new System.Drawing.Size(75, 23);
            this.buttonKick.TabIndex = 9;
            this.buttonKick.Text = "Kick client";
            this.buttonKick.UseVisualStyleBackColor = true;
            this.buttonKick.Click +=
                new System.EventHandler(this.buttonKick_Click);
            this.buttonKick.Enabled = false;
            // 
            // labelIp
            // 
            this.labelIp.AccessibleDescription = "t";
            this.labelIp.AutoSize = true;
            this.labelIp.Location = new System.Drawing.Point(9, 21);
            this.labelIp.Name = "labelIp";
            this.labelIp.Size = new System.Drawing.Size(54, 13);
            this.labelIp.TabIndex = 2;
            this.labelIp.Text = "Server IP";
            // 
            // labelPort
            // 
            this.labelPort.AccessibleDescription = "t";
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(200, 21);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(29, 13);
            this.labelPort.TabIndex = 4;
            this.labelPort.Text = "Server Port";
            // 
            // labelClientsNumber
            // 
            this.labelClientsNumber.AccessibleDescription = "t";
            this.labelClientsNumber.AutoSize = true;
            this.labelClientsNumber.Location = new System.Drawing.Point(298, 63);
            this.labelClientsNumber.Name = "labelClientsNumber";
            this.labelClientsNumber.Size = new System.Drawing.Size(95, 13);
            this.labelClientsNumber.TabIndex = 8;
            this.labelClientsNumber.Text = "Clients #";
            // 
            // textBoxIp
            // 
            this.textBoxIp.Location = new System.Drawing.Point(12, 37);
            this.textBoxIp.Name = "textBoxIp";
            this.textBoxIp.Size = new System.Drawing.Size(155, 20);
            this.textBoxIp.TabIndex = 3;
            this.textBoxIp.Text = "127.0.0.1";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(203, 37);
            this.textBoxPort.Name = "textBox2";
            this.textBoxPort.Size = new System.Drawing.Size(75, 20);
            this.textBoxPort.TabIndex = 5;
            this.textBoxPort.Text = "3200";
            // 
            // textBoxInfo
            // 
            this.textBoxInfo.Location = new System.Drawing.Point(12, 63);
            this.textBoxInfo.Name = "textBoxClientInfo";
            this.textBoxInfo.Size = new System.Drawing.Size(266, 188);
            this.textBoxInfo.TabIndex = 6;
            // 
            // listBoxClients
            // 
            //this.listBoxClients.FormattingEnabled = true;
            //this.listBoxClients.Location = new System.Drawing.Point(298, 84);
            //this.listBoxClients.Name = "listBoxClients";
            //this.listBoxClients.Size = new System.Drawing.Size(237, 134);
            //this.listBoxClients.TabIndex = 7;
            //this.listBoxClients.SelectedIndexChanged += new System.EventHandler(this.listBoxClients_SelectedIndexChanged);

            this.clientList.Location = new System.Drawing.Point(298, 84);
            this.clientList.Name = "listBoxClients";
            this.clientList.Size = new System.Drawing.Size(237, 134);
            this.clientList.TabIndex = 7;
            this.clientList.SelectedIndexChanged += new System.EventHandler(this.ClientList_SelectedIndexChanged);
            this.clientList.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.clientList.FullRowSelect = true;
            this.clientList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.clientList.MultiSelect = false;
            this.clientList.UseCompatibleStateImageBehavior = false;
            this.clientList.View = System.Windows.Forms.View.Details;

            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(620, 263);
            this.ControlBox = false;
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonKick);
            this.Controls.Add(this.textBoxIp);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.textBoxInfo);
            this.Controls.Add(this.labelIp);
            this.Controls.Add(this.labelPort);
            this.Controls.Add(this.labelClientsNumber);
            this.Controls.Add(this.clientList);
            this.Name = "Server";
            this.Text = "Server";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonKick;
        private System.Windows.Forms.Label labelIp;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.Label labelClientsNumber;
        private System.Windows.Forms.TextBox textBoxIp;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.RichTextBox textBoxInfo;

        private System.Windows.Forms.ListView clientList;
        //private System.Windows.Forms.ListBox listBoxClients;
    }
}


﻿using static System.Reflection.MethodBase;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Net;
using System;

namespace Server
{
    public partial class ServerForm : Form
    {
        public class ServerClientView
        {
            public ClientContext ClientCtx;
            public ListViewItem ClientView;

            public ServerClientView(ClientContext client)
            {
                ClientCtx = client;
                ClientView = new ListViewItem(client.Id.ToString());
                ClientView.SubItems.Add(client.Nick);
                ClientView.ForeColor = Color.Black;
            }
        }

        private List<ServerClientView> mServerClients = new List<ServerClientView>();
        private Server mServer;

        public ServerForm()
        {
            InitializeComponent();
            System.Windows.Forms.ColumnHeader columnId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            System.Windows.Forms.ColumnHeader columnNickname = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));

            columnId.Text = "ID";
            columnId.Width = 50;

            columnNickname.Text = "Nickname";
            columnNickname.Width = 200;

            this.clientList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {columnId, columnNickname });
        }
        
        private void OnClientConnected(object sender, ClientContext client)
        {
            textBoxInfo.Invoke(new Action(() => this.InfoText("New connection =====> " + client.ToString())));

            ServerClientView serverClient = new ServerClientView(client);
            mServerClients.Add(serverClient);

            clientList.Invoke(new Action(() => clientList.Items.Add(serverClient.ClientView)));
        }

        private void OnClientDisconnected(object sender, ClientContext client)
        {
            textBoxInfo.Invoke(new Action(() => this.InfoText("Client disconnected: " + client.ToString())));

            clientList.Invoke(new Action(() => {
                foreach (ServerClientView c in mServerClients)
                {
                    if (c.ClientCtx == client)
                    {
                        mServerClients.Remove(c);
                        clientList.Items.Remove(c.ClientView);
                        break;
                    }
                }
            }));
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (textBoxIp.Text == null || String.IsNullOrEmpty(textBoxIp.Text.Trim()))
            {
                this.ErrorMessageBox("Invalid server ip address");
                return;
            }

            if (textBoxPort.Text == null || String.IsNullOrEmpty(textBoxPort.Text.Trim()))
            {
                this.ErrorMessageBox("Invalid server port");
                return;
            }

            try
            {
                IPAddress ip = IPAddress.Parse(textBoxIp.Text);
                int port = Int32.Parse(textBoxPort.Text);

                if (mServer == null)
                {
                    mServer = new Server(ip, port);
                    Server.OnClientConnected += this.OnClientConnected;
                    Server.OnClientDisconnected += this.OnClientDisconnected;
                }

                mServer.StartServer();

                SuccessMessageBox("Server has been started");
                SuccessText(String.Format("IP: {0} PORT: {1}", mServer.Ip, mServer.Port));

                Text = String.Format("IP: {0} PORT: {1}", mServer.Ip, mServer.Port);

                buttonStart.Enabled = false;
                buttonStop.Enabled = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception in {0}::@::{1}",
                GetCurrentMethod().Name, GetType().FullName);
                Console.WriteLine(ex);
                Console.WriteLine("Exception in {0}::@::{1}",
                    GetCurrentMethod().Name, GetType().FullName);
                ErrorMessageBox(ex.Message);
            }
        }
        
        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (mServer != null && mServer.IsRunning)
            {
                mServer.StopServer();

                Text = "Server not running";
                buttonStart.Enabled = true;
                buttonStop.Enabled = false;
                InfoText("Server stopped.");
                SuccessMessageBox("Server stopped.");
            }
        }
        
        private void buttonExit_Click(object sender, EventArgs e)
        {
            // Exiting the form and the application
            if (mServer != null && mServer.IsRunning)
                mServer.StopServer();

            Application.Exit();
        }
        
        private void buttonKick_Click(object sender, EventArgs e)
        {
            if (clientList.SelectedItems == null)
                return;

            foreach (ListViewItem cc in clientList.SelectedItems)
            {
                foreach (ServerClientView cccc in mServerClients)
                {
                    if (cccc.ClientView == cc)
                    {
                        Console.WriteLine(cccc.ClientCtx);
                        mServer.KickClient(cccc.ClientCtx);
                        break;
                    }
                }
            }
        }

        private void ClientList_SelectedIndexChanged(object sender, EventArgs e)
        {
            buttonKick.Enabled = (clientList.SelectedItems != null);
        }

        private void ErrorMessageBox(string errMsg)
        {
            MessageBox.Show(this, errMsg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        
        private void SuccessMessageBox(string successMsg)
        {
            MessageBox.Show(this, successMsg, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        
        private void InfoMsg(string msg)
        {
            textBoxInfo.Invoke(new Action(() => this.InfoText(msg)));
        }
        
        private void SuccessMsg(string msg)
        {
            textBoxInfo.Invoke(new Action(() => this.SuccessText(msg)));
        }
        
        private void ErrorText(string errMsg)
        {
            textBoxInfo.SelectionStart = textBoxInfo.TextLength;
            textBoxInfo.SelectionLength = 0;
            textBoxInfo.SelectionColor = Color.Red;
            textBoxInfo.AppendText("[" + DateTime.Now + "] ERROR # ");
            textBoxInfo.AppendText(errMsg + Environment.NewLine);
            textBoxInfo.SelectionColor = textBoxInfo.ForeColor;
        }
        
        private void SuccessText(string successMsg)
        {
            textBoxInfo.SelectionStart = textBoxInfo.TextLength;
            textBoxInfo.SelectionLength = 0;
            textBoxInfo.SelectionColor = Color.Green;
            textBoxInfo.AppendText("[" + DateTime.Now + "] SUCCESS # ");
            textBoxInfo.AppendText(successMsg + Environment.NewLine);
            textBoxInfo.SelectionColor = textBoxInfo.ForeColor;
        }
        
        private void InfoText(string infoMsg)
        {
            textBoxInfo.SelectionStart = textBoxInfo.TextLength;
            textBoxInfo.SelectionLength = 0;
            textBoxInfo.SelectionColor = Color.Blue;
            textBoxInfo.AppendText("[" + DateTime.Now + "] INFO # ");
            textBoxInfo.AppendText(infoMsg + Environment.NewLine);
            textBoxInfo.SelectionColor = textBoxInfo.ForeColor;
        }
    }
}
﻿using System;
using Server;

namespace Server
{
    public class Pacman : Actor
    {
        private int m_Score;
        private bool m_JustEat;
        private UInt32 m_EatedFood;

        public int Score
        {
            get => m_Score;
        }
        public bool JustEat
        {
            get => m_JustEat;
        }
        public UInt32 EatedFoodScore
        {
            get => m_EatedFood;
        }


        public Pacman(UInt32 id, int posX, int posY, GameBoard gameBoard)
            : base(id, posX, posY, gameBoard)
        {
            m_Score = 0;
            m_EatedFood = 0;
            m_JustEat = false;
        }


        public void Respawn()
        {
            base.Respawn();

            m_Score -= 4;
            m_JustEat = false;
        }


        public bool Move()
        {
            if (m_NextDirection != MapDirection.NONE
            && m_NextDirection != m_CurrentDirection)
            {
                if (CheckDirection(m_NextDirection))
                    m_CurrentDirection = m_NextDirection;
            }

            if (!CheckDirection(m_CurrentDirection))
                return false;
            /*
            else
            {
                if (m_CurrentDirection == MapDirection.EAST)
                {
                    Console.WriteLine("Pacman({0}):  inspre dreapta ", m_Id);
                }
                else if (m_CurrentDirection == MapDirection.NORTH)
                {
                    Console.WriteLine("Pacman({0}):  in sus ", m_Id);
                }
                else if (m_CurrentDirection == MapDirection.SOUTH)
                {
                    Console.WriteLine("Pacman({0}):  in jos ", m_Id);
                }
                else
                {
                    Console.WriteLine("Pacman({0}):  in stanga ", m_Id);
                }
            }
            */

            m_GameMap.Matrix[m_PosY, m_PosX] = (int)MapItem.GOL;

            switch (m_CurrentDirection)
            {
                case MapDirection.NORTH:
                    --m_PosY;
                    break;

                case MapDirection.EAST:
                    ++m_PosX;
                    break;

                case MapDirection.SOUTH:
                    ++m_PosY;
                    break;

                case MapDirection.WEST:
                    --m_PosX;
                    break;

            }

            checkMapForFood();

            m_GameMap.Matrix[m_PosY, m_PosX] = m_Id;
            m_Respawned = false;

            return true;
        }


        public void SetNextDir(MapDirection dir)
        {
            m_NextDirection = dir;
        }


        private void checkMapForFood()
        {
            switch (m_GameMap.Matrix[m_PosY, m_PosX])
            {
                case (int)MapItem.BANUT_MARE:
                case (int)MapItem.BANUT_MIC:
                    m_EatedFood = m_GameMap.Matrix[m_PosY, m_PosX];
                    m_JustEat = true;
                    break;

                default:
                    m_EatedFood = 0;
                    m_JustEat = false;
                    break;
            }
            m_Score += (Int32)m_EatedFood;
        }


        public bool CheckDirection(MapDirection direction)
        {
            switch (direction)
            {
                case MapDirection.NORTH:
                    return DirectionOk(m_PosX, m_PosY - 1);

                case MapDirection.EAST:
                    return DirectionOk(m_PosX + 1, m_PosY);

                case MapDirection.SOUTH:
                    return DirectionOk(m_PosX, m_PosY + 1);

                case MapDirection.WEST:
                    return DirectionOk(m_PosX - 1, m_PosY);

                default:
                    return false;
            }
        }


        private bool DirectionOk(int x, int y)
        {
            if (m_GameMap.Matrix[y, x] == (int)MapItem.BANUT_MIC ||
                m_GameMap.Matrix[y, x] == (int)MapItem.BANUT_MARE ||
                m_GameMap.Matrix[y, x] == (int)MapItem.GOL)

                return true;

            return false;
        }
    }
}

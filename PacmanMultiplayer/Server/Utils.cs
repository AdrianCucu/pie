﻿using static System.Net.NetworkInformation.NetworkInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;

namespace Server
{
    class Utils
    {
        public static IPAddress LocalIPAddress()
        {
            if (!GetIsNetworkAvailable())
                return null;

            string hostname = Dns.GetHostName();

            IPHostEntry hostEntry = Dns.GetHostEntry(hostname);

            if (hostEntry == null)
                return null;

            IPAddress ipAddress =
                hostEntry.AddressList.LastOrDefault(
                    ip => ip.AddressFamily == AddressFamily.InterNetwork
                );

            return ipAddress;
        }


        public static IPAddress GetHostIp(string hostname, AddressFamily af)
        {
            if (!GetIsNetworkAvailable())
                return null;

            IPHostEntry hostEntry = Dns.GetHostEntry(hostname);

            if (hostEntry == null)
                return null;

            foreach (IPAddress ip in hostEntry.AddressList)
                if (ip.AddressFamily == af)
                    return ip;

            return null;
        }

        public static bool CheckNickname(string nick)
        {
            char[] valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNIPQRSTUWXYZ0123456789_".ToCharArray();

            bool isValid = false;

            foreach (char c in nick)
            {
                isValid = false;

                foreach (char ch in valid)
                    if (c == ch)
                    {
                        isValid = true;
                        break;
                    }

                if (!isValid)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
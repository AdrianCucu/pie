﻿using static System.Reflection.MethodBase;
using System.Windows.Forms;
using System.Drawing;
using System;
using Server;

namespace Server
{

    public abstract class Actor
    {
        protected UInt32 m_Id;

        protected int m_PosX;
        protected int m_PosY;

        protected int m_StartX;
        protected int m_StartY;

        protected float m_GraphicsX;
        protected float m_GraphicsY;

        protected MapDirection m_CurrentDirection;
        protected MapDirection m_NextDirection;

        protected bool m_Respawned;

        protected GameBoard m_GameMap;

        public UInt32 Id
        {
            get => m_Id;
        }
        public int X
        {
            get => m_PosX;
        }
        public int Y
        {
            get => m_PosY;
        }
        public float GrX
        {
            get => m_GraphicsX;
        }
        public float GrY
        {
            get => m_GraphicsY;
        }
        public int Dir
        {
            get => (int)m_CurrentDirection;
        }
        public int NextDir
        {
            get => (int)m_NextDirection;
        }
        public bool Respawned
        {
            get => m_Respawned;
        }


        protected Actor(UInt32 id, int posX, int posY, GameBoard gameBoard)
        {
            m_Id = id;
            m_PosX = posX;
            m_PosY = posY;
            m_StartX = posX;
            m_StartY = posY;
            m_GraphicsX = m_PosX * 1.00F;
            m_GraphicsY = m_PosY * 1.00F;
            m_Respawned = false;
            m_CurrentDirection = MapDirection.NONE;
            m_NextDirection = MapDirection.NONE;
            m_GameMap = gameBoard;
        }


        protected void Respawn()
        {
            m_GameMap.Matrix[m_PosY, m_PosX] = (int)MapItem.GOL;
            m_PosX = m_StartX;
            m_PosY = m_StartY;
            m_GameMap.Matrix[m_PosY, m_PosX] = m_Id;
            m_GraphicsX = m_PosX * 1.00F;
            m_GraphicsY = m_PosY * 1.00F;
            m_CurrentDirection = MapDirection.NONE;
            m_NextDirection = MapDirection.NONE;
            m_Respawned = false;
        }


        public void DoRespawn()
        {
            m_Respawned = true;
        }


        /*
        public bool CheckDirection(MapDirection direction)
        {
            switch (direction) 
            { 
            case MapDirection.NORTH: 
	        return DirectionOk(m_PosX, m_PosY - 1); 

            case MapDirection.EAST: 
		return DirectionOk(m_PosX + 1, m_PosY);

            case MapDirection.SOUTH: 
	    	return DirectionOk(m_PosX, m_PosY + 1);

            case MapDirection.WEST: 
		return DirectionOk(m_PosX - 1, m_PosY);

            default: 
	    	return false;
            }
        }


        public bool CheckDirection(int direction)
        {
            switch ((MapDirection)direction) 
            { 
            case MapDirection.NORTH: 
	        return DirectionOk(m_PosX, m_PosY - 1); 

            case MapDirection.EAST: 
		return DirectionOk(m_PosX + 1, m_PosY);

            case MapDirection.SOUTH: 
	    	return DirectionOk(m_PosX, m_PosY + 1);

            case MapDirection.WEST: 
		return DirectionOk(m_PosX - 1, m_PosY);

            default: 
	    	return false;
            }
        }


        public bool DirectionOk(int x, int y)
        {
            return m_GameMap.IsValidPoint(x, y);
        } 
        */


    }
}

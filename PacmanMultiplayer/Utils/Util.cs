﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    class Util
    {
        public static void DumpBuffer(byte[] buf, int offset, int size)
        {
            if (offset + size > buf.Length)
                return;

            for (int i = offset; size > 0; --size, ++i)
                Console.Write("{0:X2} ", buf[i]);
            Console.WriteLine();
        }
    }
}
